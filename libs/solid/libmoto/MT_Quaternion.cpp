#include "MT_Quaternion.h"

const MT_Quaternion MT_Quaternion::IDENTITY(0.0, 0.0, 0.0, 1.0);

#ifndef GEN_INLINED
#include "MT_Quaternion.inl"
#endif
