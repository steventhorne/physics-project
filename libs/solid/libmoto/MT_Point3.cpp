#include "MT_Point3.h"

const MT_Point3 MT_Point3::ORIGIN(0.0, 0.0, 0.0); 

#ifndef GEN_INLINED
#include "MT_Point3.inl"
#endif
