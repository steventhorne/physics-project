#include "MT_Vector3.h"

const MT_Vector3 MT_Vector3::ZERO(MT_Scalar(0.0), MT_Scalar(0.0), MT_Scalar(0.0));

#ifndef GEN_INLINED
#include "MT_Vector3.inl"
#endif
