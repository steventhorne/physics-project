project "solid"
   kind "StaticLib"
   language "C++"
   includedirs { "include", "libbroad", "libmoto", "libsolid" }
   files { "libbroad/**.cpp", "libmoto/**.cpp", "libmoto/**.inl", "libsolid/**.cpp" }

