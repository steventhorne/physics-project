project "glfw"
   kind "StaticLib"
   language "C"
   includedirs { "include" }
   files { "src/*.*" }

   if os.get() == "windows" then
      includedirs { "src", "src/win32/" }
      files { "src/win32/*.*" }
   elseif os.get() == "macosx" then
      includedirs { "src", "src/cocoa" }
      files { "src/cocoa/*.h", "src/cocoa/*.c", "src/cocoa/*.m" }
      buildoptions { "-Wno-deprecated-declarations", "-Wno-incompatible-pointer-types" }
   end
