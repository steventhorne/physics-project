solution "PhysicsEngine"
   configurations { "Debug", "Release" }

   configuration "Debug"
      defines { "DEBUG" }
      flags { "Symbols" }
      objdir ( "./obj/" )
      location ( "./builds/" .. _ACTION )
      targetdir ( "./bin/" )

   configuration "Release"
      defines { "NDEBUG" }
      flags { "Optimize" }
      objdir ( "./obj/" )
      location ( "./builds/" .. _ACTION )
      targetdir ( "./bin/" )

   include "./libs/glfw"
   include "./libs/soil"
   include "./libs/solid"
   include "./projects/current"
   include "./projects/week2"
   include "./projects/week4"
   include "./projects/week8"
   include "./projects/week9"
   include "./projects/week11"
