#include "World.h"

World::World(Game* game)
{
   mpGame = game;

   mMaxContacts = 100;
   mpContacts = new ParticleContact*[mMaxContacts];

   for (int i = 0; i < mMaxContacts; ++i)
   {
      mpContacts[i] = new ParticleContact();
   }
}

World::~World()
{
   
}

PhysicsObject* World::GetPlayer()
{
   return mPlayer;
}

void World::Update(float deltaTime)
{
   RunPhysics(deltaTime);
}

unsigned World::GenerateContacts()
{
   int count = 0;
   ParticleContact* nextContact = mpContacts[count];

   for (ContactGenerators::iterator g = mContactGenerators.begin();
      g != mContactGenerators.end();
      g++)
   {
      unsigned used =(*g)->addContact(nextContact, mMaxContacts - count);
      count += used;
      nextContact = mpContacts[count];

      // We've run out of contacts to fill. This means we're missing
      // contacts.
      if (count > mMaxContacts) break;
   }

   // Return the number of contacts used.
   return count;
}

void World::Integrate(real duration)
{
   // update objects
   for(obj_iter iterator = mObjects.begin(); iterator != mObjects.end(); iterator++)
   {
      iterator->first->update(duration);
   }
}

void World::RunPhysics(real duration)
{
   // update force registry
   mpForceRegistry->updateForces(duration);

   Integrate(duration);

   unsigned usedContacts = GenerateContacts();

   if (usedContacts)
   {
      mpResolver->setIterations(usedContacts * 2);
      mpResolver->resolveContacts(mpContacts, usedContacts, duration);
   }
}

void World::ClearCollisions()
{
   for(obj_iter iterator = mObjects.begin(); iterator != mObjects.end(); iterator++)
   {
      iterator->first->clearCollisions();
   }
}
