#include "Matrix3.h"
#include "Quaternion.h"

Matrix3::Matrix3()
{
   Data[0] = 0;
   Data[1] = 0;
   Data[2] = 0;
   Data[3] = 0;
   Data[4] = 0;
   Data[5] = 0;
   Data[6] = 0;
   Data[7] = 0;
   Data[8] = 0;
}

Matrix3::Matrix3(real t1, real t2, real t3,
                  real t4, real t5, real t6,
                  real t7, real t8, real t9)
{
   Data[0] = t1;
   Data[1] = t2;
   Data[2] = t3;
   Data[3] = t4;
   Data[4] = t5;
   Data[5] = t6;
   Data[6] = t7;
   Data[7] = t8;
   Data[8] = t9;
}

Vector3 Matrix3::operator*(const Vector3 &vector) const
{
   return Vector3(
      vector.X * Data[0] + vector.Y * Data[1] + vector.Z * Data[2],
      vector.X * Data[3] + vector.Y * Data[4] + vector.Z * Data[5],
      vector.X * Data[6] + vector.Y * Data[7] + vector.Z * Data[8]
      );
}

Matrix3 Matrix3::operator*(const Matrix3 &o) const
{
   return Matrix3(
      Data[0]*o.Data[0] + Data[1]*o.Data[3] + Data[2]*o.Data[6],
      Data[0]*o.Data[1] + Data[1]*o.Data[4] + Data[2]*o.Data[7],
      Data[0]*o.Data[2] + Data[1]*o.Data[5] + Data[2]*o.Data[8],

      Data[3]*o.Data[0] + Data[4]*o.Data[3] + Data[5]*o.Data[6],
      Data[3]*o.Data[1] + Data[4]*o.Data[4] + Data[5]*o.Data[7],
      Data[3]*o.Data[2] + Data[4]*o.Data[5] + Data[5]*o.Data[8],

      Data[6]*o.Data[0] + Data[7]*o.Data[3] + Data[8]*o.Data[6],
      Data[6]*o.Data[1] + Data[7]*o.Data[4] + Data[8]*o.Data[7],
      Data[6]*o.Data[2] + Data[7]*o.Data[5] + Data[8]*o.Data[8]
      );
}

void Matrix3::operator*=(const Matrix3 &o)
{
   real t1;
   real t2;
   real t3;

   t1 = Data[0]*o.Data[0] + Data[1]*o.Data[3] + Data[2]*o.Data[6];
   t2 = Data[0]*o.Data[1] + Data[1]*o.Data[4] + Data[2]*o.Data[7];
   t3 = Data[0]*o.Data[2] + Data[1]*o.Data[5] + Data[2]*o.Data[8];

   Data[0] = t1;
   Data[1] = t2;
   Data[2] = t3;

   t1 = Data[3]*o.Data[0] + Data[4]*o.Data[3] + Data[5]*o.Data[6];
   t2 = Data[3]*o.Data[1] + Data[4]*o.Data[4] + Data[5]*o.Data[7];
   t3 = Data[3]*o.Data[2] + Data[4]*o.Data[5] + Data[5]*o.Data[8];

   Data[3] = t1;
   Data[4] = t2;
   Data[5] = t3;

   t1 = Data[6]*o.Data[0] + Data[7]*o.Data[3] + Data[8]*o.Data[6];
   t2 = Data[6]*o.Data[1] + Data[7]*o.Data[4] + Data[8]*o.Data[7];
   t3 = Data[6]*o.Data[2] + Data[7]*o.Data[5] + Data[8]*o.Data[8];

   Data[6] = t1;
   Data[7] = t2;
   Data[8] = t3;
}

Vector3 Matrix3::Transform(const Vector3 &vector) const
{
   return (*this) * vector;
}

void Matrix3::SetInverse(const Matrix3 &m)
{
   real t1 = m.Data[0]*m.Data[4];
   real t2 = m.Data[0]*m.Data[5];
   real t3 = m.Data[1]*m.Data[3];
   real t4 = m.Data[2]*m.Data[3];
   real t5 = m.Data[1]*m.Data[6];
   real t6 = m.Data[2]*m.Data[6];

   real det = (t1*m.Data[8] - t2*m.Data[7] - t3*m.Data[8] +
               t4*m.Data[7] + t5*m.Data[5] - t6*m.Data[4]);

   if (det == (real)0.0f) return;
   real invd = (real)1.0f/det;

   Data[0] = (m.Data[4]*m.Data[8] - m.Data[5]*m.Data[7])*invd;
   Data[1] = -(m.Data[1]*m.Data[8] - m.Data[2]*m.Data[7])*invd;
   Data[2] = (m.Data[1]*m.Data[5] - m.Data[2]*m.Data[4])*invd;
   Data[3] = -(m.Data[3]*m.Data[8] - m.Data[5]*m.Data[6])*invd;
   Data[4] = (m.Data[0]*m.Data[8] - t6)*invd;
   Data[5] = -(t2 - t4)*invd;
   Data[6] = (m.Data[3]*m.Data[7] - m.Data[4]*m.Data[6])*invd;
   Data[7] = -(m.Data[0]*m.Data[7]-t5)*invd;
   Data[8] = (t1 - t3)*invd;
}

Matrix3 Matrix3::Inverse() const
{
   Matrix3 result;
   result.SetInverse(*this);
   return result;
}

void Matrix3::Invert()
{
   SetInverse(*this);
}

void Matrix3::SetTranspose(const Matrix3 &m)
{
   Data[0] = m.Data[0];
   Data[1] = m.Data[3];
   Data[2] = m.Data[6];
   Data[3] = m.Data[1];
   Data[4] = m.Data[4];
   Data[5] = m.Data[7];
   Data[6] = m.Data[2];
   Data[7] = m.Data[5];
   Data[8] = m.Data[8];
}

Matrix3 Matrix3::Transpose() const
{
   Matrix3 result;
   result.SetTranspose(*this);
   return result;
}

void Matrix3::SetOrientation(const Quaternion &q)
{
   Data[0] = 1 - (2*q.J*q.J + 2*q.K*q.K);
   Data[1] = 2*q.I*q.J + 2*q.K*q.R;
   Data[2] = 2*q.I*q.K - 2*q.J*q.R;
   Data[3] = 2*q.I*q.J - 2*q.K*q.R;
   Data[4] = 1 - (2*q.I*q.I + 2*q.K*q.K);
   Data[5] = 2*q.J*q.K + 2*q.I*q.R;
   Data[6] = 2*q.I*q.K + 2*q.J*q.R;
   Data[7] = 2*q.J*q.K - 2*q.I*q.R;
   Data[8] = 1 - (2*q.I*q.I + 2*q.J*q.J);
}
