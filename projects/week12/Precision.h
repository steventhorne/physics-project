typedef float real;

#define real_abs fabsf

#define real_sin sinf
#define real_cos cosf
#define real_exp expf
#define real_sqrt sqrtf

#define PI 3.14
