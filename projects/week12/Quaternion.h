#pragma once
#include "Vector3.h"

class Quaternion
{
public:
   Quaternion(real r, real i, real j, real k);
   ~Quaternion(){};

   void operator*=(const Quaternion &multiplier);

   void Normalize();

   void RotateByVector(const Vector3 &vector);

   void AddScaledVector(const Vector3 &vector, real scale);

   union
   {
      struct
      {
         real R;
         real I;
         real J;
         real K;
      };

      real Data[4];
   };
};
