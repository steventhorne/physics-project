#pragma once
#include <GL/glfw.h>
#include <string>

using namespace std;

class Image
{
public:
   Image(string path);
   ~Image(){};

   GLuint GetGLuint();
   bool Bind();
   string GetPath();

private:
   GLuint mTexture;
   string mPath;

};
