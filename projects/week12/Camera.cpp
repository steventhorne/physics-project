#include <math.h>
#include <iostream>
#include "Camera.h"
#include "Game.h"
#include "Input.h"

using namespace std;

Camera::Camera(Game* pGame)
{
    mpGame = pGame;
    mPos.X = 0;
    mPos.Y = 0;
    xrot = yrot = 0.0;
    mPos.Z = 0;
    mLastScroll = 0;
    mCameraSpeed = CAMERA_SPEED;
}

Camera::Camera(Game* pGame, Vector3 p)
{
    mpGame = pGame;
    mPos = p;
    xrot = 0;
    yrot = 0;
    mLastScroll = 0;
    mCameraSpeed = CAMERA_SPEED;
}

Camera::Camera(Game* pGame, Vector3 p, Vector2 r)
{
    mpGame = pGame;
    mPos = p;
    xrot = r.X;
    yrot = r.Y;
    mLastScroll = 0;
    mCameraSpeed = CAMERA_SPEED;
}

Camera::Camera(Game* pGame, Vector3 p, Vector3 r)
{
    mpGame = pGame;
    mPos = p;
    xrot = r.X;
    yrot = r.Y;
    mLastScroll = 0;
    mCameraSpeed = CAMERA_SPEED;
}

void Camera::update(real deltaTime)
{
    if (mpGame->getInputManager()->isKeyPressed(GLFW_KEY_ENTER)) //temporarily using this for debug info... press enter to see debug info.
    {
        cout << "mPos.X: " << mPos.X << ", mPos.Y: " << mPos.Y << ", mPos.Z: " << mPos.Z << ", xrot: " << xrot << ", yrot: " << yrot << endl;
        //cout << "deltaTime: " << deltaTime << endl;
    }

    if (mpGame->getInputManager()->isKeyPressed('R'))
    {
        xrot = 0;
        yrot = 0;
    }

    if (mpGame->getInputManager()->mouseButtonDown(GLFW_MOUSE_BUTTON_RIGHT))
    {
        xrot += mpGame->getInputManager()->mouseDiff().Y;
        if (xrot > 360)
            xrot -= 360;

        yrot += mpGame->getInputManager()->mouseDiff().X;
        if (yrot < -360)
            yrot += 360;
    }

    /*if (mpGame->getInputManager()->isKeyPressed('Q'))
    {
        yrot -= 1;
        if (yrot < -360)
            yrot += 360;
    }

    if (mpGame->getInputManager()->isKeyPressed('E'))
    {
        yrot += 1;
        if (yrot > 360)
            yrot -= 360;
    }

    if (mpGame->getInputManager()->isKeyPressed('W'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        mPos.X += float(sin(yrotrad)) * CAMERA_SPEED;
        mPos.Z -= float(cos(yrotrad)) * CAMERA_SPEED;
        mPos.Y -= float(sin(xrotrad)) * CAMERA_SPEED;
    }
    else if (mpGame->getInputManager()->isKeyPressed('S'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        mPos.X -= float(sin(yrotrad)) * CAMERA_SPEED;
        mPos.Z += float(cos(yrotrad)) * CAMERA_SPEED;
        mPos.Y += float(sin(xrotrad)) * CAMERA_SPEED;
    }

    if (mpGame->getInputManager()->isKeyPressed('A'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        mPos.X -= float(cos(yrotrad)) * CAMERA_SPEED;
        mPos.Z -= float(sin(yrotrad)) * CAMERA_SPEED;
        //mPos.Y -= float(sin(xrotrad));
    }
    else if(mpGame->getInputManager()->isKeyPressed('D'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        mPos.X += float(cos(yrotrad)) * CAMERA_SPEED;
        mPos.Z += float(sin(yrotrad)) * CAMERA_SPEED;
        //mPos.Y += float(sin(xrotrad));
    }*/

    int scrollDiff = glfwGetMouseWheel() - mLastScroll;
    float xrotrad, yrotrad;
    yrotrad = (yrot / 180 * 3.141592654f);
    xrotrad = (xrot / 180 * 3.141592654f);
    mPos.X += float(sin(yrotrad)) * scrollDiff * 10 * deltaTime;
    mPos.Z -= float(cos(yrotrad)) * scrollDiff * 10 * deltaTime;
    mPos.Y -= float(sin(xrotrad)) * scrollDiff * 10 * deltaTime;

    mLastScroll = glfwGetMouseWheel();
}

void Camera::applyCameraTranslation()
{
    glTranslated(-mPos.X,-mPos.Y,-mPos.Z); //translate the screen to the position of our camera
}

void Camera::applyCameraRotation()
{
    glRotatef(xrot,1.0,0.0,0.0);  //rotate our camera on the x-axis (left and right)
    glRotatef(yrot,0.0,1.0,0.0);  //rotate our camera on the y-axis (up and down)
}

void Camera::applyCamera()
{
    // camera updates
    glRotatef(xrot,1.0,0.0,0.0);  //rotate our camera on the x-axis (left and right)
    glRotatef(yrot,0.0,1.0,0.0);  //rotate our camera on the y-axis (up and down)
    glTranslated(-mPos.X,-mPos.Y,-mPos.Z); //translate the screen to the position of our camera
}

void Camera::draw()
{
    float currentColor[4];
    glGetFloatv(GL_CURRENT_COLOR,currentColor);

    int myviewport[4];
    glGetIntegerv(GL_VIEWPORT, myviewport);

    GLfloat mv[16];
    //=getModelviewMatrix();
    glGetFloatv(GL_MODELVIEW_MATRIX, mv);
    mv[3]=mv[3+4]=mv[3+8]=0.0;
    glPushMatrix();
    glLoadIdentity();
    glViewport(0, 0, 100, 100);
    glMultMatrixf(mv); // orientation
    glTranslated(0, 0, -2.0);
    applyCameraRotation();

    glBegin(GL_LINES);

    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(1.0, 0.0, 0.0);

    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 1.0, 0.0);

    glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 1.0);

    glEnd();
    glPopMatrix();

    glViewport(myviewport[0], myviewport[1], myviewport[2], myviewport[3]);
    glColor4f(currentColor[0], currentColor[1], currentColor[2], currentColor[3]);
}
