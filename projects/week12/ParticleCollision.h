#include "ParticleContactGenerator.h"
#include "ParticleContact.h"

class ParticleCollision : public ParticleContactGenerator
{
   public:
   PhysicsObject* Objects[2];

   real MinLength;
   real Restitution;

   virtual unsigned addContact(ParticleContact* contact, unsigned limit) const;

   private:
   real currentLength() const;
};
