-- A project defines one build target
project "Quaternions"
   kind "ConsoleApp"
   language "C++"
   targetname "quaternions"
   files { "**.h", "**.cpp" }
   includedirs { "./../../libs/glfw/include", "./../../libs/soil/include" }
   links { "glfw", "soil" }

   if os.get() == "macosx" then
      links { "OpenGL.framework", "Cocoa.framework", "CoreFoundation.framework", "IOKit.framework" }
   elseif os.get() == "windows" then
      links { "opengl32", "glu32" }
   end
