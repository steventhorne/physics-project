#pragma once
#include "ParticleForceGenerator.h"

class ParticleBungee : public ParticleForceGenerator
{
   public:
   ParticleBungee(PhysicsObject* other, real springConstant, real restLength);

   virtual void updateForce(PhysicsObject* object, real duration);

   private:
   PhysicsObject* mOther;
   real mSpringConstant;
   real mRestLength;
};
