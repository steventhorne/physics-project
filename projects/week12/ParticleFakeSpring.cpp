#include "ParticleFakeSpring.h"
#include <Math.h>

ParticleFakeSpring::ParticleFakeSpring(Vector3* anchor, real springConstant, real damping)
{
   mAnchor = anchor;
   mSpringConstant = springConstant;
   mDamping = damping;
}

void ParticleFakeSpring::updateForce(PhysicsObject* object, real duration)
{
   if (!object->hasFiniteMass()) return;

   Vector3 position;
   position = object->getPosition();
   position -= *mAnchor;

   real gamma = 0.5f * sqrtf(4 * mSpringConstant - mDamping*mDamping);
   if (gamma == 0.0f) return;
   Vector3 c = position * (mDamping / (2.0f * gamma)) +
      object->getVelocity() * (1.0f / gamma);

   Vector3 target = position * real_cos(gamma * duration) + c * real_sin(gamma * duration);
   target *= real_exp(-0.5f * duration * mDamping);

   Vector3 accel = (target - position) * (1.0f / duration*duration) -
      object->getVelocity() * duration;
   object->addForce(accel);
}
