#include "ParticleAnchorSpring.h"
#include <stdio.h>

ParticleAnchorSpring::ParticleAnchorSpring(Vector3 anchor, real springConstant, real restLength)
{
   mAnchor = anchor;
   mSpringConstant = springConstant;
   mRestLength = restLength;
}

void ParticleAnchorSpring::updateForce(PhysicsObject* object, real duration)
{
   Vector3 force;
   force = object->getPosition();
   force -= mAnchor;

   real magnitude = force.magnitude();
   magnitude = (mRestLength - magnitude) * mSpringConstant;

   force.normalize();

   force *= magnitude;

   object->addForce(force);
}
