#pragma once
#include "ParticleContactGenerator.h"

class ParticleLink : public ParticleContactGenerator
{
   public:
   PhysicsObject* Objects[2];

   virtual unsigned addContact(ParticleContact* contact, unsigned limit) const = 0;

   protected:
   real currentLength() const;
};
