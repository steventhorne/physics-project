#include "Quaternion.h"
#include <math.h>

Quaternion::Quaternion(real r, real i, real j, real k)
{
   R = r;
   I = i;
   J = j;
   K = k;
}

void Quaternion::Normalize()
{
   real d = R*R + I*I + J*J + K*K;

   if (d == 0)
   {
      R = 1;
      return;
   }

   d = ((real)1.0)/real_sqrt(d);
   R *= d;
   I *= d;
   J *= d;
   K *= d;
}

void Quaternion::operator*=(const Quaternion &multiplier)
{
   Quaternion q = *this;
   R = q.R*multiplier.R - q.I*multiplier.I -
       q.J*multiplier.J - q.K*multiplier.K;
   I = q.R*multiplier.I + q.I*multiplier.R +
       q.J*multiplier.K - q.K*multiplier.J;
   J = q.R*multiplier.J + q.J*multiplier.R +
       q.K*multiplier.I - q.I*multiplier.K;
   K = q.R*multiplier.K + q.K*multiplier.R +
       q.I*multiplier.J - q.J*multiplier.I;
}

void Quaternion::RotateByVector(const Vector3 &vector)
{
   Quaternion q(0, vector.X, vector.Y, vector.Z);
   (*this) *= q;
}

void Quaternion::AddScaledVector(const Vector3 &vector, real scale)
{
   Quaternion q(0, vector.X*scale, vector.Y*scale, vector.Z*scale);

   q *= *this;
   R += q.R * ((real)0.5);
   I += q.I * ((real)0.5);
   J += q.J * ((real)0.5);
   K += q.K * ((real)0.5);
}
