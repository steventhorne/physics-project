#pragma once
#include <GL/glfw.h>
#include "World.h"
#include "Plane.h"

class Cannon;

class Level2 : public World
{
public:
   Level2(Game* game);
   virtual ~Level2();

   bool Init();
   void Update(float deltaTime);
   void Draw();

private:
   //path
   Plane* mPath;

   //springs (vertical)
   PhysicsObject* mPendAnchor1;
   PhysicsObject* mPendBall1;
   PhysicsObject* mPendAnchor2;
   PhysicsObject* mPendBall2;
   PhysicsObject* mPendAnchor3;
   PhysicsObject* mPendBall3;

   //advanced pendulums
   PhysicsObject* mAdvPend1Achor;
   PhysicsObject* mAdvPend1Ball1;
   PhysicsObject* mAdvPend1Ball2;
   PhysicsObject* mAdvPend2Achor;
   PhysicsObject* mAdvPend2Ball1;
   PhysicsObject* mAdvPend2Ball2;
};
