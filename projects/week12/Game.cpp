#include <iostream>
#include <math.h>
#include <SOIL.h>
#include "Game.h"
#include "Input.h"
#include "Camera.h"
#include "Level1.h"
#include "Level2.h"

using namespace std;

Game::Game()
{
   mpInputManager = new Input();
   mpCamera = new Camera(this);

   mLevel = new Level1(this);
   mCurrentLvlNum = 1;
   mNumLevels = 2;

   mShouldExit = false;
}

Game::~Game()
{
   delete mpInputManager;
   mpInputManager = NULL;

   delete mpCamera;
   mpCamera = NULL;

   delete mLevel;
   mLevel = NULL;
}

bool Game::init()
{
   mpInputManager->Init();

   GLfloat sun_intensity[] = { 0.8, 0.8, 0.8, 1.0 };
   GLfloat sun_direction[] = { 0.0, 2.0, -1.0, 1.0 };
   GLfloat ambient_intensity[] = { 0.35, 0.35, 0.35, 1.0 };

   glEnable(GL_LIGHTING);
   glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_intensity);

   glEnable(GL_LIGHT0);
   glLightfv(GL_LIGHT0, GL_POSITION, sun_direction);
   glLightfv(GL_LIGHT0, GL_DIFFUSE, sun_intensity);

   glEnable(GL_COLOR_MATERIAL);
   glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

   mDeltaTime = 0.0;
   mLastTime = 0;

   //-- init level
   mLevel->Init();
   //--

   return true;
}

void Game::update()
{
   if (mLastTime == 0)
   {
      mLastTime = glfwGetTime();
      return;
   }

   mpInputManager->Update();

   real deltaTime = glfwGetTime() - mLastTime;

   if(mpInputManager->isKeyPressed(GLFW_KEY_UP) || mpInputManager->isKeyPressed('W'))
   {
      mLevel->GetPlayer()->addForce(Vector3(0, 0, -150*deltaTime));
   }
   else if (mpInputManager->isKeyPressed(GLFW_KEY_DOWN) || mpInputManager->isKeyPressed('S'))
   {
      mLevel->GetPlayer()->addForce(Vector3(0, 0, 150*deltaTime));
   }

   if (mpInputManager->isKeyPressed('P'))
   {
      SOIL_save_screenshot
      (
       "ss.bmp",
       SOIL_SAVE_TYPE_BMP,
       0, 0, 1080, 720
       );
   }

   //-- update level
   mLevel->Update(deltaTime);
   //--

   if (mLevel->GetPlayer()->isColliding())
   {
      loadNewLevel(mCurrentLvlNum);
   }

   mLevel->ClearCollisions();

   if (mLevel->GetPlayer()->getPosition().Z < mLevel->GetEnd())
   {
      // load new level
      mCurrentLvlNum++;

      if ( mCurrentLvlNum > mNumLevels )
      {
         mShouldExit = true;
         return;
      }

      loadNewLevel(mCurrentLvlNum);
   }

   mpCamera->update(deltaTime);

   mLastTime = glfwGetTime();

   draw();
}

void Game::draw()
{
   glClearColor( 0.2f, 0.2f, 0.2f, 1.0f ); // clear background to gray
   glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glClearDepth(1.0);

   glMatrixMode(GL_MODELVIEW);

   glLoadIdentity();

   //draw level here
   mLevel->Draw();
   //---------------

   glPushMatrix();

   mpCamera->applyCamera();

   glPopMatrix();

   mpCamera->draw();

   glfwSwapBuffers();
}

void Game::loadNewLevel(int levelNum)
{
   delete mLevel; // delete old level

   //check level num and load correct level
   switch (levelNum)
   {
      case 1:
         mLevel = new Level1(this);
         break;
      case 2:
         mLevel = new Level2(this);
         break;
      default:
         break;
   }

   mLevel->Init(); // init new level
}
