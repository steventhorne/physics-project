#include "ParticleContact.h"
#include <limits>
#include <vector>

using namespace std;

#define REAL_MAX std::numeric_limits<real>::max();

class ParticleContactResolver
{
   public:
   ParticleContactResolver(unsigned iterations);

   void setIterations(unsigned iterations);

   void resolveContacts(ParticleContact** contactArray, unsigned numContacts, real duration);

   protected:
   unsigned mIterations;

   unsigned mIterationsUsed;

};
