#pragma once
#include "Vector3.h"

class Quaternion;

class Matrix3
{
public:
   Matrix3();
   Matrix3(real t1, real t2, real t3,
      real t4, real t5, real t6,
      real t7, real t8, real t9);

   ~Matrix3(){};

   real Data[9];

   Vector3 operator*(const Vector3 &vector) const;
   Matrix3 operator*(const Matrix3 &o) const;
   void operator*=(const Matrix3 &o);

   Vector3 Transform(const Vector3 &vector3) const;

   void SetInverse(const Matrix3 &m);
   Matrix3 Inverse() const;
   void Invert();

   void SetTranspose(const Matrix3 &m);
   Matrix3 Transpose() const;

   void SetOrientation(const Quaternion &q);
};
