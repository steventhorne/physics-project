#include "Object.h"
#include "Game.h"

Object::Object()
{
   mPosition = Vector3(0, 0, 0);
   mRotation = Vector3(0, 0, 0);
   mScale = Vector3(1, 1, 1);
}

Object::Object(Game* pGame)
{
   mpGame = pGame;
   mPosition = Vector3(0, 0, 0);
   mRotation = Vector3(0, 0, 0);
   mScale = Vector3(1, 1, 1);
   mColor = Vector3(1, 1, 1);
}

Object::Object(Game* pGame, Vector3 color)
{
   mpGame = pGame;
   mPosition = Vector3(0, 0, 0);
   mRotation = Vector3(0, 0, 0);
   mScale = Vector3(1, 1, 1);
   mColor = color;
}

Object::Object(Game* pGame, Vector3 color, Vector3 position)
{
   mpGame = pGame;
   mPosition = position;
   mRotation = Vector3(0, 0, 0);
   mScale = Vector3(1, 1, 1);
   mColor = color;
}

Object::Object(Game* pGame, Vector3 color, Vector3 position, Vector3 rotation)
{
   mpGame = pGame;
   mPosition = position;
   mRotation = rotation;
   mScale = Vector3(1, 1, 1);
   mColor = color;
}

Object::Object(Game* pGame, Vector3 color, Vector3 position, Vector3 rotation, Vector3 scale)
{
   mpGame = pGame;
   mPosition = position;
   mRotation = rotation;
   mScale = scale;
   mColor = color;
}
