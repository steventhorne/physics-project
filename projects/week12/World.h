#pragma once
#include <vector>
#include <map>
#include "Game.h"
#include "ParticleForceRegistry.h"
#include "ParticleContactResolver.h"
#include "ParticleContactGenerator.h"
#include "ParticleContact.h"
#include "PhysicsObject.h"
#include "Plane.h"

using namespace std;

class World
{
public:
    World(Game* game);
    virtual ~World();

    virtual bool Init() = 0;
    virtual void Update(float deltaTime);
    virtual void Draw() = 0;

    virtual unsigned GenerateContacts();

    virtual void Integrate(real duration);
    virtual void RunPhysics(real duration);

    virtual void ClearCollisions();

    PhysicsObject* GetPlayer();

    float GetEnd() { return mEndZPos; };

protected:
    Game* mpGame;

    typedef vector<ParticleContactGenerator*> ContactGenerators;
    ParticleForceRegistry* mpForceRegistry;
    ParticleContactResolver* mpResolver;
    ContactGenerators mContactGenerators;
    ParticleContact** mpContacts;
    unsigned mMaxContacts;

    typedef map<PhysicsObject*, real> ObjMap;
    typedef pair<PhysicsObject*, real> ObjPair;
    typedef ObjMap::iterator obj_iter;
    ObjMap mObjects;

    PhysicsObject* mPlayer;
    float mEndZPos;
};
