#include "ParticleBungee.h"

ParticleBungee::ParticleBungee(PhysicsObject* other, real springConstant, real restLength)
{
   mOther = other;
   mSpringConstant = springConstant;
   mRestLength = restLength;
}

void ParticleBungee::updateForce(PhysicsObject* object, real duration)
{
   Vector3 force;
   force = object->getPosition();
   force -= mOther->getPosition();

   real magnitude = force.magnitude();
   if (magnitude <= mRestLength) return;

   magnitude = mSpringConstant * (mRestLength - magnitude);

   force.normalize();
   force *= magnitude;
   object->addForce(force);
}
