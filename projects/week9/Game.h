#pragma once
#include "Precision.h"

class Level1;
class Input;
class Camera;

class Game
{
    public:
    Game();
    ~Game();

    Input* getInputManager() { return mpInputManager; };
    Camera* getCamera() { return mpCamera; };

    bool init();
    void update();

    void draw();

    private:
    Input* mpInputManager;
    Camera* mpCamera;
    
    Level1* mLevel;

    unsigned generateContacts();
    void integrate(real duration);
    void runPhysics(real duration);

    real mDeltaTime;
    real mLastTime;
};
