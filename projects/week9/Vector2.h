#pragma once
#include "Precision.h"

class Vector2
{
    public:
    Vector2(real x, real y);
    Vector2() {};
    ~Vector2() {};

    Vector2 operator+( const Vector2& rhs ) const;
    Vector2 operator-( const Vector2& rhs ) const;
    real operator*( const Vector2& rhs ) const;
    Vector2 operator*( const real& rhs ) const;

    void normalize();
    Vector2 normalized();

    real X;
    real Y;
};
