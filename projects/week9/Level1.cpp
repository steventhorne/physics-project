#include "Level1.h"
#include "Camera.h"
#include "Sphere.h"
#include "ParticleGravity.h"
#include "ParticleSpring.h"
#include "ParticleAnchorSpring.h"
#include "ParticleCollision.h"
#include "ParticleRod.h"
#include "CableLink.h"

Level1::Level1(Game* game)
:World(game)
{
    mpForceRegistry = new ParticleForceRegistry();
    mpResolver = new ParticleContactResolver(0);
    mMaxContacts = 100;
    mpContacts = (ParticleContact*) malloc (sizeof(ParticleContact) * mMaxContacts);
}

Level1::~Level1()
{
    delete mpForceRegistry;
    mpForceRegistry = NULL;
    
    delete mpResolver;
    mpResolver = NULL;
    
    delete mpContacts;
    mpContacts = NULL;
    
    while(!mContactGenerators.empty()) mContactGenerators.pop_back();
}

bool Level1::Init()
{
    // init objects
    mFloor = new Plane(mpGame, Vector3(1, 1, 1), Vector3(0, 0, 0), Vector3(0, 0, 0), Vector3(20, 20, 20));
    
    //------------------- Instantiate Objects ----------------------
    mAnchor = new PhysicsObject(new Sphere(mpGame), Vector3(0, 6, 0));
    mAnchor->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
    mAnchor->setInverseMass(0);
    mObjects.insert(ObjPair(mAnchor, 1));
    
    mBall = new PhysicsObject(new Sphere(mpGame), Vector3(2, 5, 0));
    mBall->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mBall->setMass(1);
    mObjects.insert(ObjPair(mBall, 1));
    
    mBallPair1 = new PhysicsObject(new Sphere(mpGame), Vector3(3, 4, -2));
    mBallPair1->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mBallPair1->setMass(1);
    mObjects.insert(ObjPair(mBallPair1, 1));
    
    mBallPair2 = new PhysicsObject(new Sphere(mpGame), Vector3(6, 2, 4));
    mBallPair2->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mBallPair2->setMass(1);
    mObjects.insert(ObjPair(mBallPair2, 1));
    
    mSideBall = new PhysicsObject(new Sphere(mpGame), Vector3(-6, 2, 0));
    mSideBall->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mSideBall->setMass(1);
    mObjects.insert(ObjPair(mSideBall, 1));
    
    mSideBall2 = new PhysicsObject(new Sphere(mpGame), Vector3(-6, 0, 0), Vector3(5, 0, 0));
    mSideBall2->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mSideBall2->setMass(1);
    mObjects.insert(ObjPair(mSideBall2, 1));
    //--------------------------------------------------------------
    
    //-------------------- Add Physics Forces ----------------------
    mpForceRegistry->add(mBall, new ParticleGravity(Vector3::gravity()));
    mpForceRegistry->add(mBallPair1, new ParticleGravity(Vector3::gravity()));
    mpForceRegistry->add(mBallPair2, new ParticleGravity(Vector3::gravity()));
    
    Vector3 anchorPos = mAnchor->getPosition();
    mpForceRegistry->add(mBall, new ParticleSpring(mAnchor, 5.0f, 1.0f));
    
    mpForceRegistry->add(mBallPair1, new ParticleSpring(mBallPair2, 5, 1));
    mpForceRegistry->add(mBallPair2, new ParticleSpring(mBallPair1, 5, 1));
    
    anchorPos = Vector3(4.5, 5, 1);
    mpForceRegistry->add(mBallPair1, new ParticleAnchorSpring(anchorPos, 5, 1));
    mpForceRegistry->add(mBallPair2, new ParticleAnchorSpring(anchorPos, 5, 1));
    
    anchorPos = Vector3(-8, 2, 0);
    mpForceRegistry->add(mSideBall, new ParticleGravity(Vector3::gravity()));
    mpForceRegistry->add(mSideBall, new ParticleAnchorSpring(anchorPos, 5, 1));
    //mpForceRegistry->add(mSideBall, new ParticleSpring(mSideBall2, 5, 1));
    
    mpForceRegistry->add(mSideBall2, new ParticleGravity(Vector3::gravity()));
    //mpForceRegistry->add(mSideBall2, new ParticleSpring(mSideBall, 5, 1));
    //--------------------------------------------------------------
    
    return true;
}

void Level1::Update(float deltaTime)
{
    RunPhysics(deltaTime);
}

void Level1::Draw()
{
    //do drawin) stuff here of objects
    mFloor->draw();
    
    Vector3 position;
    
    glPushMatrix();
    mpGame->getCamera()->applyCamera();
    
    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mAnchor->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mBall->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();
    
    glBegin(GL_LINES);
    position = Vector3(4.5, 5, 1);
    glVertex3f(position.X, position.Y, position.Z);
    position = mBallPair1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();
    
    glBegin(GL_LINES);
    position = Vector3(4.5, 5, 1);
    glVertex3f(position.X, position.Y, position.Z);
    position = mBallPair2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();
    
    glBegin(GL_LINES);
    position = mBallPair1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mBallPair2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();
    
    glBegin(GL_LINES);
    position = Vector3(-7, 2, 0);
    glVertex3f(position.X, position.Y, position.Z);
    position = mSideBall->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();
    
    glBegin(GL_LINES);
    position = mSideBall2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mSideBall->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();
    
    glColor3f(1, 1, 1);
    glPopMatrix();
    
    mAnchor->draw();
    mBall->draw();
    mBallPair1->draw();
    mBallPair2->draw();
    mSideBall->draw();
    mSideBall2->draw();
}

unsigned Level1::GenerateContacts()
{
    unsigned limit = mMaxContacts;
    ParticleContact* nextContact = mpContacts;
    
    for (ContactGenerators::iterator g = mContactGenerators.begin();
         g != mContactGenerators.end();
         ++g)
    {
        unsigned used = (*g)->addContact(nextContact, limit);
        limit -= used;
        nextContact += used;
        
        if (limit <= 0) break;
    }
    
    return mMaxContacts - limit;
}

void Level1::Integrate(real duration)
{
    // update objects
    mBall->update(duration);
    mBallPair1->update(duration);
    mBallPair2->update(duration);
    mSideBall->update(duration);
    mSideBall2->update(duration);
}

void Level1::RunPhysics(real duration)
{
    // update force registry
    mpForceRegistry->updateForces(duration);
    
    Integrate(duration);
    
    unsigned usedContacts = GenerateContacts();
    
    if (usedContacts)
    {
        mpResolver->setIterations(usedContacts * 2);
        mpResolver->resolveContacts(mpContacts, usedContacts, duration);
    }
}