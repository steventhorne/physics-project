#include "ParticleContact.h"

void ParticleContact::Resolve(real duration)
{
   resolveVelocity(duration);
   resolveInterpenetration(duration);
}

real ParticleContact::CalculateSeparatingVelocity() const
{
   Vector3 relativeVelocity = Objects[0]->getVelocity();



   if (Objects[1]) relativeVelocity -= Objects[1]->getVelocity();
   return relativeVelocity * ContactNormal;
}

void ParticleContact::resolveVelocity(real duration)
{
   real separatingVelocity = CalculateSeparatingVelocity();

   if (separatingVelocity > 0)
   {
      return;
   }

   real newSepVelocity = -separatingVelocity * Restitution;

   real deltaVelocity = newSepVelocity - separatingVelocity;

   real totalInverseMass = Objects[0]->getInverseMass();
   if (Objects[1]) totalInverseMass += Objects[1]->getInverseMass();

   if (totalInverseMass <= 0) return;

   real impulse = deltaVelocity / totalInverseMass;

   Vector3 impulsePerImass = ContactNormal * impulse;

   Objects[0]->setVelocity(Objects[0]->getVelocity() +
      impulsePerImass * Objects[0]->getInverseMass()
      );

   if (Objects[1])
   {
      Objects[1]->setVelocity((Objects[1]->getVelocity() + impulsePerImass * -Objects[1]->getInverseMass()) * -1.0f);
   }
}

void ParticleContact::resolveInterpenetration(real duration)
{
   if (Penetration <= 0) return;

   real totalInverseMass = Objects[0]->getInverseMass();
   if (Objects[1]) totalInverseMass += Objects[1]->getInverseMass();

   if (totalInverseMass <= 0) return;

   Vector3 movePerIMass =
      ContactNormal * (Penetration / totalInverseMass);

   particleMovement[0] = movePerIMass * Objects[0]->getInverseMass();
   if (Objects[1])
   {
      particleMovement[1] =
         movePerIMass * -Objects[1]->getInverseMass();
   }
   else
   {
      particleMovement[1].clear();
   }

   Objects[0]->setPosition(Objects[0]->getPosition() + particleMovement[0]);

   if (Objects[1])
   {
      Objects[1]->setPosition(Objects[1]->getPosition() + particleMovement[1]);
   }

   Penetration = 0;
}
