typedef float real;

#define PI 3.14

#define real_abs fabsf

#define real_sin sinf
#define real_cos cosf
#define real_exp expf
