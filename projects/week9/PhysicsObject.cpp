#include <math.h>
#include <stdio.h>
#include <cassert>
#include "PhysicsObject.h"
#include "Object.h"

PhysicsObject::PhysicsObject(Object *object)
{
   mpObject = object;
   mPosition = object->getPosition();
   mVelocity = Vector3(0, 0, 0);
   mAcceleration = Vector3(0, 0, 0);
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::PhysicsObject(Object* object, Vector3 position)
{
   mpObject = object;
   object->setPosition(position);
   mPosition = position;
   mVelocity = Vector3(0, 0, 0);
   mAcceleration = Vector3(0, 0, 0);
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::PhysicsObject(Object* object, Vector3 position, Vector3 velocity)
{
   mpObject = object;
   object->setPosition(position);
   mPosition = position;
   mVelocity = velocity;
   mAcceleration = Vector3(0, 0, 0);
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::PhysicsObject(Object* object, Vector3 position, Vector3 velocity, Vector3 acceleration)
{
   mpObject = object;
   object->setPosition(position);
   mPosition = position;
   mVelocity = velocity;
   mAcceleration = acceleration;
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::~PhysicsObject()
{
   delete mpObject;
   mpObject = NULL;
}

void PhysicsObject::setScale( Vector3 scale )
{
   mpObject->setScale(scale);
}

Vector3 PhysicsObject::getScale()
{
   return mpObject->getScale();
}

void PhysicsObject::setMass(float mass)
{
   if (mass != 0)
      mInverseMass = 1 / mass;
}

void PhysicsObject::setInverseMass(float imass)
{
   if (imass == 0)
   {
      mVelocity = Vector3(0, 0, 0);
      mAcceleration = Vector3(0, 0, 0);
   }

   mInverseMass = imass;
}

void PhysicsObject::addForce( Vector3 force )
{
   mForceAccumulator += force;
}

void PhysicsObject::update(real deltaTime)
{
   if (mInverseMass <= 0) return;

   assert(deltaTime > 0.0);

   Vector3 resultingAcc = mAcceleration + (mForceAccumulator * mInverseMass);

   mVelocity += resultingAcc * deltaTime;

   mVelocity *= pow((float)DAMPENING, deltaTime);

   mPosition += mVelocity * deltaTime;

   clearAccumulator();

   // apply position to inner object.
   mpObject->setPosition(mPosition);
   //fprintf(stderr, "%f, %f, %f\n", mPosition.X, mPosition.Y, mPosition.Z);
}

void PhysicsObject::draw()
{
   mpObject->draw();
}

void PhysicsObject::clearAccumulator()
{
   mForceAccumulator.clear();
}
