#pragma once
#include "Precision.h"
#include "PhysicsObject.h"

class ParticleForceGenerator
{
   public:
   virtual void updateForce(PhysicsObject *object, real duration) = 0;
};
