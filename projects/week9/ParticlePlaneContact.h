#include "ParticleContactGenerator.h"
#include "PhysicsObject.h"
#include "Plane.h"

class ParticlePlaneContact : public ParticleContactGenerator
{
   public:
   PhysicsObject* Objects;
   Plane* PlaneObj;

   real MinLength;
   real Restitution;
   Vector3 IntersectPosition;

   virtual unsigned addContact(ParticleContact* contact, unsigned limit) const;

   private:
   real currentLength() const;
};
