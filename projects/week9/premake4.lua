   -- A project defines one build target
   project "CollisionsPart2"
      kind "ConsoleApp"
      language "C++"
      targetname "collisionsp2"
      files { "**.h", "**.cpp" }
      includedirs { "./../../libs/glfw/include" }
      links { "glfw" }

      if os.get() == "macosx" then
         links { "OpenGL.framework", "Cocoa.framework", "CoreFoundation.framework", "IOKit.framework" }
      elseif os.get() == "windows" then
         links { "opengl32", "glu32" }
      end
