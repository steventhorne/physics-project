#pragma once
#include <vector>
#include <map>
#include "Game.h"
#include "ParticleForceRegistry.h"
#include "ParticleContactResolver.h"
#include "ParticleContactGenerator.h"
#include "ParticleContact.h"
#include "PhysicsObject.h"
#include "Plane.h"

using namespace std;

class World
{
public:
    World(Game* game);
    virtual ~World();
    
    virtual bool Init() = 0;
    virtual void Update(float deltaTime) = 0;
    virtual void Draw() = 0;
    
    virtual unsigned GenerateContacts() = 0;
    
    virtual void Integrate(real duration) = 0;
    virtual void RunPhysics(real duration) = 0;
    
protected:
    Game* mpGame;
    
    typedef vector<ParticleContactGenerator*> ContactGenerators;
    ParticleForceRegistry* mpForceRegistry;
    ParticleContactResolver* mpResolver;
    ContactGenerators mContactGenerators;
    ParticleContact* mpContacts;
    unsigned mMaxContacts;
    
    typedef map<PhysicsObject*, real> ObjMap;
    typedef pair<PhysicsObject*, real> ObjPair;
    ObjMap mObjects;
};