#pragma once
#include "PhysicsObject.h"

class ParticleContact
{
   public:
   PhysicsObject* Objects[2];

   real Restitution;
   real Penetration;

   Vector3 ContactNormal;

   void Resolve(real duration);

   real CalculateSeparatingVelocity() const;

   private:
   void resolveVelocity(real duration);

   void resolveInterpenetration(real duration);

   Vector3 particleMovement[1];
};
