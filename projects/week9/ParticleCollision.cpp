#include "ParticleCollision.h"

unsigned ParticleCollision::addContact(ParticleContact* contact, unsigned limit) const
{
   real length = currentLength();

   if (length > MinLength)
   {
      return 0;
   }

   contact->Objects[0] = Objects[0];
   contact->Objects[1] = Objects[1];

   Vector3 normal = Objects[0]->getPosition() - Objects[1]->getPosition();
   normal.normalize();
   contact->ContactNormal = normal;

   contact->Penetration = MinLength - length;
   contact->Restitution = Restitution;

   return 1;
}

real ParticleCollision::currentLength() const
{
   Vector3 relativePos = Objects[1]->getPosition() -
                         Objects[0]->getPosition();

   return relativePos.magnitude();
}
