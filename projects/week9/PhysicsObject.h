#pragma once
#include "Precision.h"
#include "Vector3.h"

#define DAMPENING 0.999

class Object;

class PhysicsObject
{
   public:
   PhysicsObject(Object* object);
   PhysicsObject(Object* object, Vector3 position);
   PhysicsObject(Object* object, Vector3 position, Vector3 velocity);
   PhysicsObject(Object* object, Vector3 position, Vector3 velocity, Vector3 acceleration);

   ~PhysicsObject();

   void setMass(float mass);
   float getMass() { if (hasFiniteMass()) return 1/mInverseMass; else return 0; };

   void setInverseMass(float imass);
   float getInverseMass() { return mInverseMass; };

   bool hasFiniteMass() { return mInverseMass != 0; };

   void setScale( Vector3 scale );

   //avoid using these!
   void setPosition( Vector3 position ) { mPosition = position; };
   void setVelocity( Vector3 velocity ) { mVelocity = velocity; };
   void setAcceleration( Vector3 acceleration ) { mAcceleration = acceleration; };

   Vector3 getScale();
   Vector3 getPosition() { return mPosition; };
   Vector3 getVelocity() { return mVelocity; };
   Vector3 getAcceleration() { return mAcceleration; };

   void addForce( Vector3 force );
   void clearForces();

   void update(real deltaTime);

   void draw();

   void clearAccumulator();

   Object* getObject() { return mpObject; };

   private:
   Object* mpObject;
   Vector3 mPosition;
   Vector3 mVelocity;
   Vector3 mAcceleration;
   Vector3 mForceAccumulator;
   float mInverseMass;
};
