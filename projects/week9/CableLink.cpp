#include "CableLink.h"

unsigned CableLink::addContact(ParticleContact* contact, unsigned limit) const
{
   real length = currentLength();

   if (length < MaxLength)
   {
      return 0;
   }

   contact->Objects[0] = Objects[0];
   contact->Objects[1] = Objects[1];

   Vector3 normal = Objects[1]->getPosition() - Objects[0]->getPosition();
   normal.normalize();
   contact->ContactNormal = normal;

   contact->Penetration = length - MaxLength;
   contact->Restitution = Restitution;

   return 1;
}
