#pragma once
#include "Vector3.h"

class Game;

class Object
{
   public:
   Object();
   Object( Game* pGame );
   Object( Game* pGame, Vector3 color );
   Object( Game* pGame, Vector3 color, Vector3 position );
   Object( Game* pGame, Vector3 color, Vector3 position, Vector3 rotation );
   Object( Game* pGame, Vector3 color, Vector3 position, Vector3 rotation, Vector3 scale );
   virtual ~Object() {};

   void setPosition( Vector3 position ) { mPosition = position; };
   Vector3 getPosition() { return mPosition; };

   void setRotation( Vector3 rotation ) { mRotation = rotation; };
   Vector3 getRotation() { return mRotation; };

   void setScale( Vector3 scale ) { mScale = scale; };
   Vector3 getScale() { return mScale; };

   void setSelfLit(bool emissive) { mSelfLit = emissive; };
   bool isSelfLit() { return mSelfLit; };

   void setSelfLightAmount(float amount) { mLightAmount = amount; };
   float getSelfLightAmount() { return mLightAmount; };

   virtual void draw() {};

   protected:
   Game* mpGame;
   Vector3 mColor;
   Vector3 mPosition;
   Vector3 mRotation;
   Vector3 mScale;
   bool mSelfLit;
   float mLightAmount;
};
