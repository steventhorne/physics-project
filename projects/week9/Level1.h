#pragma once
#include <GL/glfw.h>
#include "World.h"
#include "Plane.h"
#include "PhysicsObject.h"

class Level1 : public World
{
public:
    Level1(Game* game);
    ~Level1();
    
    bool Init();
    void Update(float deltaTime);
    void Draw();
    
    unsigned GenerateContacts();
    
    void Integrate(real duration);
    void RunPhysics(real duration);
    
private:
    Plane* mFloor;
    PhysicsObject* mAnchor;
    PhysicsObject* mBall;
    PhysicsObject* mBallPair1;
    PhysicsObject* mBallPair2;
    PhysicsObject* mSideBall;
    PhysicsObject* mSideBall2;
    
};