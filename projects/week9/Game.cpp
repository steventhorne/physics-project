#include <iostream>
#include <math.h>
#include "Game.h"
#include "Input.h"
#include "Camera.h"
#include "Level1.h"

using namespace std;

Game::Game()
{
    mpInputManager = new Input();
    mpCamera = new Camera(this, Vector3(0, 5, 20), Vector2(0, 0));
    
    mLevel = new Level1(this);
}

Game::~Game()
{
    delete mpInputManager;
    mpInputManager = NULL;

    delete mpCamera;
    mpCamera = NULL;
}

bool Game::init()
{
    mpInputManager->Init();

    GLfloat sun_intensity[] = { 0.8, 0.8, 0.8, 1.0 };
    GLfloat sun_direction[] = { 0.0, 2.0, -1.0, 1.0 };
    GLfloat ambient_intensity[] = { 0.35, 0.35, 0.35, 1.0 };

    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_intensity);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, sun_direction);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, sun_intensity);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    mDeltaTime = 0.0;
    mLastTime = 0;
    
    //-- init level
    mLevel->Init();
    //--

    //--------------------- Add Contacts ---------------------------
    /*ParticlePlaneContact* collision = new ParticlePlaneContact();
    collision->Objects[0] = mBallPair1;
    collision->Objects[1] = mBallPair2;
    collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
    collision->Restitution = 0.99;
    mContactGenerators.push_back(collision);

    collision = new ParticleCollision();
    collision->Objects[0] = mSideBall;
    collision->Objects[1] = mSideBall2;
    collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
    collision->Restitution = 0.9;
    mContactGenerators.push_back(collision);*/

    /*collision = new ParticleCollision();
    collision->Objects[0] = mSideBall2;
    collision->MinLength = collision->Objects[0]->getScale().X;
    collision->Restitution = 1.0;
    mContactGenerators.push_back(collision);*/ // dont add this back

    /*ObjMap::iterator it;
    for (it = mObjects.begin(); it != mObjects.end(); ++it)
    {
       //doop
       ObjMap::iterator otherIt = it;
       ++otherIt;
       for (; otherIt != mObjects.end(); ++otherIt)
       {
          if ((*it) != (*otherIt))
          {
             ParticleCollision* collision = new ParticleCollision();
             collision->Objects[0] = (*it).first;
             collision->Objects[1] = (*otherIt).first;
             collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
             collision->Restitution = ((*it).second * (*otherIt).second) / 2;
             mContactGenerators.push_back(collision);
          }
       }
    }*/

    /*ParticleRod* rod = new ParticleRod();
    rod->Objects[0] = mBallPair1;
    rod->Objects[1] = mBallPair2;
    rod->Length = 7;
    mContactGenerators.push_back(rod);

    CableLink* cable = new CableLink();
    cable->Objects[0] = mSideBall;
    cable->Objects[1] = mSideBall2;
    cable->MaxLength = 4;
    cable->Restitution = 1;
    mContactGenerators.push_back(cable);*/
    
    //--------------------------------------------------------------

    return true;
}

void Game::update()
{
    if (mLastTime == 0)
    {
        mLastTime = glfwGetTime();
        return;
    }

    mpInputManager->Update();

    real deltaTime = glfwGetTime() - mLastTime;
    
    //-- update level
    mLevel->Update(deltaTime);
    //--

    mpCamera->update(deltaTime);

    mLastTime = glfwGetTime();

    draw();
}

void Game::draw()
{
    glClearColor( 0.2f, 0.2f, 0.2f, 1.0f ); // clear background to gray
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();
    
    //draw level here
    mLevel->Draw();
    //---------------

    glPushMatrix();

    mpCamera->applyCamera();

    glPopMatrix();

    mpCamera->draw();

    glfwSwapBuffers();
}
