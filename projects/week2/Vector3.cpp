#include "Vector3.h"

Vector3::Vector3()
{
    mX = 0;
    mY = 0;
    mZ = 0;
}

Vector3::Vector3( float x, float y, float z )
{
    mX = x;
    mY = y;
    mZ = z;
}

Vector3 Vector3::operator+( const Vector3& rhs ) const
{
    Vector3 newVector = Vector3(mX + rhs.mX, mY + rhs.mY, mZ + rhs.mZ);

    return newVector;
}

void Vector3::operator+=( const Vector3& rhs )
{
   mX += rhs.mX;
   mY += rhs.mY;
   mZ += rhs.mZ;
}

Vector3 Vector3::operator-( const Vector3& rhs ) const
{
    Vector3 newVector = Vector3(mX - rhs.mX, mY - rhs.mY, mZ - rhs.mZ);

    return newVector;
}

Vector3 Vector3::operator%( const Vector3& rhs ) const
{
    Vector3 crossVec = Vector3((mY*rhs.mZ) - (mZ*rhs.mY), (mZ*rhs.mX) - (mX*rhs.mZ), (mX*rhs.mY) - (mY*rhs.mX));

    return crossVec;
}

float Vector3::operator*( const Vector3& rhs ) const
{
   float dot = (rhs.mX * mX) + (mY * rhs.mY) + (mZ * rhs.mZ);

   return dot;
}

Vector3 Vector3::operator*(const float& rhs) const
{
   return Vector3(mX*rhs, mY*rhs, mZ*rhs);
}

void Vector3::normalize()
{
    float x2 = mX*mX;
    float y2 = mY*mY;
    float z2 = mZ*mZ;

    float mag = fast_sqrt(x2 + y2 + z2);

    if (mag == 0)
        return;

    mX /= mag;
    mY /= mag;
    mZ /= mag;
}

Vector3 Vector3::normalized()
{
    Vector3 newVector = Vector3(mX, mY, mZ);

    newVector.normalize();

    return newVector;
}

Vector3 Vector3::cross(Vector3 other)
{
    Vector3 crossVec = Vector3((Y()*other.Z()) - (Z()*other.Y()), (Z()*other.X()) - (X()*other.Z()), (X()*other.Y()) - (Y()*other.X()));
    return crossVec;
}

float Vector3::fast_sqrt( float number )
{
        long i;
        float x2, y;
        const float threehalfs = 1.5F;

        x2 = number * 0.5F;
        y  = number;
        i  = * ( long * ) &y;                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
//      y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

        return y;
}
