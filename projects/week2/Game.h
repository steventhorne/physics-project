#pragma once
#include <GL/glfw.h>
#include "PhysicsObject.h"
#include "Plane.h"
#include "Line.h"

class Input;
class Camera;

class Game
{
    public:
    Game();
    ~Game();

    Input* getInputManager() { return mpInputManager; };
    Camera* getCamera() { return mpCamera; };

    bool init();
    void update();

    void draw();

    private:
    Input* mpInputManager;
    Camera* mpCamera;

    PhysicsObject* sphere;
    Vector3 spawnPoint;

    Plane* floor;

    Line* direction;
    float launchForce;
    bool launched;
    bool lastSpace;

    double mDeltaTime;
    double mLastTime;
};
