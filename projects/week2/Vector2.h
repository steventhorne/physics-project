#pragma once

class Vector2
{
    public:
    Vector2(float x, float y);
    Vector2() {};
    ~Vector2() {};

    Vector2 operator+( const Vector2& rhs ) const;
    Vector2 operator-( const Vector2& rhs ) const;
    float operator*( const Vector2& rhs ) const;
    Vector2 operator*( const float& rhs ) const;

    float X() { return mX; };
    float Y() { return mY; };

    void setX(float newX) { mX = newX; };
    void setY(float newY) { mY = newY; };

    void normalize();
    Vector2 normalized();

    float fast_sqrt( float number );

    private:
    float mX;
    float mY;
};
