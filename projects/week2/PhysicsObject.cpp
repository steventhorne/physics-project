#include <math.h>
#include <stdio.h>
#include "PhysicsObject.h"
#include "Object.h"

PhysicsObject::PhysicsObject(Object *object)
{
   mpObject = object;
   mPosition = object->getPosition();
   mVelocity = Vector3(0, 0, 0);
   mAcceleration = Vector3(0, 0, 0);
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::PhysicsObject(Object* object, Vector3 position)
{
   object->setPosition(position);
   mPosition = position;
   mVelocity = Vector3(0, 0, 0);
   mAcceleration = Vector3(0, 0, 0);
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::PhysicsObject(Object* object, Vector3 position, Vector3 velocity)
{
   object->setPosition(position);
   mPosition = position;
   mVelocity = velocity;
   mAcceleration = Vector3(0, 0, 0);
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::PhysicsObject(Object* object, Vector3 position, Vector3 velocity, Vector3 acceleration)
{
   object->setPosition(position);
   mPosition = position;
   mVelocity = velocity;
   mAcceleration = acceleration;
   mInverseMass = 0;
   mForceAccumulator = Vector3(0, 0, 0);
}

PhysicsObject::~PhysicsObject()
{
   delete mpObject;
   mpObject = NULL;
}

void PhysicsObject::setMass(float mass)
{
   if (mass != 0)
      mInverseMass = 1 / mass;
}

void PhysicsObject::setInverseMass(float imass)
{
   if (imass == 0)
   {
      mVelocity = Vector3(0, 0, 0);
      mAcceleration = Vector3(0, 0, 0);
   }

   mInverseMass = imass;
}

void PhysicsObject::addForce( Vector3 force )
{
   mForceAccumulator += force;
}

void PhysicsObject::update(float deltaTime)
{
   if (mInverseMass <= 0) return;

   //clear force accumulator
   mForceAccumulator = Vector3(0, 0, 0);

   float dt = pow((float)DAMPENING, deltaTime);

   Vector3 realAcc = mAcceleration + Vector3::gravity() + (mForceAccumulator* mInverseMass);
   //change velocity and position
   mVelocity = mVelocity*dt + realAcc*deltaTime;
   mPosition = mPosition + (mVelocity * deltaTime);

   // apply position to inner object.
   mpObject->setPosition(mPosition);
}

void PhysicsObject::draw()
{
   mpObject->draw();
}
