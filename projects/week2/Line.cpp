#include "Line.h"

Line::Line(Vector3 begin, Vector3 end)
{
    mBeginPoint = begin;
    mEndPoint = end;
}

void Line::draw()
{
    glBegin(GL_LINES);
    glVertex3f( mBeginPoint.X(), mBeginPoint.Y(), mBeginPoint.Z() );
    glVertex3f( mEndPoint.X(), mEndPoint.Y(), mEndPoint.Z() );
    glEnd();
}
