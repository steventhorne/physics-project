#pragma once
#include "Vector3.h"
#include "Object.h"

class Game;

class Plane : public Object
{
    public:
    Plane( Game* pGame );
    Plane( Game* pGame, Vector3 position );
    Plane( Game* pGame, Vector3 position, Vector3 rotation );
    Plane( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale );
    ~Plane() {};

    void draw();
};
