#pragma once
#include <GL/glfw.h>
#include "Vector2.h"
#include "Vector3.h"

class Game;

class Camera
{
    public:
        Camera(Game* pGame);
        Camera(Game* pGame, Vector3 p);
        Camera(Game* pGame, Vector3 p, Vector2 r);
        Camera(Game* pGame, Vector3 p, Vector3 r);
        ~Camera() {};

        void update( double deltaTime );

        void applyCameraTranslation();
        void applyCameraRotation();
        void applyCamera();

        void draw(); //draws camera specific items.

    private:
        Game* mpGame;
        float xpos, ypos, zpos;
        float xrot, yrot;
        int mLastScroll;
};
