#pragma once
#include "Vector3.h"

#define DAMPENING 0.999

class Object;

class PhysicsObject
{
   public:
   PhysicsObject(Object* object);
   PhysicsObject(Object* object, Vector3 position);
   PhysicsObject(Object* object, Vector3 position, Vector3 velocity);
   PhysicsObject(Object* object, Vector3 position, Vector3 velocity, Vector3 acceleration);

   ~PhysicsObject();

   void setMass(float mass);

   void setInverseMass(float imass);
   float getInverseMass() { return mInverseMass; };

   //avoid using these!
   void setPosition( Vector3 position ) { mPosition = position; };
   void setVelocity( Vector3 velocity ) { mVelocity = velocity; };
   void setAcceleration( Vector3 acceleration ) { mAcceleration = acceleration; };

   Vector3 getPosition() { return mPosition; };
   Vector3 getVelocity() { return mVelocity; };
   Vector3 getAcceleration() { return mAcceleration; };

   void addForce( Vector3 force );
   void clearForces();

   void update(float deltaTime);

   void draw();

   private:
   Object* mpObject;
   Vector3 mPosition;
   Vector3 mVelocity;
   Vector3 mAcceleration;
   Vector3 mForceAccumulator;
   float mInverseMass;
};
