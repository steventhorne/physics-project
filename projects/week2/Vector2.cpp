#include "Vector2.h"

Vector2::Vector2(float x, float y)
{
    mX = x;
    mY = y;
}

Vector2 Vector2::operator+( const Vector2& rhs ) const
{
   Vector2 newVec = Vector2(mX + rhs.mX, mY + rhs.mY);

   return newVec;
}

Vector2 Vector2::operator-( const Vector2& rhs ) const
{
   Vector2 newVec = Vector2(mX - rhs.mX, mY - rhs.mY);

   return newVec;
}

float Vector2::operator*( const Vector2& rhs ) const
{
   float dot = (mX * rhs.mX) + (mY * rhs.mY);

   return dot;
}

Vector2 Vector2::operator*( const float& rhs ) const
{
   return Vector2(mX * rhs, mY * rhs);
}

void Vector2::normalize()
{
    float x2 = mX*mX;
    float y2 = mY*mY;

    float mag = fast_sqrt(x2 + y2);

    if (mag == 0) return;

    mX /= mag;
    mY /= mag;
}

Vector2 Vector2::normalized()
{
    Vector2 newVector = Vector2(mX, mY);

    newVector.normalize();

    return newVector;
}

// Quake inverse sqrt: http://en.wikipedia.org/wiki/Fast_inverse_square_root
float Vector2::fast_sqrt( float number )
{
        long i;
        float x2, y;
        const float threehalfs = 1.5F;

        x2 = number * 0.5F;
        y  = number;
        i  = * ( long * ) &y;                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
//      y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

        return y;
}
