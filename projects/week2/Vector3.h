#pragma once

class Vector3
{
    public:
    Vector3();
    Vector3( float x, float y, float z );
    ~Vector3() {};

    Vector3 operator+(const Vector3& rhs) const;
    void operator+=(const Vector3& rhs);
    Vector3 operator-(const Vector3& rhs) const;
    Vector3 operator%(const Vector3& rhs) const;
    float operator*(const Vector3& rhs) const;
    Vector3 operator*(const float& rhs) const;

    float X() { return mX; };
    float Y() { return mY; };
    float Z() { return mZ; };

    void setX( float x ) { mX = x; };
    void setY( float y ) { mY = y; };
    void setZ( float z ) { mZ = z; };

    static Vector3 gravity() { return Vector3(0, -15, 0); };

    void normalize();
    Vector3 normalized();

    Vector3 cross(Vector3 other);

    float fast_sqrt( float number );

    private:
    float mX;
    float mY;
    float mZ;
};
