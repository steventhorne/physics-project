#include <iostream>
#include "Game.h"
#include "Input.h"
#include "Camera.h"
#include "Sphere.h"

using namespace std;

Game::Game()
{
    mpInputManager = new Input();
    mpCamera = new Camera(this, Vector3(0, 0, -60));
}

Game::~Game()
{
    delete mpInputManager;
    mpInputManager = NULL;

    delete mpCamera;
    mpCamera = NULL;

    delete sphere;
    sphere = NULL;

    delete floor;
    floor = NULL;

    delete direction;
    direction = NULL;
}

bool Game::init()
{
    mpInputManager->Init();

    GLfloat sun_intensity[] = { 0.8, 0.8, 0.8, 1.0 };
    GLfloat sun_direction[] = { 0.0, 2.0, -1.0, 1.0 };
    GLfloat ambient_intensity[] = { 0.35, 0.35, 0.35, 1.0 };

    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_intensity);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, sun_direction);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, sun_intensity);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    mDeltaTime = 0.0;
    mLastTime = glfwGetTime();

    spawnPoint = Vector3(0, 0, 0);
    sphere = new PhysicsObject(new Sphere ( this, spawnPoint, Vector3(1, 1, 1) ));
    sphere->setInverseMass(0);

    floor = new Plane( this, Vector3(0, -10, 0), Vector3(0, 0, 0), Vector3(50, 50, 50) );

    direction = new Line(sphere->getPosition(), Vector3(0, 1.414, 1.414));
    launchForce = 500;
    launched = false;
    lastSpace = false;

    return true;
}

void Game::update()
{
    mpInputManager->Update();

    float deltaTime = glfwGetTime() - mLastTime;

    mpCamera->update(deltaTime);

    sphere->update(deltaTime);

    //change line to reflect controls
    if (mpInputManager->isKeyPressed(GLFW_KEY_UP))
    {
       launchForce += 20;
       cout << "Force: " << launchForce << endl;
       //printf("Force: %d\n", launchForce);
    }

    if (mpInputManager->isKeyPressed(GLFW_KEY_DOWN))
    {
       launchForce -= 20;
       cout << "Force: " << launchForce << endl;
       //printf("Force: %d\n", launchForce);
    }

    if (!lastSpace && mpInputManager->isKeyPressed(GLFW_KEY_SPACE))
    {
       if (!launched)
       {
         sphere->setMass(1);
         sphere->addForce(Vector3(0, 1.414, 1.414) * launchForce);
         launched = true;
       }
       else
       {
         sphere->setPosition(spawnPoint);
         sphere->setInverseMass(0);
         launched = false;
       }
    }
    lastSpace = mpInputManager->isKeyPressed(GLFW_KEY_SPACE);

    mLastTime = glfwGetTime();
}

void Game::draw()
{
    glClearColor( 0.2f, 0.2f, 0.2f, 0.2f ); // clear background to gray
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    //do drawing stuff here
    floor->draw();
    sphere->draw();

    glPushMatrix();

    mpCamera->applyCamera();

    GLfloat sun_direction[] = { 0.0, 2.0, -1.0, 1.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, sun_direction);

    glPopMatrix();

    mpCamera->draw();

    glfwSwapBuffers();
}
