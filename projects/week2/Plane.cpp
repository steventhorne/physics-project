#include "Plane.h"
#include "Camera.h"
#include "Game.h"

Plane::Plane( Game* pGame )
   :Object(pGame)
{

}

Plane::Plane( Game* pGame, Vector3 position )
   :Object(pGame, position)
{

}

Plane::Plane( Game* pGame, Vector3 position, Vector3 rotation )
   :Object(pGame, position, rotation)
{

}

Plane::Plane( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale )
   :Object(pGame, position, rotation, scale)
{

}

void Plane::draw()
{
    glPushMatrix();

    mpGame->getCamera()->applyCamera();

    glTranslated(mPosition.X(), mPosition.Y(), mPosition.Z());
    glRotatef(mRotation.X(), 1, 0, 0);
    glRotatef(mRotation.Y(), 0, 1, 0);
    glRotatef(mRotation.Z(), 0, 0, 1);

    glBegin(GL_QUADS);

    //far left
    Vector3 point1 = Vector3(-mScale.X() / 2, 0, mScale.Z() / 2);
    //front left
    Vector3 point2 = Vector3(-mScale.X() / 2, 0, -mScale.Z() / 2);
    //front right
    Vector3 point3 = Vector3(mScale.X() / 2, 0, -mScale.Z() / 2);
    //far right
    Vector3 point4 = Vector3(mScale.X() / 2, 0, mScale.Z() / 2);

    Vector3 normal1 = ((point4 - point1)%((point2 - point1))).normalized();
    Vector3 normal2 = ((point1 - point2)%((point3 - point2))).normalized();
    Vector3 normal3 = ((point2 - point3)%((point4 - point3))).normalized();
    Vector3 normal4 = ((point3 - point4)%((point1 - point4))).normalized();

    glNormal3f(normal1.X(), normal1.Y(), normal1.Z());
    glVertex3f(-mScale.X() / 2, 0, mScale.Z() / 2);
    glNormal3f(normal2.X(), normal2.Y(), normal2.Z());
    glVertex3f(-mScale.X() / 2, 0, -mScale.Z() / 2);
    glNormal3f(normal3.X(), normal3.Y(), normal3.Z());
    glVertex3f(mScale.X() / 2, 0, -mScale.Z() / 2);
    glNormal3f(normal4.X(), normal4.Y(), normal4.Z());
    glVertex3f(mScale.X() / 2, 0, mScale.Z() / 2);

    glEnd();

    glPopMatrix();
}
