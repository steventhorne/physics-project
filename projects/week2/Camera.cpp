#include <math.h>
#include <iostream>
#include "Camera.h"
#include "Game.h"
#include "Input.h"

using namespace std;

Camera::Camera(Game* pGame)
{
    mpGame = pGame;
    xpos = ypos = xrot = yrot = 0.0;
    zpos = 0;
    mLastScroll = 0;
}

Camera::Camera(Game* pGame, Vector3 p)
{
    mpGame = pGame;
    xpos = -p.X();
    ypos = -p.Y();
    zpos = -p.Z();
    xrot = 0;
    yrot = 0;
    mLastScroll = 0;
}

Camera::Camera(Game* pGame, Vector3 p, Vector2 r)
{
    mpGame = pGame;
    xpos = -p.X();
    ypos = -p.Y();
    zpos = -p.Z();
    xrot = r.X();
    yrot = r.Y();
    mLastScroll = 0;
}

Camera::Camera(Game* pGame, Vector3 p, Vector3 r)
{
    mpGame = pGame;
    xpos = -p.X();
    ypos = -p.Y();
    zpos = -p.Z();
    xrot = r.X();
    yrot = r.Y();
    mLastScroll = 0;
}

void Camera::update(double deltaTime)
{
    if (mpGame->getInputManager()->isKeyPressed(GLFW_KEY_ENTER)) //temporarily using this for debug info... press enter to see debug info.
    {
        //cout << "xpos: " << xpos << ", ypos: " << ypos << ", zpos: " << zpos << endl;
        cout << "deltaTime: " << deltaTime << endl;
    }

    if (mpGame->getInputManager()->isKeyPressed('R'))
    {
        xrot = 0;
        yrot = 0;
    }

    if (mpGame->getInputManager()->mouseButtonDown(GLFW_MOUSE_BUTTON_RIGHT))
    {
        xrot += mpGame->getInputManager()->mouseDiff().Y();
        if (xrot > 360)
            xrot -= 360;

        yrot += mpGame->getInputManager()->mouseDiff().X();
        if (yrot < -360)
            yrot += 360;
    }

    if (mpGame->getInputManager()->isKeyPressed('Q'))
    {
        yrot -= 1;
        if (yrot < -360)
            yrot += 360;
    }

    if (mpGame->getInputManager()->isKeyPressed('E'))
    {
        yrot += 1;
        if (yrot > 360)
            yrot -= 360;
    }

    if (mpGame->getInputManager()->isKeyPressed('W'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos += float(sin(yrotrad));
        zpos -= float(cos(yrotrad));
        ypos -= float(sin(xrotrad));
    }
    else if (mpGame->getInputManager()->isKeyPressed('S'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos -= float(sin(yrotrad));
        zpos += float(cos(yrotrad));
        ypos += float(sin(xrotrad));
    }

    if (mpGame->getInputManager()->isKeyPressed('A'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos -= float(cos(yrotrad));
        zpos -= float(sin(yrotrad));
        //ypos -= float(sin(xrotrad));
    }
    else if(mpGame->getInputManager()->isKeyPressed('D'))
    {
        float xrotrad, yrotrad;
        yrotrad = (yrot / 180 * 3.141592654f);
        xrotrad = (xrot / 180 * 3.141592654f);
        xpos += float(cos(yrotrad));
        zpos += float(sin(yrotrad));
        //ypos += float(sin(xrotrad));
    }

    int scrollDiff = glfwGetMouseWheel() - mLastScroll;
    float xrotrad, yrotrad;
    yrotrad = (yrot / 180 * 3.141592654f);
    xrotrad = (xrot / 180 * 3.141592654f);
    xpos += float(sin(yrotrad)) * scrollDiff * 10 * deltaTime;
    zpos -= float(cos(yrotrad)) * scrollDiff * 10 * deltaTime;
    ypos -= float(sin(xrotrad)) * scrollDiff * 10 * deltaTime;

    mLastScroll = glfwGetMouseWheel();
}

void Camera::applyCameraTranslation()
{
    glTranslated(-xpos,-ypos,-zpos); //translate the screen to the position of our camera
}

void Camera::applyCameraRotation()
{
    glRotatef(xrot,1.0,0.0,0.0);  //rotate our camera on the x-axis (left and right)
    glRotatef(yrot,0.0,1.0,0.0);  //rotate our camera on the y-axis (up and down)
}

void Camera::applyCamera()
{
    // camera updates
    glRotatef(xrot,1.0,0.0,0.0);  //rotate our camera on the x-axis (left and right)
    glRotatef(yrot,0.0,1.0,0.0);  //rotate our camera on the y-axis (up and down)
    glTranslated(-xpos,-ypos,-zpos); //translate the screen to the position of our camera
}

void Camera::draw()
{
    float currentColor[4];
    glGetFloatv(GL_CURRENT_COLOR,currentColor);

    int myviewport[4];
    glGetIntegerv(GL_VIEWPORT, myviewport);

    GLfloat mv[16];
    //=getModelviewMatrix();
    glGetFloatv(GL_MODELVIEW_MATRIX, mv);
    mv[3]=mv[3+4]=mv[3+8]=0.0;
    glPushMatrix();
    glLoadIdentity();
    glViewport(0, 0, 100, 100);
    glMultMatrixf(mv); // orientation
    glTranslated(0, 0, -2.0);
    applyCameraRotation();

    glBegin(GL_LINES);

    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(1.0, 0.0, 0.0);

    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 1.0, 0.0);

    glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    glVertex3f(0.0, 0.0, 0.0);
    glVertex3f(0.0, 0.0, 1.0);

    glEnd();
    glPopMatrix();

    glViewport(myviewport[0], myviewport[1], myviewport[2], myviewport[3]);
    glColor4f(currentColor[0], currentColor[1], currentColor[2], currentColor[3]);
    // hud items
    //glPushMatrix();
    /*float currentColor[4];
    glGetFloatv(GL_CURRENT_COLOR,currentColor);

    //glMatrixMode(GL_MODELVIEW);
    //glLoadIdentity();

    int myviewport[4];
    glGetIntegerv(GL_VIEWPORT, myviewport);
    //glRotatef(xrot,1.0,0.0,0.0);
    //glRotatef(yrot,0.0,1.0,0.0);

    glViewport(0, 0, 100, 100);

    glBegin(GL_LINES);

    glColor4f(1.0f, 0.0f, 0.0f, 1.0f);
    glVertex3f(0, 0, 0);
    glVertex3f(25.0f, 0, 0);

    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 25.0f, 0);

    glColor4f(0.0f, 0.0f, 1.0f, 1.0f);
    glVertex3f(0, 0, 0);
    glVertex3f(0, 0, 25.0f);

    glEnd();

    glViewport(myviewport[0], myviewport[1], myviewport[2], myviewport[3]);
    glColor4f(currentColor[0], currentColor[1], currentColor[2], currentColor[3]);
    //glPopMatrix();
    */
}
