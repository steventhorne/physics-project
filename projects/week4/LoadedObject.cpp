#include <istream>
#include <fstream>
#include <stdio.h>
#include <sstream>
#include <iostream>
#include "LoadedObject.h"
#include "Game.h"
#include "Camera.h"

std::vector<string> &split(const string &s, char delim, vector<string> &elems)
{

    stringstream ss(s);

    string item;

    while(getline(ss, item, delim))
    {
        elems.push_back(item);
    }
    return elems;
}


std::vector<string> split(const std::string &s, char delim)
{
    vector<string> elems;
    split(s, delim, elems);
    return elems;
}

LoadedObject::LoadedObject( Game* pGame, char* filename )
   :Object(pGame)
{
   loadObject(filename);
}

LoadedObject::LoadedObject( Game* pGame, char* filename, Vector3 color )
   :Object(pGame, color)
{
   loadObject(filename);
}

LoadedObject::LoadedObject( Game* pGame, char* filename, Vector3 color, Vector3 position )
   :Object(pGame, color, position)
{
   loadObject(filename);
}

LoadedObject::LoadedObject( Game* pGame, char* filename, Vector3 color, Vector3 position, Vector3 rotation )
   :Object(pGame, color, position, rotation)
{
   loadObject(filename);
}

LoadedObject::LoadedObject( Game* pGame, char* filename, Vector3 color, Vector3 position, Vector3 rotation, Vector3 scale )
   :Object(pGame, color, position, rotation, scale)
{
   loadObject(filename);
}

void LoadedObject::draw()
{
   glPushMatrix();
   glShadeModel(GL_SMOOTH);

   mpGame->getCamera()->applyCamera();

   if (mSelfLit)
   {
      GLfloat lightColor0[] = {mLightAmount, mLightAmount, mLightAmount, 1.0f};
      GLfloat lightPos0[] = {0.0f, 0.0f, 0.0f, 1.0f};
      glLightfv(GL_LIGHT1, GL_AMBIENT, lightColor0);
      glLightfv(GL_LIGHT1, GL_POSITION, lightPos0);
   }

   glTranslated(mPosition.X(), mPosition.Y(), mPosition.Z());
   glRotatef(mRotation.X(), 1, 0, 0);
   glRotatef(mRotation.Y(), 0, 1, 0);
   glRotatef(mRotation.Z(), 0, 0, 1);
   glScalef(mScale.X(), mScale.Y(), mScale.Z());

   for (int i = 0; i < mFaces.size(); i++)
   {
      string line(mFaces[i]);

      vector<string> splitLine = split(line, ' ');

      if (splitLine.size() - 1 == 3)
         glBegin(GL_TRIANGLES);
      else
         glBegin(GL_QUADS);

      for (int j = 1; j < splitLine.size(); j++)
      {
         if (splitLine[j].find("/") != -1)
         {
            vector<string> possibles = split(splitLine[j], '/');
            if (possibles.size() >= 1)
            {
               if (possibles[0] != "")
               {
                  float index = atof(possibles[0].c_str());
                  Vector3 vertex = mVertices[index - 1];
                  glVertex3f(vertex.X(), vertex.Y(), vertex.Z());
               }
            }
            if (possibles.size() >= 2)
            {
               if (possibles[1] != "")
               {
                  float index = atof(possibles[1].c_str());
                  Vector2 tex = mTex[index - 1];

                  //do texture stuff later
               }
            }
            if (possibles.size() >= 3)
            {
               if (possibles[2] != "")
               {
                  float index = atof(possibles[2].c_str());
                  Vector3 normal = mNormals[index - 1];
                  glVertex3f(normal.X(), normal.Y(), normal.Z());
               }
            }
         }
         else
         {
            float index = atof(splitLine[j].c_str());
            Vector3 vertex = mVertices[index - 1];
            glVertex3f(vertex.X(), vertex.Y(), vertex.Z());
         }
      }

      glEnd();
   }

   glEnd();

   if (mSelfLit)
   {
      GLfloat lightColor0[] = {0.0f, 0.0f, 0.0f, 0.0f};
      GLfloat lightPos0[] = {0.0f, 0.0f, 0.0f, 1.0f};
      glLightfv(GL_LIGHT1, GL_AMBIENT, lightColor0);
      glLightfv(GL_LIGHT1, GL_POSITION, lightPos0);
   }

   glPopMatrix();
}

void LoadedObject::loadObject(char* file)
{
   ifstream read;
   string strLine = "";
   char* line;

   read.open(file);

   while (getline(read, strLine))
   {
      line = new char[strLine.length() + 1];
      strcpy(line, strLine.c_str());

      if (line[0] == 'v')
      {
         float u, v, x, y, z;
         switch(line[1])
         {
            case 't':
               sscanf(line, "vt %f %f", &u, &v);
               mTex.push_back(Vector2(u, v));
               break;
            case 'n':
               sscanf(line, "vn %f %f %f", &x, &y, &z);
               mNormals.push_back(Vector3(x, y, z));
               break;
            case ' ':
               sscanf(line, "v %f %f %f", &x, &y, &z);
               mVertices.push_back(Vector3(x, y, z));
               break;
            default:
               break;
         }
      }
      else if ( line[0] == 'f' )
      {
         mFaces.push_back(line);
      }
   }
}
