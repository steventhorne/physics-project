#pragma once
#include "ParticleForceGenerator.h"

class ParticleGravity : public ParticleForceGenerator
{
   public:
   ParticleGravity(PhysicsObject* object1, PhysicsObject* object2, real G);

   virtual void updateForce(PhysicsObject *object, real duration);
   private:
   Vector3 mGravity;
   PhysicsObject* mObject1;
   PhysicsObject* mObject2;
   real mG;
};
