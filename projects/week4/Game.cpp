#include <iostream>
#include <math.h>
#include "Game.h"
#include "Input.h"
#include "Camera.h"
#include "Sphere.h"
#include "ParticleGravity.h"
#include "ParticleDrag.h"
#include "LoadedObject.h"

using namespace std;

Game::Game()
{
    mpInputManager = new Input();
    mpCamera = new Camera(this, Vector3(0, 10, 0), Vector2(90, 0));
    mpForceRegistry = new ParticleForceRegistry();
    mPlanet = 0;
    mLastUp = false;
    mLastDown = false;
}

Game::~Game()
{
    delete mpInputManager;
    mpInputManager = NULL;

    delete mpCamera;
    mpCamera = NULL;

    delete mpForceRegistry;
    mpForceRegistry = NULL;

    delete mSun;
    mSun = NULL;

    delete mMercury;
    mMercury = NULL;

    delete mVenus;
    mVenus = NULL;

    delete mEarth;
    mEarth = NULL;

    delete mMars;
    mMars = NULL;

    delete mSaturn;
    mSaturn = NULL;

    delete mJupiter;
    mJupiter = NULL;

    delete mUranus;
    mUranus = NULL;

    delete mNeptune;
    mNeptune = NULL;
}

bool Game::init()
{
    mpInputManager->Init();

    GLfloat sun_intensity[] = { 0.8, 0.8, 0.8, 1.0 };
    GLfloat sun_direction[] = { 0.0, 200.0, -1.0, 1.0 };
    GLfloat ambient_intensity[] = { 0.35, 0.35, 0.35, 1.0 };

    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_intensity);

    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT0, GL_POSITION, sun_direction);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, sun_intensity);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    mDeltaTime = 0.0;
    mLastTime = 0;

    Vector3 initPos;
    Vector3 initVel;
    Vector3 scale;
    float mass;

    //---------------------- SUN ----------------------------
    initPos = Vector3(0, 0, 0);
    initVel = Vector3(0, 0, 0);
    scale = Vector3(0.1,0.1,0.1);//Vector3(0.046, 0.046, 0.046);
    mass = 332948.6;

    mSun = new PhysicsObject(new Sphere(this, Vector3(1, 1, 0)), initPos, initVel);
    mSun->setScale(scale);
    mSun->setMass(mass);
    mSun->getObject()->setSelfLit(true);
    mSun->getObject()->setSelfLightAmount(1.0f);
    //-------------------------------------------------------

    //------------------- MERCURY ---------------------------
    initPos = Vector3(0, 0, 0.338);
    initVel = Vector3(0.191, 0, 0);
    scale = /*Vector3(1.6308e-3, 1.6308e-3, 1.6308e-3);*/PLANET_SIZE;
    mass = 0.055293;

    mMercury = new PhysicsObject(new Sphere(this, Vector3(0.93, 0.27, 0)), initPos, initVel);
    mMercury->setScale(scale);
    mMercury->setMass(mass);
    //-------------------------------------------------------

    //------------------- VENUS -----------------------------
    initPos = Vector3(0, 0, 0.7278);
    initVel = Vector3(0.142, 0, 0);
    scale = /*Vector3(4.0343e-3, 4.0343e-3, 4.0343e-3);*/PLANET_SIZE;
    mass = 0.81528;

    mVenus = new PhysicsObject(new Sphere(this, Vector3(0, 0, 1)), initPos, initVel);
    mVenus->setScale(scale);
    mVenus->setMass(mass);
    //-------------------------------------------------------

    //-------------------- EARTH ----------------------------
    initPos = Vector3(0, 0, 1);
    initVel = Vector3(0.12, 0, 0); //au/wk = 0.12 au/d = 0.017142857
    scale = /*Vector3(4.2564e-5, 4.2564e-5, 4.2564e-5);*/PLANET_SIZE;
    mass = 1;

    mEarth = new PhysicsObject(new Sphere(this, Vector3(0, 1, 0)), initPos, initVel);
    mEarth->setScale(scale);
    mEarth->setMass(mass);
    //-------------------------------------------------------

    //---------------------- MOON ---------------------------
    initPos = Vector3(0, 0, 1.0024);
    initVel = Vector3(0.12411, 0, 0); //au/wk = 0.12411 au/d = 0.01773
    scale = /*Vector3( 1.1614e-5, 1.1614e-5, 1.1614e-5 );*/Vector3(0.005,0.005,0.005);
    mass = 0.0123;

    mMoon = new PhysicsObject(new Sphere(this, Vector3(0.5, 0.5, 0.5)), initPos, initVel);
    mMoon->setScale(scale);
    mMoon->setMass(mass);
    //-------------------------------------------------------

    //--------------------- MARS ----------------------------
    initPos = Vector3(0, 0, 1.383);
    initVel = Vector3(0.0973, 0, 0); //au/wk = 0.12 au/d = 0.017142857
    scale = /*Vector3(2.263e-3, 2.263e-3, 2.263e-3);*/PLANET_SIZE;
    mass = 0.10748;

    mMars = new PhysicsObject(new Sphere(this, Vector3(1, 0, 0)), initPos, initVel);
    mMars->setScale(scale);
    mMars->setMass(mass);
    //-------------------------------------------------------

    //--------------------- JUPITER -------------------------
    initPos = Vector3(0, 0, 5.076);
    initVel = Vector3(0.0528, 0, 0); //au/wk = 0.12 au/d = 0.017142857
    scale = /*Vector3(4.6239e-2, 4.6239e-2, 4.62394e-2);*/PLANET_SIZE;
    mass = 317.94;

    mJupiter = new PhysicsObject(new Sphere(this, Vector3(1, 0.65, 0)), initPos, initVel);
    mJupiter->setScale(scale);
    mJupiter->setMass(mass);
    //-------------------------------------------------------

    //--------------------- SATURN --------------------------
    initPos = Vector3(0, 0, 9.818);
    initVel = Vector3(0.039, 0, 0); //au/wk = 0.12 au/d = 0.017142857
    scale = /*Vector3(3.8313e-2, 3.8313e-2, 3.8313e-2);*/PLANET_SIZE;
    mass = 95.191;

    mSaturn = new PhysicsObject(new Sphere(this, Vector3(0, 0.82, 0.82)), initPos, initVel);
    mSaturn->setScale(scale);
    mSaturn->setMass(mass);
    //-------------------------------------------------------

    //--------------------- URANUS --------------------------
    initPos = Vector3(0, 0, 20.07);
    initVel = Vector3(0.0275, 0, 0); //au/wk = 0.12 au/d = 0.017142857
    scale = /*Vector3(1.6889e-2, 1.6889e-2, 1.6889e-2);*/PLANET_SIZE;
    mass = 14.505;

    mUranus = new PhysicsObject(new Sphere(this), initPos, initVel);
    mUranus->setScale(scale);
    mUranus->setMass(mass);
    //-------------------------------------------------------

    //--------------------- NEPTUNE -------------------------
    initPos = Vector3(0, 0, 30.04);
    initVel = Vector3(0.022, 0, 0); //au/wk = 0.12 au/d = 0.017142857
    scale = /*Vector3(1.6412e-2, 1.6412e-2, 1.6412e-2);*/PLANET_SIZE;
    mass = 17.21;

    mNeptune = new PhysicsObject(new Sphere(this), initPos, initVel);
    mNeptune->setScale(scale);
    mNeptune->setMass(mass);
    //-------------------------------------------------------

    //--------------- add gravitational forces --------------
    mpForceRegistry->add(mMercury, new ParticleGravity(mMercury, mSun, GC));
    mpForceRegistry->add(mVenus, new ParticleGravity(mVenus, mSun, GC));
    mpForceRegistry->add(mEarth, new ParticleGravity(mEarth, mSun, GC));
    mpForceRegistry->add(mMoon, new ParticleGravity(mMoon, mEarth, GC));
    mpForceRegistry->add(mMoon, new ParticleGravity(mMoon, mSun, GC));
    mpForceRegistry->add(mMars, new ParticleGravity(mMars, mSun, GC));
    mpForceRegistry->add(mJupiter, new ParticleGravity(mJupiter, mSun, GC));
    mpForceRegistry->add(mSaturn, new ParticleGravity(mSaturn, mSun, GC));
    mpForceRegistry->add(mUranus, new ParticleGravity(mUranus, mSun, GC));
    mpForceRegistry->add(mNeptune, new ParticleGravity(mNeptune, mSun, GC));
    //-------------------------------------------------------

    return true;
}

void Game::update()
{
    if (mLastTime == 0)
    {
        mLastTime = glfwGetTime();
        return;
    }

    mpInputManager->Update();

    real deltaTime = glfwGetTime() - mLastTime;

    //------------------- UPDATE CAMERA ---------------------

    //change planet choice
    if (glfwGetKey(GLFW_KEY_UP) == GLFW_PRESS && !mLastUp)
    {
        if (mPlanet >= 8)
        {
            mPlanet = 0;
        }
        else
           mPlanet++;

        mLastUp = true;
    }

    if (glfwGetKey(GLFW_KEY_UP) == GLFW_RELEASE)
    {
       mLastUp = false;
    }

    if (glfwGetKey(GLFW_KEY_DOWN) == GLFW_PRESS && !mLastDown)
    {
        if (mPlanet <= 0)
        {
            mPlanet = 8;
        }
        else
           mPlanet--;

        mLastDown = true;
    }

    if (glfwGetKey(GLFW_KEY_DOWN) == GLFW_RELEASE)
    {
       mLastDown = false;
    }

    // update force registry
    mpForceRegistry->updateForces(deltaTime);

    //-------------------- UPDATE OBJECTS -------------------
    mSun->update(deltaTime);
    mMercury->update(deltaTime);
    mVenus->update(deltaTime);
    mEarth->update(deltaTime);
    mMoon->update(deltaTime);
    mMars->update(deltaTime);
    mJupiter->update(deltaTime);
    mSaturn->update(deltaTime);
    mUranus->update(deltaTime);
    mNeptune->update(deltaTime);
    //-------------------------------------------------------

// set camera position based on planet choice
    Vector3 camPos;
    switch (mPlanet)
    {
    case 0:
       camPos = mpCamera->getPosition();
       camPos.setX(mSun->getPosition().X());
       camPos.setZ(mSun->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 1:
       camPos = mpCamera->getPosition();
       camPos.setX(mMercury->getPosition().X());
       camPos.setZ(mMercury->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 2:
       camPos = mpCamera->getPosition();
       camPos.setX(mVenus->getPosition().X());
       camPos.setZ(mVenus->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 3:
       camPos = mpCamera->getPosition();
       camPos.setX(mEarth->getPosition().X());
       camPos.setZ(mEarth->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 4:
       camPos = mpCamera->getPosition();
       camPos.setX(mMars->getPosition().X());
       camPos.setZ(mMars->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 5:
       camPos = mpCamera->getPosition();
       camPos.setX(mJupiter->getPosition().X());
       camPos.setZ(mJupiter->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 6:
       camPos = mpCamera->getPosition();
       camPos.setX(mSaturn->getPosition().X());
       camPos.setZ(mSaturn->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 7:
       camPos = mpCamera->getPosition();
       camPos.setX(mUranus->getPosition().X());
       camPos.setZ(mUranus->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    case 8:
       camPos = mpCamera->getPosition();
       camPos.setX(mNeptune->getPosition().X());
       camPos.setZ(mNeptune->getPosition().Z());
       mpCamera->setPosition(camPos);
       break;
    default:
       break;
    }
    //-------------------------------------------------------

    mpCamera->update(deltaTime);

    mLastTime = glfwGetTime();
}

void Game::draw()
{
    glClearColor( 0.0f, 0.0f, 0.0f, 1.0f ); // clear background to gray
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    //do drawing stuff here of objects
    mSun->draw();
    mMercury->draw();
    mVenus->draw();
    mEarth->draw();
    mMoon->draw();
    mMars->draw();
    mJupiter->draw();
    mSaturn->draw();
    mUranus->draw();
    mNeptune->draw();

    glPushMatrix();

    mpCamera->applyCamera();

    GLfloat sun_direction[] = { 0.0, 0.0, 0.0, 1.0 };
    glLightfv(GL_LIGHT0, GL_POSITION, sun_direction);

    glPopMatrix();

    mpCamera->draw();

    glfwSwapBuffers();
}
