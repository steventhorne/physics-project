#pragma once
#include <GL/glfw.h>
#include "Precision.h"
#include "Vector2.h"
#include "Vector3.h"

#define CAMERA_SPEED 0.01

class Game;

class Camera
{
    public:
        Camera(Game* pGame);
        Camera(Game* pGame, Vector3 p);
        Camera(Game* pGame, Vector3 p, Vector2 r);
        Camera(Game* pGame, Vector3 p, Vector3 r);
        ~Camera() {};

        Vector3 getPosition() { return mPos; };
        void setPosition( Vector3 position ) { mPos = position; };

        void update( real deltaTime );

        void applyCameraTranslation();
        void applyCameraRotation();
        void applyCamera();

        void draw(); //draws camera specific items.

    private:
        Game* mpGame;
        Vector3 mPos;
        float xrot, yrot;
        int mLastScroll;
        float mCameraSpeed;
};
