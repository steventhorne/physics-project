#pragma once
#include <GL/glfw.h>
#include "Vector2.h"

class Input
{
    public:
    Input();
    ~Input();

    bool Init();

    void Update();

    bool isKeyPressed(int key);
    bool isKeyReleased(int key);

    bool mouseButtonDown(int button);

    Vector2 mouseDiff();

    private:
    void GLFWCALL mousePosCallback(int x, int y);

    void GLFWCALL mouseButtonCallback(int button, int action);

    void GLFWCALL keyboardPressCallback(int key, int action);

    void GLFWCALL charPressCallback(int key, int action);

    int mKeysDown[255];

    Vector2 lastMouse;
    Vector2 currentMouse;
};
