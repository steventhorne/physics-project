#include "Cube.h"
#include "Camera.h"
#include "Game.h"

Cube::Cube( Game* pGame )
   :Object(pGame)
{

}

Cube::Cube( Game* pGame, Vector3 position )
   :Object(pGame, position)
{

}

Cube::Cube( Game* pGame, Vector3 position, Vector3 rotation )
   :Object(pGame, position, rotation)
{

}

Cube::Cube( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale )
   :Object(pGame, position, rotation, scale)
{

}

void Cube::draw()
{
    float currentColor[4];
    glGetFloatv(GL_CURRENT_COLOR,currentColor);

    glPushMatrix();

    mpGame->getCamera()->applyCamera();
    glTranslated(mPosition.X(), mPosition.Y(), mPosition.Z());
    glRotatef(mRotation.X(), 1, 0, 0);
    glRotatef(mRotation.Y(), 0, 1, 0);
    glRotatef(mRotation.Z(), 0, 0, 1);

    glBegin(GL_QUADS);

    // FRONT
    glColor4f(1.0f, 0.0f, 1.0f, 1.0f);
    glNormal3f(0, 0, 1);
    glVertex3f(-mScale.X() / 2, -mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(0, 0, 1);
    glVertex3f(-mScale.X() / 2, mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(0, 0, 1);
    glVertex3f(mScale.X() / 2, mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(0, 0, 1);
    glVertex3f(mScale.X() / 2, -mScale.Y() / 2, mScale.Z() / 2);

    // BACK
    glColor4f(0.0f, 0.0f, 0.0f, 1.0f);
    glNormal3f(0, 0, -1);
    glVertex3f(-mScale.X() / 2, -mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(0, 0, -1);
    glVertex3f(mScale.X() / 2, -mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(0, 0, -1);
    glVertex3f(mScale.X() / 2, mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(0, 0, -1);
    glVertex3f(-mScale.X() / 2, mScale.Y() / 2, -mScale.Z() / 2);


    // LEFT
    glColor4f(0.0f, 1.0f, 0.0f, 1.0f);
    glNormal3f(-1, 0, 0);
    glVertex3f(-mScale.X() / 2, -mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(-1, 0, 0);
    glVertex3f(-mScale.X() / 2, mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(-1, 0, 0);
    glVertex3f(-mScale.X() / 2, mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(-1, 0, 0);
    glVertex3f(-mScale.X() / 2, -mScale.Y() / 2, mScale.Z() / 2);


    // RIGHT
    glColor4f(0.3f, 0.3f, 0.3f, 1.0f);
    glNormal3f(1, 0, 0);
    glVertex3f(mScale.X() / 2, -mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(1, 0, 0);
    glVertex3f(mScale.X() / 2, -mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(1, 0, 0);
    glVertex3f(mScale.X() / 2, mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(1, 0, 0);
    glVertex3f(mScale.X() / 2, mScale.Y() / 2, -mScale.Z() / 2);


    // TOP
    glColor4f(1.0f, 1.0f, 0.0f, 1.0f);
    glNormal3f(0, 1, 0);
    glVertex3f(-mScale.X() / 2, mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(0, 1, 0);
    glVertex3f(mScale.X() / 2, mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(0, 1, 0);
    glVertex3f(mScale.X() / 2, mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(0, 1, 0);
    glVertex3f(-mScale.X() / 2, mScale.Y() / 2, mScale.Z() / 2);


    // BOTTOM
    glColor4f(1.0f, 0.65f, 0.0f, 1.0f);
    glNormal3f(0, -1, 0);
    glVertex3f(-mScale.X() / 2, -mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(0, -1, 0);
    glVertex3f(mScale.X() / 2, -mScale.Y() / 2, mScale.Z() / 2);
    glNormal3f(0, -1, 0);
    glVertex3f(mScale.X() / 2, -mScale.Y() / 2, -mScale.Z() / 2);
    glNormal3f(0, -1, 0);
    glVertex3f(-mScale.X() / 2, -mScale.Y() / 2, -mScale.Z() / 2);

    glEnd();

    glPopMatrix();

    glColor4f(currentColor[0], currentColor[1], currentColor[2], currentColor[3]);
}
