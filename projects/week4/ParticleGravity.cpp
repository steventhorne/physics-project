#include "ParticleGravity.h"

ParticleGravity::ParticleGravity( PhysicsObject* object1, PhysicsObject* object2, real G )
{
   mObject1 = object1;
   mObject2 = object2;

   mG = G;
}

void ParticleGravity::updateForce(PhysicsObject *object, real duration)
{
   if (!object->hasFiniteMass()) return;

   Vector3 distance = mObject2->getPosition() - mObject1->getPosition();
   float dist2 = distance.magnitude2();
   float force = mG * ((mObject1->getMass() * mObject2->getMass())/dist2);

   Vector3 direction = distance.normalized();
   mGravity = direction * force;

   object->addForce(mGravity);
}
