#include "Vector3.h"
#include <math.h>

Vector3::Vector3()
{
    mX = 0;
    mY = 0;
    mZ = 0;
}

Vector3::Vector3( float x, float y, float z )
{
    mX = x;
    mY = y;
    mZ = z;
}

Vector3 Vector3::operator+( const Vector3& rhs ) const
{
    Vector3 newVector = Vector3(mX + rhs.mX, mY + rhs.mY, mZ + rhs.mZ);

    return newVector;
}

void Vector3::operator+=( const Vector3& rhs )
{
   mX += rhs.mX;
   mY += rhs.mY;
   mZ += rhs.mZ;
}

Vector3 Vector3::operator-( const Vector3& rhs ) const
{
    Vector3 newVector = Vector3(mX - rhs.mX, mY - rhs.mY, mZ - rhs.mZ);

    return newVector;
}

Vector3 Vector3::operator%( const Vector3& rhs ) const
{
    Vector3 crossVec = Vector3((mY*rhs.mZ) - (mZ*rhs.mY), (mZ*rhs.mX) - (mX*rhs.mZ), (mX*rhs.mY) - (mY*rhs.mX));

    return crossVec;
}

float Vector3::operator*( const Vector3& rhs ) const
{
   float dot = (rhs.mX * mX) + (mY * rhs.mY) + (mZ * rhs.mZ);

   return dot;
}

Vector3 Vector3::operator*(const float& rhs) const
{
   return Vector3(mX*rhs, mY*rhs, mZ*rhs);
}

void Vector3::operator*=(const float& rhs)
{
   mX *= rhs;
   mY *= rhs;
   mZ *= rhs;
}

void Vector3::clear()
{
   mX = 0;
   mY = 0;
   mZ = 0;
}

void Vector3::normalize()
{
    float mag = magnitude();

    if (mag == 0)
        return;

    mX /= mag;
    mY /= mag;
    mZ /= mag;
}

Vector3 Vector3::normalized()
{
    Vector3 newVector = Vector3(mX, mY, mZ);

    newVector.normalize();

    return newVector;
}

float Vector3::magnitude()
{
   float mag2 = magnitude2();

   float mag = sqrtf(mag2);

   return mag;
}

float Vector3::magnitude2()
{
   return (mX*mX + mY*mY + mZ*mZ);
}

Vector3 Vector3::cross(Vector3 other)
{
    Vector3 crossVec = Vector3((Y()*other.Z()) - (Z()*other.Y()), (Z()*other.X()) - (X()*other.Z()), (X()*other.Y()) - (Y()*other.X()));
    return crossVec;
}
