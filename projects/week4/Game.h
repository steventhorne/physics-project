#pragma once
#include <GL/glfw.h>
#include "ParticleForceRegistry.h"
#include "PhysicsObject.h"
#include "Plane.h"
#include "Line.h"

#define GCpile 2.9e+38 // pile's thing
#define GCD 8.886e-10 // au3 earthmass-1 d-2
#define GC 4.352e-8 // au3 earthmass-1 wk-2

#define PLANET_SIZE Vector3(0.03, 0.03, 0.03)

class Input;
class Camera;

class Game
{
    public:
    Game();
    ~Game();

    Input* getInputManager() { return mpInputManager; };
    Camera* getCamera() { return mpCamera; };

    bool init();
    void update();

    void draw();

    private:
    Input* mpInputManager;
    Camera* mpCamera;
    ParticleForceRegistry* mpForceRegistry;

    int mPlanet;
    bool mLastUp;
    bool mLastDown;

    PhysicsObject* mSun;
    PhysicsObject* mMercury;
    PhysicsObject* mVenus;
    PhysicsObject* mEarth;
    PhysicsObject* mMoon;
    PhysicsObject* mMars;
    PhysicsObject* mSaturn;
    PhysicsObject* mJupiter;
    PhysicsObject* mUranus;
    PhysicsObject* mNeptune;

    real mDeltaTime;
    real mLastTime;
};
