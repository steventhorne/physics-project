#pragma once
#include "Precision.h"

class Vector3
{
    public:
    Vector3();
    Vector3( float x, float y, float z );
    ~Vector3() {};

    Vector3 operator+(const Vector3& rhs) const; // Vec3 + Vec3
    void operator+=(const Vector3& rhs); // Vec3 += Vec3
    Vector3 operator-(const Vector3& rhs) const; // Vec3 - Vec3
    Vector3 operator%(const Vector3& rhs) const; // cross product
    float operator*(const Vector3& rhs) const; // dot product
    Vector3 operator*(const float& rhs) const; // Vec3 * float
    void operator*=(const float& rhs); // Vec3 *= float

    float X() { return mX; };
    float Y() { return mY; };
    float Z() { return mZ; };

    void setX( float x ) { mX = x; };
    void setY( float y ) { mY = y; };
    void setZ( float z ) { mZ = z; };

    void clear();

    static Vector3 gravity() { return Vector3(0, -15, 0); };

    void normalize();
    Vector3 normalized();

    float magnitude();
    float magnitude2();

    Vector3 cross(Vector3 other);

    float fast_sqrt( float number );

    private:
    float mX;
    float mY;
    float mZ;
};
