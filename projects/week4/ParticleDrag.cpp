#include "ParticleDrag.h"

ParticleDrag::ParticleDrag(real k1, real k2)
{
   mK1 = k1;
   mK2 = k2;
}

void ParticleDrag::updateForce(PhysicsObject *object, real duration)
{
   Vector3 force;
   force = object->getVelocity();

   real dragCoeff = force.magnitude();
   dragCoeff = mK1 * dragCoeff + mK2 * dragCoeff * dragCoeff;

   force.normalize();
   force *= -dragCoeff;
   object->addForce(force);
}
