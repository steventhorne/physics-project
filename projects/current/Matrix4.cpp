#include "Matrix4.h"
#include "Vector3.h"
#include "Quaternion.h"

Matrix4::Matrix4()
{
   Data[0] = 0;
   Data[1] = 0;
   Data[2] = 0;
   Data[3] = 0;
   Data[4] = 0;
   Data[5] = 0;
   Data[6] = 0;
   Data[7] = 0;
   Data[8] = 0;
   Data[9] = 0;
   Data[10] = 0;
   Data[11] = 0;
}

Matrix4::Matrix4(real t1, real t2, real t3,
                  real t4, real t5, real t6,
                  real t7, real t8, real t9,
                  real t10, real t11, real t12)
{
   Data[0] = t1;
   Data[0] = t2;
   Data[0] = t3;
   Data[0] = t4;
   Data[0] = t5;
   Data[0] = t6;
   Data[0] = t7;
   Data[0] = t8;
   Data[0] = t9;
   Data[0] = t10;
   Data[0] = t11;
   Data[0] = t12;
}

Vector3 Matrix4::operator*(const Vector3 &vector) const
{
   return Vector3(
      vector.X * Data[0] +
      vector.Y * Data[1] +
      vector.Z * Data[2] + Data[3],

      vector.X * Data[4] +
      vector.Y * Data[5] +
      vector.Z * Data[6] + Data[7],

      vector.X * Data[8] +
      vector.Y * Data[9] +
      vector.Z * Data[10] + Data[11]
      );
}

Matrix4 Matrix4::operator*(const Matrix4 &o) const
{
   Matrix4 result;
   result.Data[0] = o.Data[0]*Data[0] + o.Data[4]*Data[1] +
                    o.Data[8]*Data[2];
   result.Data[4] = o.Data[0]*Data[4] + o.Data[4]*Data[5] +
                    o.Data[8]*Data[6];
   result.Data[8] = o.Data[0]*Data[8] + o.Data[4]*Data[9] +
                    o.Data[8]*Data[10];

   result.Data[1] = o.Data[1]*Data[0] + o.Data[5]*Data[1] +
                    o.Data[9]*Data[2];
   result.Data[5] = o.Data[1]*Data[4] + o.Data[5]*Data[5] +
                    o.Data[9]*Data[6];
   result.Data[9] = o.Data[1]*Data[8] + o.Data[5]*Data[9] +
                    o.Data[9]*Data[10];

   result.Data[2] = o.Data[2]*Data[0] + o.Data[6]*Data[1] +
                    o.Data[10]*Data[2];
   result.Data[6] = o.Data[2]*Data[4] + o.Data[6]*Data[5] +
                    o.Data[10]*Data[6];
   result.Data[10] = o.Data[2]*Data[8] + o.Data[6]*Data[9] +
                     o.Data[10]*Data[10];

   result.Data[3] = o.Data[3]*Data[0] + o.Data[7]*Data[1] +
                    o.Data[11]*Data[2];
   result.Data[7] = o.Data[3]*Data[4] + o.Data[7]*Data[5] +
                    o.Data[11]*Data[6];
   result.Data[11] = o.Data[3]*Data[8] + o.Data[7]*Data[9] +
                     o.Data[11]*Data[10];

   return result;
}

void Matrix4::operator*=(const Matrix4 &o)
{
   real t1, t2, t3;

   t1 = o.Data[0]*Data[0] + o.Data[4]*Data[1] +
                    o.Data[8]*Data[2];
   t2 = o.Data[0]*Data[4] + o.Data[4]*Data[5] +
                    o.Data[8]*Data[6];
   t3 = o.Data[0]*Data[8] + o.Data[4]*Data[9] +
                    o.Data[8]*Data[10];

   Data[0] = t1;
   Data[1] = t2;
   Data[2] = t3;

   t1 = o.Data[1]*Data[0] + o.Data[5]*Data[1] +
                    o.Data[9]*Data[2];
   t2 = o.Data[1]*Data[4] + o.Data[5]*Data[5] +
                    o.Data[9]*Data[6];
   t3 = o.Data[1]*Data[8] + o.Data[5]*Data[9] +
                    o.Data[9]*Data[10];

   Data[3] = t1;
   Data[4] = t2;
   Data[5] = t3;

   t1 = o.Data[2]*Data[0] + o.Data[6]*Data[1] +
                    o.Data[10]*Data[2];
   t2 = o.Data[2]*Data[4] + o.Data[6]*Data[5] +
                    o.Data[10]*Data[6];
   t3 = o.Data[2]*Data[8] + o.Data[6]*Data[9] +
                     o.Data[10]*Data[10];

   Data[6] = t1;
   Data[7] = t2;
   Data[8] = t3;

   t1 = o.Data[3]*Data[0] + o.Data[7]*Data[1] +
                    o.Data[11]*Data[2];
   t2 = o.Data[3]*Data[4] + o.Data[7]*Data[5] +
                    o.Data[11]*Data[6];
   t3 = o.Data[3]*Data[8] + o.Data[7]*Data[9] +
                     o.Data[11]*Data[10];

   Data[9] = t1;
   Data[10] = t2;
   Data[11] = t3;
}

Vector3 Matrix4::Transform(const Vector3 &vector) const
{
   return (*this) * vector;
}

Vector3 Matrix4::TransformInverse(const Vector3 &vector) const
{
   Vector3 tmp = vector;
   tmp.X -= Data[3];
   tmp.Y -= Data[7];
   tmp.Z -= Data[11];
   return Vector3(
      tmp.X * Data[0] +
      tmp.Y * Data[4] +
      tmp.Z * Data[8],

      tmp.X * Data[1] +
      tmp.Y * Data[5] +
      tmp.Z * Data[9],

      tmp.X * Data[2] +
      tmp.Y * Data[6] +
      tmp.Z * Data[10]
   );
}

Vector3 Matrix4::TransformInverseDirection(const Vector3 &vector) const
{
   return Vector3(
      vector.X * Data[0] +
      vector.Y * Data[4] +
      vector.Z * Data[8],

      vector.X * Data[1] +
      vector.Y * Data[5] +
      vector.Z * Data[9],

      vector.X * Data[2] +
      vector.Y * Data[6] +
      vector.Z * Data[10]
   );
}

Vector3 Matrix4::TransformDirection(const Vector3 &vector) const
{
   return Vector3(
      vector.X * Data[0] +
      vector.Y * Data[1] +
      vector.Z * Data[2],

      vector.X * Data[4] +
      vector.Y * Data[5] +
      vector.Z * Data[6],

      vector.X * Data[8] +
      vector.Y * Data[9] +
      vector.Z * Data[10]
   );
}


real Matrix4::GetDeterminant() const
{
   return Data[8]*Data[5]*Data[2] +
          Data[4]*Data[9]*Data[2] +
          Data[8]*Data[1]*Data[6] -
          Data[0]*Data[9]*Data[6] -
          Data[4]*Data[1]*Data[10] +
          Data[0]*Data[5]*Data[10];
}

void Matrix4::SetInverse(const Matrix4 &m)
{
   real det = GetDeterminant();

   if (det == 0) return;
   det = ((real)1.0f)/det;

   Data[0] = (-m.Data[9]*m.Data[6] + m.Data[5]*m.Data[10])*det;
   Data[1] = (m.Data[9]*m.Data[2] - m.Data[1]*m.Data[10])*det;
   Data[2] = (-m.Data[5]*m.Data[2] + m.Data[1]*m.Data[6])*det;
   Data[3] = (m.Data[9]*m.Data[6]*m.Data[3] -
              m.Data[5]*m.Data[10]*m.Data[3] -
              m.Data[9]*m.Data[2]*m.Data[7] +
              m.Data[1]*m.Data[10]*m.Data[7] +
              m.Data[5]*m.Data[2]*m.Data[11] -
              m.Data[1]*m.Data[6]*m.Data[11])*det;
   Data[4] = (m.Data[8]*m.Data[6] - m.Data[4]*m.Data[10])*det;
   Data[5] = (-m.Data[8]*m.Data[2] + m.Data[0]*m.Data[10])*det;
   Data[6] = (m.Data[4]*m.Data[2] - m.Data[0]*m.Data[6])*det;
   Data[7] = (-m.Data[8]*m.Data[6]*m.Data[3] +
              m.Data[4]*m.Data[10]*m.Data[3] +
              m.Data[8]*m.Data[2]*m.Data[7] -
              m.Data[0]*m.Data[10]*m.Data[7] -
              m.Data[4]*m.Data[2]*m.Data[11] +
              m.Data[0]*m.Data[6]*m.Data[11])*det;
   Data[8] = (-m.Data[8]*m.Data[5] + m.Data[4]*m.Data[9])*det;
   Data[9] = (m.Data[8]*m.Data[1] - m.Data[0]*m.Data[9])*det;
   Data[10] = (-m.Data[4]*m.Data[1] + m.Data[0]*m.Data[5])*det;
   Data[11] = (m.Data[8]*m.Data[5]*m.Data[3] -
               m.Data[4]*m.Data[10]*m.Data[3] -
               m.Data[8]*m.Data[1]*m.Data[7] +
               m.Data[0]*m.Data[9]*m.Data[7] +
               m.Data[4]*m.Data[1]*m.Data[11] -
               m.Data[0]*m.Data[5]*m.Data[11])*det;
}

Matrix4 Matrix4::Inverse() const
{
   Matrix4 result;
   result.SetInverse(*this);
   return result;
}

void Matrix4::Invert()
{
   SetInverse(*this);
}

void Matrix4::SetOrientationAndPos(const Quaternion &q, const Vector3 &pos)
{
   Data[0] = 1 - (2*q.J*q.J + 2*q.K*q.K);
   Data[1] = 2*q.I*q.J + 2*q.K*q.R;
   Data[2] = 2*q.I*q.K - 2*q.J*q.R;
   Data[3] = pos.X;
   Data[4] = 2*q.I*q.J - 2*q.K*q.R;
   Data[5] = 1 - (2*q.I*q.I + 2*q.K*q.K);
   Data[6] = 2*q.J*q.K + 2*q.I*q.R;
   Data[7] = pos.Y;
   Data[8] = 2*q.I*q.K + 2*q.J*q.R;
   Data[9] = 2*q.J*q.K - 2*q.I*q.R;
   Data[10] = 1 - (2*q.I*q.I + 2*q.J*q.J);
   Data[11] = pos.Z;
}
