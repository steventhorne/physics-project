#pragma once
#include "Precision.h"
#include "Rigidbody.h"

class ParticleForceGenerator
{
   public:
   virtual void updateForce(Rigidbody *object, real duration) = 0;
};
