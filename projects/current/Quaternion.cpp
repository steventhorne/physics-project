#include "Quaternion.h"
#include "Vector3.h"
#include <math.h>

Quaternion::Quaternion()
{
   R = 0;
   I = 0;
   J = 0;
   K = 0;
}

Quaternion::Quaternion(real x, real y, real z)
{
   Quaternion quat = EulerToQuaternion(Vector3(x, y, z));

   R = quat.R;
   I = quat.I;
   J = quat.J;
   K = quat.K;
}

Quaternion::Quaternion(Vector3 euler)
{
   Quaternion quat = EulerToQuaternion(euler);

   R = quat.R;
   I = quat.I;
   J = quat.J;
   K = quat.K;
}

Quaternion::Quaternion(real r, real i, real j, real k)
{
   R = r;
   I = i;
   J = j;
   K = k;
}

void Quaternion::Normalize()
{
   real d = R*R + I*I + J*J + K*K;

   if (d == 0)
   {
      R = 1;
      return;
   }

   d = ((real)1.0)/real_sqrt(d);
   R *= d;
   I *= d;
   J *= d;
   K *= d;
}

void Quaternion::operator*=(const Quaternion &multiplier)
{
   Quaternion q = *this;
   R = q.R*multiplier.R - q.I*multiplier.I -
       q.J*multiplier.J - q.K*multiplier.K;
   I = q.R*multiplier.I + q.I*multiplier.R +
       q.J*multiplier.K - q.K*multiplier.J;
   J = q.R*multiplier.J + q.J*multiplier.R +
       q.K*multiplier.I - q.I*multiplier.K;
   K = q.R*multiplier.K + q.K*multiplier.R +
       q.I*multiplier.J - q.J*multiplier.I;
}

void Quaternion::RotateByVector(const Vector3 &vector)
{
   Quaternion q(0, vector.X, vector.Y, vector.Z);
   (*this) *= q;
}

void Quaternion::AddScaledVector(const Vector3 &vector, real scale)
{
   Quaternion q(0, vector.X*scale, vector.Y*scale, vector.Z*scale);

   q *= *this;
   R += q.R * ((real)0.5);
   I += q.I * ((real)0.5);
   J += q.J * ((real)0.5);
   K += q.K * ((real)0.5);
}

Vector3 Quaternion::EulerAngles(bool homogenous) const
{
   float sqw = R*R;
   float sqx = I*I;
   float sqy = J*J;
   float sqz = K*K;

   Vector3 euler;
   if (homogenous)
   {
      euler.X = atan2f(2.f * (I*J + K*R), sqx - sqy - sqz + sqw);
      euler.Y = asinf(-2.f * (I*K - J*R));
      euler.Z = atan2f(2.f * (J*K + I*R), -sqx - sqy + sqz + sqw);
   }
   else
   {
      euler.X = atan2f(2.f * (K*J + I*R), 1 - 2*(sqx + sqy));
      euler.Y = asinf(-2.f * (I*K - J*R));
      euler.Z = atan2f(2.f * (I*J + K*R), 1 - 2*(sqy + sqz));
   }
   return euler;
}

Quaternion Quaternion::EulerToQuaternion(Vector3 euler) const
{
   Quaternion quat;

   float cos_z_2 = cosf(0.5*euler.Z);
   float cos_y_2 = cosf(0.5*euler.Y);
   float cos_x_2 = cosf(0.5*euler.X);

   float sin_z_2 = sinf(0.5*euler.Z);
   float sin_y_2 = sinf(0.5*euler.Y);
   float sin_x_2 = sinf(0.5*euler.X);

   // and now compute quaternion
   quat.R = cos_z_2*cos_y_2*cos_x_2 + sin_z_2*sin_y_2*sin_x_2;
   quat.I = cos_z_2*cos_y_2*sin_x_2 - sin_z_2*sin_y_2*cos_x_2;
   quat.J = cos_z_2*sin_y_2*cos_x_2 + sin_z_2*cos_y_2*sin_x_2;
   quat.K = sin_z_2*cos_y_2*cos_x_2 - cos_z_2*sin_y_2*sin_x_2;

   return quat;
}
