#include "ParticleGravity.h"

ParticleGravity::ParticleGravity( const Vector3 &gravity )
{
   mGravity = gravity;
}

void ParticleGravity::updateForce(Rigidbody *object, real duration)
{
   object->AddForce(mGravity * object->GetMass());
}
