#include "World.h"

World::World(Game* game)
{
   mpGame = game;
   mDeltaTime = 0;
}

World::~World()
{

}

void World::ResolveContacts(void * client_data, DtObjectRef obj1, DtObjectRef obj2, const DtCollData *coll_data)
{
   bool ob1 = false;
   bool ob2 = false;

   GameObject* gob1;
   GameObject* gob2;

   Collider cob1 = (*(Collider *)obj1);
   Collider cob2 = (*(Collider *)obj2);

   for(obj_iter iterator = mGameObjects.begin(); iterator != mGameObjects.end(); iterator++)
   {
      if (iterator->first->GetCollider().id == cob1.id)
      {
         gob1 = iterator->first;
         ob1 = true;
      }

      if (iterator->first->GetCollider().id == cob2.id)
      {
         gob2 = iterator->first;
         ob2 = true;
      }
   }

   if (ob1 && ob2) // if both objects exist
   {
      Rigidbody* gob1r = gob1->GetRigidbody();
      Rigidbody* gob2r = gob2->GetRigidbody();

      Vector3 normal = Vector3(coll_data->normal[0], coll_data->normal[1], coll_data->normal[2]).normalized();
      Vector3 point1 = Vector3(coll_data->point1[0], coll_data->point1[1], coll_data->point1[2]);
      Vector3 point2 = Vector3(coll_data->point2[0], coll_data->point2[1], coll_data->point2[2]);

      if (gob1r != 0)
      {
         Vector3 sepVelocityVec = gob1r->GetVelocity();

         if (gob2r != 0)
            sepVelocityVec -= gob2r->GetVelocity();

         real sepVelocity = sepVelocityVec * normal;

         real oldSepVelocity = sepVelocity;

         Vector3 accCausedVelocity = gob1r->GetAcceleration();
         if (gob2r) accCausedVelocity -= gob2r->GetAcceleration();
         real accCausedSepVelocity = accCausedVelocity * normal * 0.16;

         if (accCausedSepVelocity < 0)
         {
            sepVelocity += accCausedSepVelocity;
            if (sepVelocity < 0) sepVelocity = 0;
         }

         real deltaVelocity = sepVelocity - oldSepVelocity;

         real totalInverseMass = gob1r->GetInverseMass();
         if (gob2r) totalInverseMass += gob2r->GetInverseMass();

         if (totalInverseMass <= 0) return;

         real impulse = deltaVelocity / totalInverseMass;

         Vector3 force = normal * impulse;

         gob1r->AddForceAtPoint(force * 500, point1);
      }

      if (gob2r != 0)
      {
         Vector3 sepVelocityVec = gob2r->GetVelocity();

         if (gob1r != 0)
            sepVelocityVec -= gob1r->GetVelocity();

         real sepVelocity = sepVelocityVec * normal;

         real oldSepVelocity = sepVelocity;

         Vector3 accCausedVelocity = gob2r->GetAcceleration();
         if (gob1r) accCausedVelocity -= gob1r->GetAcceleration();
         real accCausedSepVelocity = accCausedVelocity * normal * -0.16f;

         if (accCausedSepVelocity < 0)
         {
            sepVelocity += accCausedSepVelocity;
            if (sepVelocity < 0) sepVelocity = 0;
         }

         real deltaVelocity = sepVelocity - oldSepVelocity;

         real totalInverseMass = gob2r->GetInverseMass();
         if (gob1r) totalInverseMass += gob1r->GetInverseMass();

         if (totalInverseMass <= 0) return;

         real impulse = deltaVelocity / totalInverseMass;

         Vector3 force = normal * impulse;

         gob2r->AddForceAtPoint(force * 500, point2);
      }
   }
}

Cube* World::GetPlayer()
{
   return mPlayer;
}

void World::Update(float deltaTime)
{
   mDeltaTime = deltaTime;
   RunPhysics(deltaTime);
}

void World::Integrate(real duration)
{
   // update objects
   for(obj_iter iterator = mGameObjects.begin(); iterator != mGameObjects.end(); iterator++)
   {
      iterator->first->Update(duration);
   }
}

void World::RunPhysics(real duration)
{
   // update force registry
   mpForceRegistry->updateForces(duration);

   Integrate(duration);
}
