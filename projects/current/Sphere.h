#pragma once
#include <math.h>
#include "Vector3.h"
#include "GameObject.h"

class Game;

class Sphere : public GameObject
{
    public:
    Sphere( Game* pGame );
    Sphere( Game* pGame, Vector3 position );
    Sphere( Game* pGame, Vector3 position, Vector3 rotation );
    Sphere( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale );
    ~Sphere() {};

    void Update(real duration);
    void Draw();
    void Draw(double lats, double longs);
};
