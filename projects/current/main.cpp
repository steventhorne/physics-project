#include <GL/glfw.h>
#include <stdlib.h>
#include "Game.h"
#include "World.h"

int windowWidth, windowHeight;
static Game* gpGame;

void DefCollision(void * client_data, DtObjectRef obj1, DtObjectRef obj2, const DtCollData *coll_data)
{
   gpGame->GetLevel()->ResolveContacts(client_data, obj1, obj2, coll_data);
}

void GLFWCALL WindowResized( int width, int height )
{
    windowWidth = width;
    windowHeight = height;

    //Tell OpenGL how to convert from coordinates to pixel values
    glViewport( 0, 0, width, height );

    glMatrixMode( GL_PROJECTION ); //Switch to setting the camera perspective
    //Set the camera perspective
    glLoadIdentity(); //reset the camera
    gluPerspective( 45.0f,                      //camera angle
                (GLfloat)width/(GLfloat)height, //The width to height ratio
                 0.1f,                          //The near z clipping coordinate
                1000000.0f );
}

int main()
{
    gpGame = new Game();
    int running = GL_TRUE;

    glfwInit();

    GLFWvidmode vm;
    glfwGetDesktopMode(&vm);
    if (!glfwOpenWindow(1080, 720, vm.RedBits, vm.GreenBits, vm.BlueBits, 0, 24, 8, GLFW_WINDOW))
    {
        fprintf(stderr, "Failed to open GLFW window!\n");
        glfwTerminate();
        return 2;
    }

    //set up window properties
    glfwSetWindowTitle("Graphics Base");

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH_BUFFER_BIT);
    glDisable(GL_CULL_FACE);
    glDepthFunc(GL_LEQUAL);
    glDepthMask(GL_TRUE);
    glEnable(GL_NORMALIZE);

    //set up game
    gpGame->Init();

    while ( running  && !gpGame->ShouldExit())
    {
        gpGame->Update();

        //pGame->draw();

        glfwSetWindowSizeCallback( WindowResized );

        running = !glfwGetKey( GLFW_KEY_ESC ) && glfwGetWindowParam( GLFW_OPENED );
    }

    delete gpGame;
    gpGame = NULL;

    glfwTerminate();
    return 0;
}
