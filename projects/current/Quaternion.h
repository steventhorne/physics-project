#pragma once
#include "Precision.h"

class Vector3;

class Quaternion
{
public:
   Quaternion();
   Quaternion(real x, real y, real z);
   Quaternion(Vector3 euler);
   Quaternion(real r, real i, real j, real k);
   ~Quaternion(){};

   void operator*=(const Quaternion &multiplier);

   void Normalize();

   void RotateByVector(const Vector3 &vector);

   void AddScaledVector(const Vector3 &vector, real scale);

   Vector3 EulerAngles(bool homogenous=true) const;

   Quaternion EulerToQuaternion(Vector3 euler) const;

   union
   {
      struct
      {
         real R;
         real I;
         real J;
         real K;
      };

      real Data[4];
   };

   static Quaternion IDENTITY() { return Quaternion(0.0, 0.0, 0.0, 1.0); };
};
