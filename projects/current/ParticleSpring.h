#pragma once
#include "ParticleForceGenerator.h"

class ParticleSpring : public ParticleForceGenerator
{
   public:
   ParticleSpring(Rigidbody* other, real springConstant, real restLength);

   virtual void updateForce(Rigidbody* object, real duration);

   private:
   Rigidbody* mOther;
   real mSpringConstant;
   real mRestLength;
};
