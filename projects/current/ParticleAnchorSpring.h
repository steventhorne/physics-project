#pragma once
#include "ParticleForceGenerator.h"

class ParticleAnchorSpring : public ParticleForceGenerator
{
   public:
   ParticleAnchorSpring(Vector3 anchor, real springConstant, real restLength);

   virtual void updateForce(Rigidbody* object, real duration);

   protected:
   Vector3 mAnchor;
   real mSpringConstant;
   real mRestLength;
};
