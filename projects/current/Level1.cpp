#include "Level1.h"
#include "Camera.h"
#include "Cube.h"
#include "ParticleGravity.h"
#include "ParticleAnchorSpring.h"

Level1::Level1(Game* game)
:World(game)
{
   mpForceRegistry = new ParticleForceRegistry();

   mEndZPos = -20;
}

Level1::~Level1()
{
   delete mpForceRegistry;
   mpForceRegistry = NULL;

   delete mPath;
   mPath = NULL;

   for(obj_iter iterator = mGameObjects.begin(); iterator != mGameObjects.end(); iterator++)
   {
      if (iterator->first != 0)
         delete iterator->first;
   }
   mGameObjects.clear();
}

bool Level1::Init()
{
   // set up camera for level
   mpGame->getCamera()->setPosition(Vector3(25, 2, -10));
   mpGame->getCamera()->setRotation(Vector2(0, -90));

   // init objects
   mPath = new Plane(mpGame, Vector3(0, 0, -10), Vector3(0, 0, 0), Vector3(20, 0, 20));
   mPath->AddCollider();
   mGameObjects.insert(ObjPair(mPath, 0.5));

   //------------------- Instantiate Objects ----------------------
   mPlayer = new Cube(mpGame, Vector3(0, 4, -1), Vector3(0, 0, 0));
   mPlayer->SetScale(Vector3(2, 2, 2));
   mPlayer->AddCollider();
   mPlayer->AddRigidbody();
   mPlayer->GetRigidbody()->SetInverseMass(1.0f);
   //mPlayer->GetRigidbody()->SetInertiaTensor(Matrix3(0.1, 0, 0, 0, 0.1, 0, 0, 0, 0.1));
   mGameObjects.insert(ObjPair(mPlayer, 0.5));
   //--------------------------------------------------------------

   //-------------------- Add Physics Forces ----------------------
   mpForceRegistry->add(mPlayer->GetRigidbody(), new ParticleGravity(Vector3::gravity()));
   //--------------------------------------------------------------

   return true;
}

void Level1::Update(float deltaTime)
{
   RunPhysics(deltaTime);
}

void Level1::Draw()
{
   //do drawin) stuff here of objects
   mPath->Draw();

   Vector3 position;

   glPushMatrix();
   mpGame->getCamera()->applyCamera();

   glColor3f(1, 1, 1);
   glPopMatrix();

   for(obj_iter iterator = mGameObjects.begin(); iterator != mGameObjects.end(); iterator++)
   {
     iterator->first->Draw();
   }
}
