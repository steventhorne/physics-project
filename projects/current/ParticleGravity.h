#pragma once
#include "ParticleForceGenerator.h"

class ParticleGravity : public ParticleForceGenerator
{
   public:
   ParticleGravity(const Vector3 &gravity);

   virtual void updateForce(Rigidbody* object, real duration);
   private:
   Vector3 mGravity;
};
