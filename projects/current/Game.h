#pragma once
#include "Precision.h"
#include "SOLID/solid.h"

class World;
class Input;
class Camera;

class Game
{
public:
   Game();
   ~Game();

   Input* getInputManager() { return mpInputManager; };
   Camera* getCamera() { return mpCamera; };

   bool Init();
   void Update();

   void Draw();

   World* GetLevel() { return mLevel; };

   bool ShouldExit() { return mShouldExit; };

   int GetNextCollisionId();

private:
   int mCollisionId;
   Input* mpInputManager;
   Camera* mpCamera;

   World* mLevel;

   unsigned generateContacts();
   void integrate(real duration);
   void runPhysics(real duration);

   void loadNewLevel(int levelNum);

   real mDeltaTime;
   real mLastTime;

   int mCurrentLvlNum;
   int mNumLevels;

   bool mShouldExit;

};

extern void DefCollision(void * client_data, DtObjectRef obj1, DtObjectRef obj2, const DtCollData *coll_data);
