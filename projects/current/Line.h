#pragma once
#include "Vector3.h"
#include <GL/glfw.h>

class Line
{
    public:
    Line(Vector3 begin, Vector3 end);
    ~Line() {};

    Vector3 getBeginCoords() { return mBeginPoint; };
    Vector3 getEndCoords() { return mEndPoint; };

    void setBeginCoords( float x, float y, float z ) { mBeginPoint = Vector3(x, y, z); };
    void setBeginCoords( Vector3 coords ) { mBeginPoint = coords; };

    void setEndCoords( float x, float y, float z ) { mEndPoint = Vector3(x, y, z); };
    void setEndCoords( Vector3 coords ) { mEndPoint = coords; };

    void draw();

    private:
    Vector3 mBeginPoint;
    Vector3 mEndPoint;
};
