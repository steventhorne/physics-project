#pragma once
#include "Vector3.h"
#include "Matrix3.h"
#include "Matrix4.h"
#include "Quaternion.h"

class Rigidbody
{
public:

   Rigidbody();
   ~Rigidbody() {};

   void CalculateDerivedData();
   void Integrate(real duration);

   void SetMass(const real mass);
   real GetMass() const;

   void SetInverseMass(const real inverseMass);
   real GetInverseMass() const;
   bool HasFiniteMass() const;

   void SetInertiaTensor(const Matrix3 &inertiaTensor);
   void GetInertiaTensor(Matrix3* inertiaTensor) const;
   Matrix3 GetInertiaTensor() const;

   void GetInertiaTensorWorld(Matrix3 *inertiaTensor) const;
   Matrix3 GetInertiaTensorWorld() const;

   void SetInverseInertiaTensor(const Matrix3 &inverseInertiaTensor);
   void GetInverseInertiaTensor(Matrix3* inverseInertiaTensor) const;
   Matrix3 GetInverseInertiaTensor() const;
   void GetInverseInertiaTensorWorld(Matrix3 *inverseInertiaTensor) const;
   Matrix3 GetInverseInertiaTensorWorld() const;

   void SetDamping(const real linearDamping, const real angularDamping);
   void SetLinearDamping(const real linearDamping);
   real GetLinearDamping() const;

   void SetAngularDamping(const real angularDamping);
   real GetAngularDamping() const;

   void SetPosition(const Vector3& position);
   void SetPosition(const real x, const real y, const real z);
   void GetPosition(Vector3* position) const;
   Vector3 GetPosition() const;

   void SetOrientation(const Quaternion& orientation);
   void SetOrientation(const real r, const real i, const real j, const real k);
   void GetOrientation(Quaternion* orientation) const;
   Quaternion GetOrientation() const;
   void GetOrientation(Matrix3* matrix) const;
   void GetOrientation(real matrix[9]) const;

   void GetTransform(Matrix4* transform) const;
   void GetTransform(real matrix[16]) const;
   void GetGLTransform(real matrix[16]) const;
   Matrix4 GetTransform() const;

   Vector3 GetPointInLocalSpace(const Vector3& point) const;
   Vector3 GetPointInWorldSpace(const Vector3 &point) const;
   Vector3 GetDirectionInLocalSpace(const Vector3& direction) const;
   Vector3 GetDirectionInWorldSpace(const Vector3& direction) const;

   void SetVelocity(const Vector3& velocity);
   void SetVelocity(const real x, const real y, const real z);
   void GetVelocity(Vector3* velocity) const;
   Vector3 GetVelocity() const;
   void AddVelocity(const Vector3& deltaVelocity);

   void SetRotation(const Vector3& rotation);
   void SetRotation(const real x, const real y, const real z);
   void GetRotation(Vector3* rotation) const;
   Vector3 GetRotation() const;
   void AddRotation(const Vector3& deltaRotation);

   bool GetAwake() const { return mIsAwake; };
   void SetAwake(const bool awake=true);
   bool GetCanSleep() const { return mCanSleep; };
   void SetCanSleep(const bool canSleep=true);

   void GetLastFrameAcceleration(Vector3* linearAcceleration) const;
   Vector3 GetLastFrameAcceleration() const;

   void ClearAccumulators();
   void AddForce(const Vector3& force);
   void AddForceAtPoint(const Vector3& force, const Vector3& point);
   void AddForceAtBodyPoint(const Vector3& force, const Vector3& point);
   void AddTorque(const Vector3& torque);

   void SetAcceleration(const Vector3& acceleration);
   void SetAcceleration(const real x, const real y, const real z);
   void GetAcceleration(Vector3* acceleration) const;
   Vector3 GetAcceleration() const;

protected:
   real mInverseMass;
   Matrix3 mInverseInertiaTensor;
   real mLinearDamping;
   real mAngularDamping;
   Vector3 mPosition;
   Quaternion mOrientation;
   Vector3 mVelocity;
   Vector3 mRotation;
   Matrix3 mInverseInertiaTensorWorld;
   real mMotion;
   bool mIsAwake;
   bool mCanSleep;
   Matrix4 mTransformMatrix;
   Vector3 mForceAccum;
   Vector3 mTorqueAccum;
   Vector3 mAcceleration;
   Vector3 mLastFrameAcceleration;
};
