#pragma once
#include "Precision.h"

class Vector3;

class Quaternion;

class Matrix4
{
public:
   Matrix4();
   Matrix4(real t1, real t2, real t3,
      real t4, real t5, real t6,
      real t7, real t8, real t9,
      real t10, real t11, real t12);

   ~Matrix4(){};

   real Data[12];

   Vector3 operator*(const Vector3 &vector) const;
   Matrix4 operator*(const Matrix4 &o) const;
   void operator*=(const Matrix4 &o);

   Vector3 Transform(const Vector3 &vector) const;
   Vector3 TransformInverse(const Vector3 &vector) const;
   Vector3 TransformInverseDirection(const Vector3 &vector) const;
   Vector3 TransformDirection(const Vector3 &vector) const;

   real GetDeterminant() const;
   void SetInverse(const Matrix4 &m);
   Matrix4 Inverse() const;
   void Invert();

   void SetOrientationAndPos(const Quaternion &q, const Vector3 &pos);
};
