#include "ParticleBungee.h"

ParticleBungee::ParticleBungee(Rigidbody* other, real springConstant, real restLength)
{
   mOther = other;
   mSpringConstant = springConstant;
   mRestLength = restLength;
}

void ParticleBungee::updateForce(Rigidbody* object, real duration)
{
   Vector3 force;
   force = object->GetPosition();
   force -= mOther->GetPosition();

   real magnitude = force.magnitude();
   if (magnitude <= mRestLength) return;

   magnitude = mSpringConstant * (mRestLength - magnitude);

   force.normalize();
   force *= magnitude;
   object->AddForce(force);
}
