#pragma once
#include "Vector3.h"
#include "GameObject.h"

class Game;
class Image;

class Plane : public GameObject
{
public:
   Plane( Game* pGame );
   Plane( Game* pGame, Vector3 position );
   Plane( Game* pGame, Vector3 position, Vector3 rotation );
   Plane( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale );
   ~Plane();

   void AddCollider();

   void Draw();

private:
   Image* mImage;
   DtShapeRef mShape;
};
