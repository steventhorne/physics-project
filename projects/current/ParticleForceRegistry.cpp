#include "ParticleForceRegistry.h"

ParticleForceRegistry::ParticleForceRegistry()
{
   registrations = Registry();
}

ParticleForceRegistry::~ParticleForceRegistry()
{
   clear();
}

void ParticleForceRegistry::add(Rigidbody *object, ParticleForceGenerator *fg)
{
   ParticleForceRegistration regi;
   regi.object = object;
   regi.fg = fg;
   registrations.push_back(regi);
}

void ParticleForceRegistry::remove(Rigidbody *object, ParticleForceGenerator *fg)
{
   for (Registry::iterator it = registrations.begin(); it != registrations.end(); it++)
   {
      if (it->object == object && it->fg == fg)
      {
         registrations.erase(it);
         break;
      }
   }
}

void ParticleForceRegistry::clear()
{
   registrations.clear();
}

void ParticleForceRegistry::updateForces(real duration)
{
   for(Registry::iterator it = registrations.begin(); it != registrations.end(); it++)
   {
      it->fg->updateForce(it->object, duration);
   }
}
