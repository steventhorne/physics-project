#pragma once
#include "ParticleForceGenerator.h"

class ParticleBungee : public ParticleForceGenerator
{
   public:
   ParticleBungee(Rigidbody* other, real springConstant, real restLength);

   virtual void updateForce(Rigidbody* object, real duration);

   private:
   Rigidbody* mOther;
   real mSpringConstant;
   real mRestLength;
};
