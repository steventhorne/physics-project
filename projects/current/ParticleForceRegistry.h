#pragma once
#include <vector>
#include "ParticleForceGenerator.h"

class ParticleForceRegistry
{
   protected:
   struct ParticleForceRegistration
   {
      Rigidbody *object;
      ParticleForceGenerator *fg;
   };

   typedef std::vector<ParticleForceRegistration> Registry;
   Registry registrations;

   public:
   ParticleForceRegistry();
   ~ParticleForceRegistry();

   void add(Rigidbody *object, ParticleForceGenerator *fg);

   void remove(Rigidbody *object, ParticleForceGenerator *fg);

   void clear();

   void updateForces(real duration);
};
