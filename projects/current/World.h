#pragma once
#include <vector>
#include <map>
#include "Game.h"
#include "ParticleForceRegistry.h"
#include "Plane.h"
#include "Cube.h"

using namespace std;

class World
{
public:
   World(Game* game);
   virtual ~World();

   virtual bool Init() = 0;
   virtual void Update(float deltaTime);
   virtual void Draw() = 0;

   virtual void Integrate(real duration);
   virtual void RunPhysics(real duration);

   void ResolveContacts(void* client_data, DtObjectRef obj1, DtObjectRef obj2, const DtCollData* coll_data);

   Cube* GetPlayer();

   float GetEnd() { return mEndZPos; };

protected:
   Game* mpGame;

   ParticleForceRegistry* mpForceRegistry;

   typedef map<GameObject*, real> ObjMap;
   typedef pair<GameObject*, real> ObjPair;
   typedef ObjMap::iterator obj_iter;
   ObjMap mGameObjects;

   Cube* mPlayer;
   float mEndZPos;

   real mDeltaTime;
};
