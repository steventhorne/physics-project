#pragma once
#include "ParticleForceGenerator.h"

class ParticleBuoyancy : public ParticleForceGenerator
{
   public:
   ParticleBuoyancy(real maxDepth, real volume, real waterHeight, real liquidDensity = 1000.0f);

   virtual void updateForce(Rigidbody* object, real duration);

   private:
   real mMaxDepth;
   real mVolume;
   real mWaterHeight;
   real mLiquidDensity;
};
