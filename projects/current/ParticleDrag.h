#pragma once
#include "ParticleForceGenerator.h"

class ParticleDrag : public ParticleForceGenerator
{
   public:
   ParticleDrag(real k1, real k2);

   virtual void updateForce(Rigidbody *object, real duration);

   private:
   real mK1;
   real mK2;
};
