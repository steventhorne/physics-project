#include "ParticleAnchorSpring.h"
#include <stdio.h>

ParticleAnchorSpring::ParticleAnchorSpring(Vector3 anchor, real springConstant, real restLength)
{
   mAnchor = anchor;
   mSpringConstant = springConstant;
   mRestLength = restLength;
}

void ParticleAnchorSpring::updateForce(Rigidbody* object, real duration)
{
   Vector3 force;
   force = object->GetPosition();
   force -= mAnchor;

   real magnitude = force.magnitude();
   magnitude = (mRestLength - magnitude) * mSpringConstant;

   force.normalize();

   force *= magnitude;

   object->AddForce(force);
}
