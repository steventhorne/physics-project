#include "ParticleDrag.h"

ParticleDrag::ParticleDrag(real k1, real k2)
{
   mK1 = k1;
   mK2 = k2;
}

void ParticleDrag::updateForce(Rigidbody *object, real duration)
{
   Vector3 force;
   force = object->GetVelocity();

   real dragCoeff = force.magnitude();
   dragCoeff = mK1 * dragCoeff + mK2 * dragCoeff * dragCoeff;

   force.normalize();
   force *= -dragCoeff;
   object->AddForce(force);
}
