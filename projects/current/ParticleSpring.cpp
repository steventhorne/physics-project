#include "ParticleSpring.h"
#include <Math.h>

ParticleSpring::ParticleSpring(Rigidbody* other, real springConstant, real restLength)
{
   mOther = other;
   mSpringConstant = springConstant;
   mRestLength = restLength;
}

void ParticleSpring::updateForce(Rigidbody* object, real duration)
{
   Vector3 force;
   force = object->GetPosition();
   force -= mOther->GetPosition();

   real magnitude = force.magnitude();
   magnitude = real_abs(magnitude - mRestLength);
   magnitude *= mSpringConstant;

   force.normalize();
   force *= -magnitude;
   object->AddForce(force);
}
