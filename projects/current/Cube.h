#pragma once
#include "Vector3.h"
#include "GameObject.h"

class Game;

class Cube : public GameObject
{
public:
   Cube( Game* pGame );
   Cube( Game* pGame, Vector3 position );
   Cube( Game* pGame, Vector3 position, Vector3 rotation );
   Cube( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale );
   ~Cube() {};

   void AddCollider();

   void Update(real duration);
   void Draw();
private:
   DtShapeRef mShape;
};
