#include "Vector3.h"
#include <math.h>

Vector3::Vector3()
{
    X = 0;
    Y = 0;
    Z = 0;
}

Vector3::Vector3( real x, real y, real z )
   :X(x), Y(y), Z(z) {}

Vector3 Vector3::operator+( const Vector3& rhs ) const
{
    Vector3 newVector = Vector3(X + rhs.X, Y + rhs.Y, Z + rhs.Z);

    return newVector;
}

void Vector3::operator+=( const Vector3& rhs )
{
   X += rhs.X;
   Y += rhs.Y;
   Z += rhs.Z;
}

Vector3 Vector3::operator-( const Vector3& rhs ) const
{
    Vector3 newVector = Vector3(X - rhs.X, Y - rhs.Y, Z - rhs.Z);

    return newVector;
}

void Vector3::operator-=( const Vector3& rhs )
{
   X -= rhs.X;
   Y -= rhs.Y;
   Z -= rhs.Z;
}

Vector3 Vector3::operator%( const Vector3& rhs ) const
{
    Vector3 crossVec = Vector3((Y*rhs.Z) - (Z*rhs.Y), (Z*rhs.X) - (X*rhs.Z), (X*rhs.Y) - (Y*rhs.X));

    return crossVec;
}

real Vector3::operator*( const Vector3& rhs ) const
{
   real dot = (rhs.X * X) + (Y * rhs.Y) + (Z * rhs.Z);

   return dot;
}

Vector3 Vector3::operator*(const real& rhs) const
{
   return Vector3(X*rhs, Y*rhs, Z*rhs);
}

void Vector3::operator*=(const real& rhs)
{
   X *= rhs;
   Y *= rhs;
   Z *= rhs;
}

void Vector3::clear()
{
   X = 0;
   Y = 0;
   Z = 0;
}

void Vector3::normalize()
{
    real mag = magnitude();

    if (mag == 0)
        return;

    X /= mag;
    Y /= mag;
    Z /= mag;
}

Vector3 Vector3::normalized()
{
    Vector3 newVector = Vector3(X, Y, Z);

    newVector.normalize();

    return newVector;
}

real Vector3::magnitude()
{
   real mag2 = magnitude2();

   real mag = sqrtf(mag2);

   return mag;
}

real Vector3::magnitude2()
{
   return (X*X + Y*Y + Z*Z);
}

Vector3 Vector3::cross(Vector3 other)
{
    Vector3 crossVec = Vector3((Y*other.Z) - (Z*other.Y), (Z*other.X) - (X*other.Z), (X*other.Y) - (Y*other.X));
    return crossVec;
}
