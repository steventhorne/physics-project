#include "ParticleBuoyancy.h"

ParticleBuoyancy::ParticleBuoyancy(real maxDepth, real volume, real waterHeight, real liquidDensity)
{
   mMaxDepth = maxDepth;
   mVolume = volume;
   mWaterHeight = waterHeight;
   mLiquidDensity = liquidDensity;
}

void ParticleBuoyancy::updateForce(Rigidbody* object, real duration)
{
   real depth = object->GetPosition().Y;

   if (depth >= mWaterHeight + mMaxDepth) return;
   Vector3 force(0,0,0);

   if (depth <= mWaterHeight - mMaxDepth)
   {
      force.Y = mLiquidDensity * mVolume;
      object->AddForce(force);
      return;
   }

   force.Y = mLiquidDensity * mVolume *
      (depth - mMaxDepth - mWaterHeight) / 2 * mMaxDepth;
   object->AddForce(force);
}
