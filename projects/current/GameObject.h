#pragma once
#include "Vector3.h"
#include "SOLID/solid.h"

class Game;
class Rigidbody;

typedef struct Collider
{
   int id;
} Collider;

class GameObject
{
   public:
   GameObject();
   GameObject( Game* pGame );
   GameObject( Game* pGame, Vector3 position );
   GameObject( Game* pGame, Vector3 position, Vector3 rotation );
   GameObject( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale );
   virtual ~GameObject();

   void CommonConstructor();

   void AddCollider(Collider collider);

   void AddRigidbody();

   void SetPosition( Vector3 position ) { mPosition = position; };
   Vector3 GetPosition() { return mPosition; };

   void SetRotation( Vector3 rotation ) { mRotation = rotation; };
   Vector3 SetRotation() { return mRotation; };

   void SetScale( Vector3 scale ) { mScale = scale; };
   Vector3 GetScale() { return mScale; };

   Rigidbody* GetRigidbody() { return mpBody; };

   Collider GetCollider() { return mpCollider; };

   void SetSelfLit(bool emissive) { mSelfLit = emissive; };
   bool IsSelfLit() { return mSelfLit; };

   void SetSelfLightAmount(float amount) { mLightAmount = amount; };
   float GetSelfLightAmount() { return mLightAmount; };


   virtual void Update(real duration);
   virtual void Draw() {};

   protected:
   Game* mpGame;
   Rigidbody* mpBody;
   Collider mpCollider;

   Vector3 mPosition;
   Vector3 mRotation;
   Vector3 mScale;
   bool mSelfLit;
   float mLightAmount;
};
