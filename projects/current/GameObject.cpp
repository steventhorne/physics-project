#include "GameObject.h"
#include <stdio.h>
#include "Rigidbody.h"
#include "Game.h"

GameObject::GameObject()
{
   mPosition = Vector3(0, 0, 0);
   mRotation = Vector3(0, 0, 0);
   mScale = Vector3(1, 1, 1);
   CommonConstructor();
}

GameObject::GameObject(Game* pGame)
{
   mpGame = pGame;
   mPosition = Vector3(0, 0, 0);
   mRotation = Vector3(0, 0, 0);
   mScale = Vector3(1, 1, 1);
   CommonConstructor();
}

GameObject::GameObject(Game* pGame, Vector3 position)
{
   mpGame = pGame;
   mPosition = position;
   mRotation = Vector3(0, 0, 0);
   mScale = Vector3(1, 1, 1);
   CommonConstructor();
}

GameObject::GameObject(Game* pGame, Vector3 position, Vector3 rotation)
{
   mpGame = pGame;
   mPosition = position;
   mRotation = rotation;
   mScale = Vector3(1, 1, 1);
   CommonConstructor();
}

GameObject::GameObject(Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale)
{
   mpGame = pGame;
   mPosition = position;
   mRotation = rotation;
   mScale = scale;
   CommonConstructor();
}

void GameObject::CommonConstructor()
{
   mpBody = 0;
   mpCollider.id = 0;
}

GameObject::~GameObject()
{
    if (mpBody != 0)
    {
        delete mpBody;
        mpBody = 0;
    }
}

void GameObject::AddRigidbody()
{
   mpBody = new Rigidbody();
   mpBody->SetPosition(mPosition);
   mpBody->SetRotation(mRotation);
}

void GameObject::AddCollider(Collider collider)
{
   mpCollider = collider;
}

void GameObject::Update(real duration)
{
   if (mpBody)
   {
      mpBody->Integrate(duration);

      mPosition = mpBody->GetPosition();
      mRotation = mpBody->GetRotation();
   }

   if (mpCollider.id != 0)
   {
      dtSelectObject(&mpCollider);
      dtLoadIdentity();
      dtTranslate(mPosition.X, mPosition.Y, mPosition.Z);
      Quaternion rotation = Quaternion(mRotation);
      dtRotate(rotation.R, rotation.I, rotation.J, rotation.K);
   }
}
