#pragma once
#include "ParticleForceGenerator.h"

class ParticleFakeSpring : public ParticleForceGenerator
{
   public:
   ParticleFakeSpring(Vector3* anchor, real springConstant, real damping);

   virtual void updateForce(Rigidbody* object, real duration);

   private:
   Vector3* mAnchor;
   real mSpringConstant;
   real mDamping;
};
