#pragma once
#include <GL/glfw.h>
#include "World.h"
#include "Plane.h"

class Level1 : public World
{
public:
    Level1(Game* game);
    virtual ~Level1();

    bool Init();
    void Update(float deltaTime);
    void Draw();

private:
    //path
    Plane* mPath;
    Plane* mBackground;

};
