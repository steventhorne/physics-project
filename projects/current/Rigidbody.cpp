#include "Rigidbody.h"
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <memory.h>

real sleepEpsilon = (real)0.3;

Rigidbody::Rigidbody()
{
   //mInverseMass = 0;
   //mInverseInertiaTensor;
   mLinearDamping = 0.999f;
   mAngularDamping = 0.001f;
   mInverseInertiaTensor.SetInverse(Matrix3(1, 0, 0, 0, 1, 0, 0, 0, 1));
   //mPosition;
   //mOrientation;
   //mVelocity;
   //mRotation;
   //mInverseInertiaTensorWorld;
   //mMotion;
   //mIsAwake;
   //mCanSleep;
   //mTransformMatrix;
   //mForceAccum;
   //mTorqueAccum;
   //mAcceleration;
   //mLastFrameAcceleration;
   //mIsAwake = true;
}

static inline void _transformInertiaTensor(Matrix3 &iitWorld,
                                           const Quaternion &q,
                                           const Matrix3 &iitBody,
                                           const Matrix4 &rotmat)
{
    real t4 = rotmat.Data[0]*iitBody.Data[0]+
        rotmat.Data[1]*iitBody.Data[3]+
        rotmat.Data[2]*iitBody.Data[6];
    real t9 = rotmat.Data[0]*iitBody.Data[1]+
        rotmat.Data[1]*iitBody.Data[4]+
        rotmat.Data[2]*iitBody.Data[7];
    real t14 = rotmat.Data[0]*iitBody.Data[2]+
        rotmat.Data[1]*iitBody.Data[5]+
        rotmat.Data[2]*iitBody.Data[8];
    real t28 = rotmat.Data[4]*iitBody.Data[0]+
        rotmat.Data[5]*iitBody.Data[3]+
        rotmat.Data[6]*iitBody.Data[6];
    real t33 = rotmat.Data[4]*iitBody.Data[1]+
        rotmat.Data[5]*iitBody.Data[4]+
        rotmat.Data[6]*iitBody.Data[7];
    real t38 = rotmat.Data[4]*iitBody.Data[2]+
        rotmat.Data[5]*iitBody.Data[5]+
        rotmat.Data[6]*iitBody.Data[8];
    real t52 = rotmat.Data[8]*iitBody.Data[0]+
        rotmat.Data[9]*iitBody.Data[3]+
        rotmat.Data[10]*iitBody.Data[6];
    real t57 = rotmat.Data[8]*iitBody.Data[1]+
        rotmat.Data[9]*iitBody.Data[4]+
        rotmat.Data[10]*iitBody.Data[7];
    real t62 = rotmat.Data[8]*iitBody.Data[2]+
        rotmat.Data[9]*iitBody.Data[5]+
        rotmat.Data[10]*iitBody.Data[8];

    iitWorld.Data[0] = t4*rotmat.Data[0]+
        t9*rotmat.Data[1]+
        t14*rotmat.Data[2];
    iitWorld.Data[1] = t4*rotmat.Data[4]+
        t9*rotmat.Data[5]+
        t14*rotmat.Data[6];
    iitWorld.Data[2] = t4*rotmat.Data[8]+
        t9*rotmat.Data[9]+
        t14*rotmat.Data[10];
    iitWorld.Data[3] = t28*rotmat.Data[0]+
        t33*rotmat.Data[1]+
        t38*rotmat.Data[2];
    iitWorld.Data[4] = t28*rotmat.Data[4]+
        t33*rotmat.Data[5]+
        t38*rotmat.Data[6];
    iitWorld.Data[5] = t28*rotmat.Data[8]+
        t33*rotmat.Data[9]+
        t38*rotmat.Data[10];
    iitWorld.Data[6] = t52*rotmat.Data[0]+
        t57*rotmat.Data[1]+
        t62*rotmat.Data[2];
    iitWorld.Data[7] = t52*rotmat.Data[4]+
        t57*rotmat.Data[5]+
        t62*rotmat.Data[6];
    iitWorld.Data[8] = t52*rotmat.Data[8]+
        t57*rotmat.Data[9]+
        t62*rotmat.Data[10];
}

/**
 * Inline function that creates a transform matrix from a
 * position and mOrientation.
 */
static inline void _calculateTransformMatrix(Matrix4 &transformMatrix,
                                             const Vector3 &position,
                                             const Quaternion &orientation)
{
    transformMatrix.Data[0] = 1-2*orientation.J*orientation.J-
        2*orientation.K*orientation.K;
    transformMatrix.Data[1] = 2*orientation.I*orientation.J -
        2*orientation.R*orientation.K;
    transformMatrix.Data[2] = 2*orientation.I*orientation.K +
        2*orientation.R*orientation.J;
    transformMatrix.Data[3] = position.X;

    transformMatrix.Data[4] = 2*orientation.I*orientation.J +
        2*orientation.R*orientation.K;
    transformMatrix.Data[5] = 1-2*orientation.I*orientation.I-
        2*orientation.K*orientation.K;
    transformMatrix.Data[6] = 2*orientation.J*orientation.K -
        2*orientation.R*orientation.I;
    transformMatrix.Data[7] = position.Y;

    transformMatrix.Data[8] = 2*orientation.I*orientation.K -
        2*orientation.R*orientation.J;
    transformMatrix.Data[9] = 2*orientation.J*orientation.K +
        2*orientation.R*orientation.I;
    transformMatrix.Data[10] = 1-2*orientation.I*orientation.I-
        2*orientation.J*orientation.J;
    transformMatrix.Data[11] = position.Z;
}

void Rigidbody::CalculateDerivedData()
{
    mOrientation.Normalize();

    // Calculate the transform matrix for the body.
    _calculateTransformMatrix(mTransformMatrix, mPosition, mOrientation);

    // Calculate the inertiaTensor in world space.
    _transformInertiaTensor(mInverseInertiaTensorWorld,
        mOrientation,
        mInverseInertiaTensor,
        mTransformMatrix);

}

void Rigidbody::Integrate(real duration)
{
    if (!mIsAwake) return;

    //fprintf(stderr, "%f\n", (mForceAccum*mInverseMass).Z);
    // Calculate linear acceleration from force inputs.
    mLastFrameAcceleration = mAcceleration;
    mLastFrameAcceleration += (mForceAccum * mInverseMass);

    // Calculate angular acceleration from torque inputs.
    Vector3 angularAcceleration =
        mInverseInertiaTensorWorld.Transform(mTorqueAccum);

    // Adjust velocities
    // Update linear velocity from both acceleration and impulse.
    mVelocity += (mLastFrameAcceleration * duration);

    // Update angular velocity from both acceleration and impulse.
    mRotation += (angularAcceleration * duration);

    // Impose drag.
    mVelocity *= real_pow(mLinearDamping, duration);
    mRotation *= real_pow(mAngularDamping, duration);

    // Adjust positions
    // Update linear position.
    mPosition += (mVelocity * duration);

    // Update angular position.
    mOrientation.AddScaledVector(mRotation, duration);

    // Normalize the mOrientation, and update the matrices with the new
    // position and mOrientation
    CalculateDerivedData();

    // Clear accumulators.
    ClearAccumulators();

    // Update the kinetic energy store, and possibly put the body to
    // sleep.
    if (mCanSleep) {
        real currentMotion = mVelocity * mVelocity +
            mRotation * mRotation;

        real bias = real_pow(0.5, duration);
        mMotion = bias*mMotion + (1-bias)*currentMotion;

        if (mMotion < sleepEpsilon) SetAwake(false);
        else if (mMotion > 10 * sleepEpsilon) mMotion = 10 * sleepEpsilon;
    }
}

void Rigidbody::SetMass(const real mass)
{
    assert(mass != 0);
    mInverseMass = ((real)1.0)/mass;
}

real Rigidbody::GetMass() const
{
    if (mInverseMass == 0) {
        return 0;
    } else {
        return ((real)1.0)/mInverseMass;
    }
}

void Rigidbody::SetInverseMass(const real inverseMass)
{
    mInverseMass = inverseMass;
}

real Rigidbody::GetInverseMass() const
{
    return mInverseMass;
}

bool Rigidbody::HasFiniteMass() const
{
    return mInverseMass >= 0.0f;
}

void Rigidbody::SetInertiaTensor(const Matrix3 &inertiaTensor)
{
    mInverseInertiaTensor.SetInverse(inertiaTensor);
}

void Rigidbody::GetInertiaTensor(Matrix3 *inertiaTensor) const
{
    inertiaTensor->SetInverse(mInverseInertiaTensor);
}

Matrix3 Rigidbody::GetInertiaTensor() const
{
    Matrix3 it;
    GetInertiaTensor(&it);
    return it;
}

void Rigidbody::GetInertiaTensorWorld(Matrix3 *inertiaTensor) const
{
    inertiaTensor->SetInverse(mInverseInertiaTensorWorld);
}

Matrix3 Rigidbody::GetInertiaTensorWorld() const
{
    Matrix3 it;
    GetInertiaTensorWorld(&it);
    return it;
}

void Rigidbody::SetInverseInertiaTensor(const Matrix3 &inverseInertiaTensor)
{
    mInverseInertiaTensor = inverseInertiaTensor;
}

void Rigidbody::GetInverseInertiaTensor(Matrix3 *inverseInertiaTensor) const
{
    *inverseInertiaTensor = mInverseInertiaTensor;
}

Matrix3 Rigidbody::GetInverseInertiaTensor() const
{
    return mInverseInertiaTensor;
}

void Rigidbody::GetInverseInertiaTensorWorld(Matrix3 *inverseInertiaTensor) const
{
    *inverseInertiaTensor = mInverseInertiaTensorWorld;
}

Matrix3 Rigidbody::GetInverseInertiaTensorWorld() const
{
    return mInverseInertiaTensorWorld;
}

void Rigidbody::SetDamping(const real linearDamping,
               const real angularDamping)
{
    mLinearDamping = linearDamping;
    mAngularDamping = angularDamping;
}

void Rigidbody::SetLinearDamping(const real linearDamping)
{
    mLinearDamping = linearDamping;
}

real Rigidbody::GetLinearDamping() const
{
    return mLinearDamping;
}

void Rigidbody::SetAngularDamping(const real angularDamping)
{
    mAngularDamping = angularDamping;
}

real Rigidbody::GetAngularDamping() const
{
    return mAngularDamping;
}

void Rigidbody::SetPosition(const Vector3 &position)
{
    mPosition = position;
}

void Rigidbody::SetPosition(const real x, const real y, const real z)
{
    mPosition.X = x;
    mPosition.Y = y;
    mPosition.Z = z;
}

void Rigidbody::GetPosition(Vector3 *position) const
{
    *position = mPosition;
}

Vector3 Rigidbody::GetPosition() const
{
    return mPosition;
}

void Rigidbody::SetOrientation(const Quaternion &orientation)
{
    mOrientation = orientation;
    mOrientation.Normalize();
}

void Rigidbody::SetOrientation(const real r, const real i,
                   const real j, const real k)
{
    mOrientation.R = r;
    mOrientation.I = i;
    mOrientation.J = j;
    mOrientation.K = k;
    mOrientation.Normalize();
}

void Rigidbody::GetOrientation(Quaternion *orientation) const
{
    *orientation = mOrientation;
}

Quaternion Rigidbody::GetOrientation() const
{
    return mOrientation;
}

void Rigidbody::GetOrientation(Matrix3 *matrix) const
{
    GetOrientation(matrix->Data);
}

void Rigidbody::GetOrientation(real matrix[9]) const
{
    matrix[0] = mTransformMatrix.Data[0];
    matrix[1] = mTransformMatrix.Data[1];
    matrix[2] = mTransformMatrix.Data[2];

    matrix[3] = mTransformMatrix.Data[4];
    matrix[4] = mTransformMatrix.Data[5];
    matrix[5] = mTransformMatrix.Data[6];

    matrix[6] = mTransformMatrix.Data[8];
    matrix[7] = mTransformMatrix.Data[9];
    matrix[8] = mTransformMatrix.Data[10];
}

void Rigidbody::GetTransform(Matrix4 *transform) const
{
    memcpy(transform, &mTransformMatrix.Data, sizeof(Matrix4));
}

void Rigidbody::GetTransform(real matrix[16]) const
{
    memcpy(matrix, mTransformMatrix.Data, sizeof(real)*12);
    matrix[12] = matrix[13] = matrix[14] = 0;
    matrix[15] = 1;
}

void Rigidbody::GetGLTransform(real matrix[16]) const
{
    matrix[0] = (real)mTransformMatrix.Data[0];
    matrix[1] = (real)mTransformMatrix.Data[4];
    matrix[2] = (real)mTransformMatrix.Data[8];
    matrix[3] = 0;

    matrix[4] = (real)mTransformMatrix.Data[1];
    matrix[5] = (real)mTransformMatrix.Data[5];
    matrix[6] = (real)mTransformMatrix.Data[9];
    matrix[7] = 0;

    matrix[8] = (real)mTransformMatrix.Data[2];
    matrix[9] = (real)mTransformMatrix.Data[6];
    matrix[10] = (real)mTransformMatrix.Data[10];
    matrix[11] = 0;

    matrix[12] = (real)mTransformMatrix.Data[3];
    matrix[13] = (real)mTransformMatrix.Data[7];
    matrix[14] = (real)mTransformMatrix.Data[11];
    matrix[15] = 1;
}

Matrix4 Rigidbody::GetTransform() const
{
    return mTransformMatrix;
}


Vector3 Rigidbody::GetPointInLocalSpace(const Vector3 &point) const
{
    return mTransformMatrix.TransformInverse(point);
}

Vector3 Rigidbody::GetPointInWorldSpace(const Vector3 &point) const
{
    return mTransformMatrix.Transform(point);
}

Vector3 Rigidbody::GetDirectionInLocalSpace(const Vector3 &direction) const
{
    return mTransformMatrix.TransformInverseDirection(direction);
}

Vector3 Rigidbody::GetDirectionInWorldSpace(const Vector3 &direction) const
{
    return mTransformMatrix.TransformDirection(direction);
}


void Rigidbody::SetVelocity(const Vector3 &velocity)
{
    mVelocity = velocity;
}

void Rigidbody::SetVelocity(const real x, const real y, const real z)
{
    mVelocity.X = x;
    mVelocity.Y = y;
    mVelocity.Z = z;
}

void Rigidbody::GetVelocity(Vector3 *velocity) const
{
    *velocity = mVelocity;
}

Vector3 Rigidbody::GetVelocity() const
{
    return mVelocity;
}

void Rigidbody::AddVelocity(const Vector3 &deltaVelocity)
{
    mVelocity += deltaVelocity;
}

void Rigidbody::SetRotation(const Vector3 &rotation)
{
    mRotation = rotation;
}

void Rigidbody::SetRotation(const real x, const real y, const real z)
{
    mRotation.X = x;
    mRotation.Y = y;
    mRotation.Z = z;
}

void Rigidbody::GetRotation(Vector3 *rotation) const
{
    *rotation = mRotation;
}

Vector3 Rigidbody::GetRotation() const
{
    return mRotation;
}

void Rigidbody::AddRotation(const Vector3 &deltaRotation)
{
    mRotation += deltaRotation;
}

void Rigidbody::SetAwake(const bool awake)
{
    if (awake) {
        mIsAwake= true;

        // Add a bit of motion to avoid it falling asleep immediately.
        mMotion = sleepEpsilon*2.0f;
    } else {
        mIsAwake = false;
        mVelocity.clear();
        mRotation.clear();
    }
}

void Rigidbody::SetCanSleep(const bool canSleep)
{
    mCanSleep = canSleep;

    if (!mCanSleep && !mIsAwake) SetAwake();
}


void Rigidbody::GetLastFrameAcceleration(Vector3 *acceleration) const
{
    *acceleration = mLastFrameAcceleration;
}

Vector3 Rigidbody::GetLastFrameAcceleration() const
{
    return mLastFrameAcceleration;
}

void Rigidbody::ClearAccumulators()
{
    mForceAccum.clear();
    mTorqueAccum.clear();
}

void Rigidbody::AddForce(const Vector3 &force)
{
    mForceAccum += force;
    mIsAwake = true;
}

void Rigidbody::AddForceAtBodyPoint(const Vector3 &force,
                                    const Vector3 &point)
{
    // Convert to coordinates relative to center of mass.
    Vector3 pt = GetPointInWorldSpace(point);
    AddForceAtPoint(force, pt);

}

void Rigidbody::AddForceAtPoint(const Vector3 &force,
                                const Vector3 &point)
{
    // Convert to coordinates relative to center of mass.
    Vector3 pt = point;
    pt -= mPosition;

    mForceAccum += force;
    mTorqueAccum += pt % force;

    mIsAwake = true;
}

void Rigidbody::AddTorque(const Vector3 &torque)
{
    mTorqueAccum += torque;
    mIsAwake = true;
}

void Rigidbody::SetAcceleration(const Vector3 &acceleration)
{
    mAcceleration = acceleration;
}

void Rigidbody::SetAcceleration(const real x, const real y, const real z)
{
    mAcceleration.X = x;
    mAcceleration.Y = y;
    mAcceleration.Z = z;
}

void Rigidbody::GetAcceleration(Vector3 *acceleration) const
{
    *acceleration = mLastFrameAcceleration;
}

Vector3 Rigidbody::GetAcceleration() const
{
    return mLastFrameAcceleration;
}
