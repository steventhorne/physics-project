-- A project defines one build target
project "Rigidbodies"
   kind "ConsoleApp"
   language "C++"
   targetname "rigidbodies"
   files { "**.h", "**.cpp" }
   includedirs { "./../../libs/glfw/include", "./../../libs/soil/include", "./../../libs/solid/include" }
   links { "glfw", "soil", "solid" }

   if os.get() == "macosx" then
      links { "OpenGL.framework", "Cocoa.framework", "CoreFoundation.framework", "IOKit.framework" }
   elseif os.get() == "windows" then
      links { "opengl32", "glu32" }
   end
