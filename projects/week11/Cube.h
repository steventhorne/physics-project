#pragma once
#include "Vector3.h"
#include "Object.h"

class Game;

class Cube : public Object
{
   public:
   Cube( Game* pGame );
   Cube( Game* pGame, Vector3 position );
   Cube( Game* pGame, Vector3 position, Vector3 rotation );
   Cube( Game* pGame, Vector3 position, Vector3 rotation, Vector3 scale );
   ~Cube() {};

   void draw();
};
