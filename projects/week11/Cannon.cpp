#include "Cannon.h"
#include "Game.h"
#include "Cube.h"
#include "Sphere.h"
#include "PhysicsObject.h"

Cannon::Cannon(Game* game, Vector3 position, Vector3 rotation)
{
   mpGame = game;

   mFire = true;
   mHasFired = false;

   mCurrentAction = 0;
   mCurrentTime = 0;

   mCannonBase = new Cube(mpGame, position, rotation);
}

Cannon::~Cannon()
{
   delete mCannonBase;
   mCannonBase = NULL;

   delete mProjectile;
   mProjectile = NULL;
}

void Cannon::AddAction(Action action)
{
   mActionList.push_back(action);
}

void Cannon::Update(float deltaTime)
{
   if (mFire)
   {
      if (mCurrentAction > 0)
      {
         delete mProjectile;
         mHasFired = false;
      }

      mProjectile = new PhysicsObject(new Sphere(mpGame), mCannonBase->getPosition());
      mProjectile->getObject()->setScale(Vector3(0.25, 0.25, 0.25));

      mProjectile->addForce(mActionList[mCurrentAction].Force);

      mFire = false;
      mHasFired = true;

      mCurrentTime = 0;
      mCurrentAction++;
   }

   mCurrentTime += deltaTime;

   if (mHasFired)
      mProjectile->update(deltaTime);

   if (mCurrentTime > mActionList[mCurrentAction].Time)
      mFire = true;
}

void Cannon::Draw()
{
   mCannonBase->draw();

   if (mHasFired)
   {
      mProjectile->draw();
   }
}
