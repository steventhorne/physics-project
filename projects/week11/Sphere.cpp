#include "Sphere.h"
#include "Camera.h"
#include "Game.h"

Sphere::Sphere( Game* pGame )
   :Object(pGame)
{

}

Sphere::Sphere( Game* pGame, Vector3 color )
   :Object(pGame, color)
{

}

Sphere::Sphere( Game* pGame, Vector3 color, Vector3 position )
   :Object(pGame, color, position)
{

}

Sphere::Sphere( Game* pGame, Vector3 color, Vector3 position, Vector3 rotation )
   :Object(pGame, color, position, rotation)
{

}

Sphere::Sphere( Game* pGame, Vector3 color, Vector3 position, Vector3 rotation, Vector3 scale )
   :Object(pGame, color, position, rotation, scale)
{

}

void Sphere::draw()
{
    draw(50, 50);
}

void Sphere::draw(double lats, double longs)
{
    glPushMatrix();
    glShadeModel(GL_SMOOTH);

    mpGame->getCamera()->applyCamera();

    if (mSelfLit)
    {
      GLfloat lightColor0[] = {mLightAmount, mLightAmount, mLightAmount, 1.0f};
      GLfloat lightPos0[] = {0.0f, 0.0f, 0.0f, 1.0f};
      glLightfv(GL_LIGHT1, GL_AMBIENT, lightColor0);
      glLightfv(GL_LIGHT1, GL_POSITION, lightPos0);
    }

    glTranslated(mPosition.X, mPosition.Y, mPosition.Z);
    glRotatef(mRotation.X, 1, 0, 0);
    glRotatef(mRotation.Y, 0, 1, 0);
    glRotatef(mRotation.Z, 0, 0, 1);

    int i, j;
    for(i = 0; i <= lats; i++)
    {
        double lat0 = PI * (-0.5 + (double) (i - 1) / lats);
        double z0  = sin(lat0);
        double zr0 =  cos(lat0);

        double lat1 = PI * (-0.5 + (double) i / lats);
        double z1 = sin(lat1);
        double zr1 = cos(lat1);

        glBegin(GL_QUAD_STRIP);
        glColor3f(mColor.X, mColor.Y, mColor.Z);

        for(j = 0; j <= longs; j++)
        {
            double lng = 2 * PI * (double) (j - 1) / longs;
            double x = cos(lng);
            double y = sin(lng);

            glNormal3f(x * zr0, y * zr0, z0);
            glVertex3f(x * zr0 * mScale.X, y * zr0 * mScale.Y, z0 * mScale.Z);
            glNormal3f(x * zr1, y * zr1, z1);
            glVertex3f(x * zr1 * mScale.X, y * zr1 * mScale.Y, z1 * mScale.Z);
        }
    }

    glEnd();

    if (mSelfLit)
    {
      GLfloat lightColor0[] = {0.0f, 0.0f, 0.0f, 0.0f};
      GLfloat lightPos0[] = {0.0f, 0.0f, 0.0f, 1.0f};
      glLightfv(GL_LIGHT1, GL_AMBIENT, lightColor0);
      glLightfv(GL_LIGHT1, GL_POSITION, lightPos0);
    }

    glPopMatrix();
}
