#pragma once
#include "PhysicsObject.h"

class ParticleContactResolver;

class ParticleContact
{
   friend class ParticleContactResolver;


   public:
   PhysicsObject* Objects[2];

   real Restitution;
   real Penetration;

   Vector3 ContactNormal;

   void Resolve(real duration);

   real CalculateSeparatingVelocity() const;

   Vector3 ParticleMovement[1];

   private:
   void resolveVelocity(real duration);

   void resolveInterpenetration(real duration);
};
