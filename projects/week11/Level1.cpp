#include "Level1.h"
#include "Camera.h"
#include "Sphere.h"
#include "ParticleGravity.h"
#include "ParticleAnchorSpring.h"
#include "ParticleCollision.h"

Level1::Level1(Game* game)
:World(game)
{
   mpForceRegistry = new ParticleForceRegistry();
   mpResolver = new ParticleContactResolver(0);

   mEndZPos = -20;
}

Level1::~Level1()
{
   delete mpForceRegistry;
   mpForceRegistry = NULL;

   delete mpResolver;
   mpResolver = NULL;

   delete[] mpContacts;
   mpContacts = NULL;

   delete mPath;
   mPath = NULL;

   for(obj_iter iterator = mObjects.begin(); iterator != mObjects.end(); iterator++)
   {
      delete iterator->first;
   }
   mObjects.clear();

   while(!mContactGenerators.empty()) mContactGenerators.pop_back();
}

bool Level1::Init()
{
   // set up camera for level
   mpGame->getCamera()->setPosition(Vector3(25, 2, -10));
   mpGame->getCamera()->setRotation(Vector2(0, -90));

   // init objects
   mPath = new Plane(mpGame, Vector3(1, 1, 1), Vector3(0, 0, -10), Vector3(0, 0, 0), Vector3(5, 0, 20));
   mBackground = new Plane( mpGame, Vector3(1, 1, 1), Vector3(0, 0, -10), Vector3(0, 0, 90), Vector3(16, 0, 15));

   //------------------- Instantiate Objects ----------------------
   mPlayer = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0.5, -1));
   mPlayer->getObject()->setScale(Vector3(0.5, 0.5, 0.5));
   mPlayer->setInverseMass(1);
   mObjects.insert(ObjPair(mPlayer, 0.5));

   mPendAnchor1 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 5, -5));
   mPendAnchor1->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mPendAnchor1->setInverseMass(0);
   mObjects.insert(ObjPair(mPendAnchor1, 1)); // object, restitution

   mPendBall1 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0, -5));
   mPendBall1->getObject()->setScale(Vector3(1, 1, 1));
   mPendBall1->setInverseMass(1);
   mObjects.insert(ObjPair(mPendBall1, 1));

   mPendAnchor2 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 5, -10));
   mPendAnchor2->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mPendAnchor2->setInverseMass(0);
   mObjects.insert(ObjPair(mPendAnchor2, 1)); // object, restitution

   mPendBall2 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 4, -10));
   mPendBall2->getObject()->setScale(Vector3(1, 1, 1));
   mPendBall2->setInverseMass(1);
   mObjects.insert(ObjPair(mPendBall2, 1));

   mPendAnchor3 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 5, -15));
   mPendAnchor3->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mPendAnchor3->setInverseMass(0);
   mObjects.insert(ObjPair(mPendAnchor3, 1)); // object, restitution

   mPendBall3 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0, -15));
   mPendBall3->getObject()->setScale(Vector3(1, 1, 1));
   mPendBall3->setInverseMass(1);
   mObjects.insert(ObjPair(mPendBall3, 1));

   //--------------------------------------------------------------

   //-------------------- Add Physics Forces ----------------------
   //Gravity
   mpForceRegistry->add(mPendBall1, new ParticleGravity(Vector3::gravity()));
   mpForceRegistry->add(mPendBall2, new ParticleGravity(Vector3::gravity()));
   mpForceRegistry->add(mPendBall3, new ParticleGravity(Vector3::gravity()));

   mpForceRegistry->add(mPendBall1, new ParticleAnchorSpring(mPendAnchor1->getPosition(), 7, 1));
   mpForceRegistry->add(mPendBall2, new ParticleAnchorSpring(mPendAnchor2->getPosition(), 7, 1));
   mpForceRegistry->add(mPendBall3, new ParticleAnchorSpring(mPendAnchor3->getPosition(), 7, 1));
   //--------------------------------------------------------------

   //-------------------- Create Collisions -----------------------
   obj_iter it;
   for (it = mObjects.begin(); it != mObjects.end(); ++it)
   {
      //doop
      ObjMap::iterator otherIt = it;
      ++otherIt;
      for (; otherIt != mObjects.end(); ++otherIt)
      {
         if ((*it) != (*otherIt))
         {
            ParticleCollision* collision = new ParticleCollision();
            collision->Objects[0] = (*it).first;
            collision->Objects[1] = (*otherIt).first;
            collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
            collision->Restitution = ((*it).second * (*otherIt).second) / 2;
            mContactGenerators.push_back(collision);
         }
      }
   }
   //--------------------------------------------------------------

   return true;
}

void Level1::Update(float deltaTime)
{
   RunPhysics(deltaTime);
}

void Level1::Draw()
{
   //do drawin) stuff here of objects
   mPath->draw();
   mBackground->draw();

   Vector3 position;

   glPushMatrix();
   mpGame->getCamera()->applyCamera();

   glBegin(GL_LINES);
   glColor3f(1, 0, 0);
   position = mPendAnchor1->getPosition();
   glVertex3f(position.X, position.Y, position.Z);
   position = mPendBall1->getPosition();
   glVertex3f(position.X, position.Y, position.Z);
   glEnd();

   glBegin(GL_LINES);
   glColor3f(1, 0, 0);
   position = mPendAnchor2->getPosition();
   glVertex3f(position.X, position.Y, position.Z);
   position = mPendBall2->getPosition();
   glVertex3f(position.X, position.Y, position.Z);
   glEnd();

   glBegin(GL_LINES);
   glColor3f(1, 0, 0);
   position = mPendAnchor3->getPosition();
   glVertex3f(position.X, position.Y, position.Z);
   position = mPendBall3->getPosition();
   glVertex3f(position.X, position.Y, position.Z);
   glEnd();

   glColor3f(1, 1, 1);
   glPopMatrix();

   for(obj_iter iterator = mObjects.begin(); iterator != mObjects.end(); iterator++)
   {
     iterator->first->draw();
   }
}
