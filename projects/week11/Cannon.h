#pragma once
#include <vector>
#include "Vector3.h"

class Game;
class Cube;
class PhysicsObject;

using namespace std;

struct Action
{
   float Time;
   Vector3 Force;

   Action(float time, Vector3 force)
   {
      Time = time;
      Force = force;
   }
};

class Cannon
{
public:
   Cannon(Game* game, Vector3 position, Vector3 rotation);
   ~Cannon();

   void AddAction(Action action);

   void Update(float deltaTime);

   void Draw();

private:
   Game* mpGame;

   Cube* mCannonBase;

   vector<Action> mActionList;

   PhysicsObject* mProjectile;

   int mCurrentAction;
   float mCurrentTime;
   bool mFire;
   bool mHasFired;
};
