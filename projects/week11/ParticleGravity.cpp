#include "ParticleGravity.h"

ParticleGravity::ParticleGravity( const Vector3 &gravity )
{
   mGravity = gravity;
}

void ParticleGravity::updateForce(PhysicsObject *object, real duration)
{
   object->addForce(mGravity * object->getMass());
}
