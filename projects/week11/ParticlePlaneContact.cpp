#include "ParticlePlaneContact.h"

unsigned ParticlePlaneContact::addContact(ParticleContact* contact, unsigned limit) const
{
   real length = currentLength();

   if (length > MinLength)
   {
      return 0;
   }

   contact->Objects[0] = Objects;

   Vector3 normal = Vector3(0, 1, 0);
   normal.normalize();
   contact->ContactNormal = normal;

   contact->Penetration = MinLength - length;
   contact->Restitution = Restitution;

   return 1;
}

real ParticlePlaneContact::currentLength() const
{
   float dist = Objects->getPosition().Y - PlaneObj->getPosition().Y;

   return dist;
}
