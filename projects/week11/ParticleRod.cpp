#include "ParticleRod.h"

unsigned ParticleRod::addContact(ParticleContact* contact, unsigned limit) const
{
   real currentLen = currentLength();

   if (currentLen == Length)
   {
      return 0;
   }

   contact->Objects[0] = Objects[0];
   contact->Objects[1] = Objects[1];

   Vector3 normal = Objects[1]->getPosition() - Objects[0]->getPosition();
   normal.normalize();

   if (currentLen > Length)
   {
      contact->ContactNormal = normal;
      contact->Penetration = currentLen - Length;
   }
   else
   {
      contact->ContactNormal = normal * -1;
      contact->Penetration = Length - currentLen;
   }

   contact->Restitution = 0;

   return 1;
}
