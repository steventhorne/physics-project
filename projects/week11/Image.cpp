#include "Image.h"
#include <GL/glfw.h>
#include <SOIL.H>
#include <string>

Image::Image(string path)
{
   mTexture = SOIL_load_OGL_texture(
            path.c_str(),
            SOIL_LOAD_AUTO,
            SOIL_CREATE_NEW_ID,
            SOIL_FLAG_POWER_OF_TWO
            | SOIL_FLAG_MIPMAPS
            | SOIL_FLAG_MULTIPLY_ALPHA
            | SOIL_FLAG_COMPRESS_TO_DXT
            | SOIL_FLAG_DDS_LOAD_DIRECT
            | SOIL_FLAG_INVERT_Y
            );
}

GLuint Image::GetGLuint()
{
   if( mTexture > 0 )
      return mTexture;
   else
   {
      return 0;
   }

}

bool Image::Bind()
{
   if ( mTexture > 0 )
   {
      glBindTexture( GL_TEXTURE_2D, mTexture );
      return true;
   }

   return false;
}

string Image::GetPath()
{
   return mPath;
}
