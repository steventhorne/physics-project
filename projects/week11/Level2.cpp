#include "Level2.h"
#include "Camera.h"
#include "Cannon.h"
#include "Sphere.h"
#include "ParticleGravity.h"
#include "ParticleAnchorSpring.h"
#include "ParticleSpring.h"
#include "ParticleCollision.h"

Level2::Level2(Game* game)
:World(game)
{
    mpForceRegistry = new ParticleForceRegistry();
    mpResolver = new ParticleContactResolver(0);

    mEndZPos = -40;
}

Level2::~Level2()
{
    delete mpForceRegistry;
    mpForceRegistry = NULL;

    delete mpResolver;
    mpResolver = NULL;

    delete[] mpContacts;
    mpContacts = NULL;

    delete mPath;
    mPath = NULL;

    for(obj_iter iterator = mObjects.begin(); iterator != mObjects.end(); iterator++)
    {
        delete iterator->first;
    }
    mObjects.clear();

    while(!mContactGenerators.empty()) mContactGenerators.pop_back();
}

bool Level2::Init()
{
   // set up camera for level
   mpGame->getCamera()->setPosition(Vector3(10, 10, 10));
   mpGame->getCamera()->setRotation(Vector2(23, -23));

   // init objects
   mPath = new Plane(mpGame, Vector3(1, 1, 1), Vector3(0, 0, -20), Vector3(0, 0, 0), Vector3(5, 0, 40));

   //------------------- Instantiate Objects ----------------------
   mPlayer = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0.5, -1));
   mPlayer->getObject()->setScale(Vector3(0.5, 0.5, 0.5));
   mPlayer->setInverseMass(1);
   mObjects.insert(ObjPair(mPlayer, 0.5));

   mPendAnchor1 = new PhysicsObject( new Sphere(mpGame), Vector3(5, 0, -5));
   mPendAnchor1->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mPendAnchor1->setInverseMass(0);
   mObjects.insert(ObjPair(mPendAnchor1, 1)); // object, restitution

   mPendBall1 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0, -5));
   mPendBall1->getObject()->setScale(Vector3(1, 1, 1));
   mPendBall1->setInverseMass(1);
   mObjects.insert(ObjPair(mPendBall1, 1));

   mPendAnchor2 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 5, -10));
   mPendAnchor2->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mPendAnchor2->setInverseMass(0);
   mObjects.insert(ObjPair(mPendAnchor2, 1)); // object, restitution

   mPendBall2 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 4, -10));
   mPendBall2->getObject()->setScale(Vector3(1, 1, 1));
   mPendBall2->setInverseMass(1);
   mObjects.insert(ObjPair(mPendBall2, 1));

   mPendAnchor3 = new PhysicsObject( new Sphere(mpGame), Vector3(-5, 0, -15));
   mPendAnchor3->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mPendAnchor3->setInverseMass(0);
   mObjects.insert(ObjPair(mPendAnchor3, 1)); // object, restitution

   mPendBall3 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0, -15));
   mPendBall3->getObject()->setScale(Vector3(1, 1, 1));
   mPendBall3->setInverseMass(1);
   mObjects.insert(ObjPair(mPendBall3, 1));

   mAdvPend1Achor = new PhysicsObject( new Sphere(mpGame), Vector3(0, 8, -20));
   mAdvPend1Achor->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mAdvPend1Achor->setInverseMass(0);
   mObjects.insert(ObjPair(mAdvPend1Achor, 1));

   mAdvPend1Ball1 = new PhysicsObject( new Sphere(mpGame), Vector3(5, 5, -20));
   mAdvPend1Ball1->getObject()->setScale(Vector3(0.5, 0.5, 0.5));
   mAdvPend1Ball1->setInverseMass(1);
   mObjects.insert(ObjPair(mAdvPend1Ball1, 1));

   mAdvPend1Ball2 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0, -20));
   mAdvPend1Ball2->getObject()->setScale(Vector3(0.5, 0.5, 0.5));
   mAdvPend1Ball2->setInverseMass(1);
   mObjects.insert(ObjPair(mAdvPend1Ball2, 1));

   mAdvPend2Achor = new PhysicsObject( new Sphere(mpGame), Vector3(0, 8, -25));
   mAdvPend2Achor->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
   mAdvPend2Achor->setInverseMass(0);
   mObjects.insert(ObjPair(mAdvPend2Achor, 1));

   mAdvPend2Ball1 = new PhysicsObject( new Sphere(mpGame), Vector3(-5, 5, -25));
   mAdvPend2Ball1->getObject()->setScale(Vector3(0.5, 0.5, 0.5));
   mAdvPend2Ball1->setInverseMass(1);
   mObjects.insert(ObjPair(mAdvPend2Ball1, 1));

   mAdvPend2Ball2 = new PhysicsObject( new Sphere(mpGame), Vector3(0, 0, -25));
   mAdvPend2Ball2->getObject()->setScale(Vector3(0.5, 0.5, 0.5));
   mAdvPend2Ball2->setInverseMass(1);
   mObjects.insert(ObjPair(mAdvPend2Ball2, 1));
   //--------------------------------------------------------------

   //-------------------- Add Physics Forces ----------------------
   //Gravity
   mpForceRegistry->add(mPendBall2, new ParticleGravity(Vector3::gravity()));

   mpForceRegistry->add(mAdvPend1Ball1, new ParticleGravity(Vector3::gravity()));
   mpForceRegistry->add(mAdvPend1Ball2, new ParticleGravity(Vector3::gravity()));
   mpForceRegistry->add(mAdvPend2Ball1, new ParticleGravity(Vector3::gravity()));
   mpForceRegistry->add(mAdvPend2Ball2, new ParticleGravity(Vector3::gravity()));

   mpForceRegistry->add(mPendBall1, new ParticleAnchorSpring(mPendAnchor1->getPosition(), 7, 3));
   mpForceRegistry->add(mPendBall2, new ParticleAnchorSpring(mPendAnchor2->getPosition(), 7, 1));
   mpForceRegistry->add(mPendBall3, new ParticleAnchorSpring(mPendAnchor3->getPosition(), 7, 3));

   mpForceRegistry->add(mAdvPend1Ball1, new ParticleAnchorSpring(mAdvPend1Achor->getPosition(), 7, 1));
   mpForceRegistry->add(mAdvPend1Ball1, new ParticleSpring(mAdvPend1Ball2, 7, 1));
   mpForceRegistry->add(mAdvPend1Ball2, new ParticleSpring(mAdvPend1Ball1, 7, 1));
   mpForceRegistry->add(mAdvPend2Ball1, new ParticleAnchorSpring(mAdvPend2Achor->getPosition(), 7, 1));
   mpForceRegistry->add(mAdvPend2Ball1, new ParticleSpring(mAdvPend2Ball2, 7, 1));
   mpForceRegistry->add(mAdvPend2Ball2, new ParticleSpring(mAdvPend2Ball1, 7, 1));
   //--------------------------------------------------------------

   //-------------------- Create Collisions -----------------------
   obj_iter it;
   for (it = mObjects.begin(); it != mObjects.end(); ++it)
   {
     //doop
     ObjMap::iterator otherIt = it;
     ++otherIt;
     for (; otherIt != mObjects.end(); ++otherIt)
     {
         if ((*it) != (*otherIt))
         {
             ParticleCollision* collision = new ParticleCollision();
             collision->Objects[0] = (*it).first;
             collision->Objects[1] = (*otherIt).first;
             collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
             collision->Restitution = ((*it).second * (*otherIt).second) / 2;
             mContactGenerators.push_back(collision);
         }
     }
   }
   //--------------------------------------------------------------

   return true;
}

void Level2::Update(float deltaTime)
{
   RunPhysics(deltaTime);
}

void Level2::Draw()
{
    //do drawin) stuff here of objects
    mPath->draw();

    Vector3 position;

    glPushMatrix();
    mpGame->getCamera()->applyCamera();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mPendAnchor1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mPendBall1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mPendAnchor2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mPendBall2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mPendAnchor3->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mPendBall3->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mAdvPend1Achor->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mAdvPend1Ball1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mAdvPend1Ball1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mAdvPend1Ball2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mAdvPend2Achor->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mAdvPend2Ball1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mAdvPend2Ball1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mAdvPend2Ball2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glColor3f(1, 1, 1);
    glPopMatrix();

    for(obj_iter iterator = mObjects.begin(); iterator != mObjects.end(); iterator++)
    {
        iterator->first->draw();
    }
}
