#include "ParticleLink.h"

real ParticleLink::currentLength() const
{
   Vector3 relativePos = Objects[0]->getPosition() -
                         Objects[1]->getPosition();

   return relativePos.magnitude();
}
