#include "ParticleLink.h"

class ParticleRod : public ParticleLink
{
   public:
   real Length;

   virtual unsigned addContact(ParticleContact* contact, unsigned limit) const;
};
