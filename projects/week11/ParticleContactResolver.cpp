#include "ParticleContactResolver.h"

ParticleContactResolver::ParticleContactResolver(unsigned iterations)
:mIterations(iterations)
{

}

void ParticleContactResolver::setIterations(unsigned iterations)
{
   mIterations = iterations;
}

void ParticleContactResolver::resolveContacts(ParticleContact** contactArray, unsigned numContacts, real duration)
{
   unsigned i;

   mIterationsUsed = 0;
   while(mIterationsUsed < mIterations)
   {
      real max = REAL_MAX;
      unsigned maxIndex = numContacts;

      for (i = 0; i < numContacts; i++)
      {
         real sepVel = contactArray[i]->CalculateSeparatingVelocity();
         if (sepVel < max &&
            (sepVel < 0 || contactArray[i]->Penetration > 0))
         {
            max = sepVel;
            maxIndex = i;
         }
      }

      if (maxIndex == numContacts) break;

      contactArray[maxIndex]->Resolve(duration);

       // Update the interpenetrations for all particles
       Vector3 *move = contactArray[maxIndex]->ParticleMovement;
       for (i = 0; i < numContacts; i++)
       {
           if (contactArray[i]->Objects[0] == contactArray[maxIndex]->Objects[0])
           {
               contactArray[i]->Penetration -= move[0] * contactArray[i]->ContactNormal;
           }
           else if (contactArray[i]->Objects[0] == contactArray[maxIndex]->Objects[1])
           {
               contactArray[i]->Penetration -= move[1] * contactArray[i]->ContactNormal;
           }
           if (contactArray[i]->Objects[1])
           {
               if (contactArray[i]->Objects[1] == contactArray[maxIndex]->Objects[0])
               {
                   contactArray[i]->Penetration += move[0] * contactArray[i]->ContactNormal;
               }
               else if (contactArray[i]->Objects[1] == contactArray[maxIndex]->Objects[1])
               {
                   contactArray[i]->Penetration += move[1] * contactArray[i]->ContactNormal;
               }
           }
       }

      mIterationsUsed++;
   }
}
