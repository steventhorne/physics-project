#pragma once
#include <vector>
#include "Vector3.h"
#include "Vector2.h"
#include "Object.h"

using namespace std;

class Game;

class LoadedObject : public Object
{
   public:
   LoadedObject( Game* pGame, char* filename );
   LoadedObject( Game* pGame, char* filename, Vector3 color );
   LoadedObject( Game* pGame, char* filename, Vector3 color, Vector3 position );
   LoadedObject( Game* pGame, char* filename, Vector3 color, Vector3 position, Vector3 rotation );
   LoadedObject( Game* pGame, char* filename, Vector3 color, Vector3 position, Vector3 rotation, Vector3 scale );
   ~LoadedObject() {};

   void draw();

   private:
   void loadObject(char* file);

   vector<Vector3> mVertices;
   vector<Vector2> mTex;
   vector<Vector3> mNormals;
   vector<char*> mFaces;
};
