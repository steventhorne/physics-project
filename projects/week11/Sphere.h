#pragma once
#include <math.h>
#include "Vector3.h"
#include "Object.h"

class Game;

class Sphere : public Object
{
    public:
    Sphere( Game* pGame );
    Sphere( Game* pGame, Vector3 color );
    Sphere( Game* pGame, Vector3 color, Vector3 position );
    Sphere( Game* pGame, Vector3 color, Vector3 position, Vector3 rotation );
    Sphere( Game* pGame, Vector3 color, Vector3 position, Vector3 rotation, Vector3 scale );
    ~Sphere() {};

    void draw();
    void draw(double lats, double longs);
};
