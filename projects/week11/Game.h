#pragma once
#include "Precision.h"

class World;
class Input;
class Camera;

class Game
{
public:
   Game();
   ~Game();

   Input* getInputManager() { return mpInputManager; };
   Camera* getCamera() { return mpCamera; };

   bool init();
   void update();

   void draw();

   bool shouldExit() { return mShouldExit; };

private:
   Input* mpInputManager;
   Camera* mpCamera;

   World* mLevel;

   unsigned generateContacts();
   void integrate(real duration);
   void runPhysics(real duration);

   void loadNewLevel(int levelNum);

   real mDeltaTime;
   real mLastTime;

   int mCurrentLvlNum;
   int mNumLevels;

   bool mShouldExit;

};
