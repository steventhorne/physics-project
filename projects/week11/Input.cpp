#include "Input.h"

Input::Input()
{
    int x, y;
    glfwGetMousePos( &x, &y );

    lastMouse = Vector2(x, y);
    currentMouse = Vector2(x, y);

    for (int i = 0; i < 255; i++)
    {
        mKeysDown[i] = NULL;
    }
}

Input::~Input()
{

}

bool Input::Init()
{
    glfwEnable( GLFW_KEY_REPEAT );

    return true;
}

void Input::Update()
{
    int x, y;
    glfwGetMousePos( &x, &y );
    lastMouse = currentMouse;
    currentMouse = Vector2(x, y);

    for (int i = 0; i < 255; i++)
    {
        mKeysDown[i] = NULL;
    }
}

void GLFWCALL Input::mousePosCallback(int x, int y)
{

}

// GLFW_PRESS/GLFW_RELEASE
void GLFWCALL Input::mouseButtonCallback(int button, int action)
{

}

// GLFW_PRESS/GLFW_RELEASE
void GLFWCALL Input::keyboardPressCallback(int key, int action)
{
    if (action == GLFW_PRESS)
    {
        bool containsKey = false;
        for (int i = 0; i < 255; i++)
        {
            if (mKeysDown[i] == key)
            {
                containsKey = true;
                break;
            }
        }

        if (!containsKey)
        {
            for (int i = 0; i < 255; i++)
            {
                if (mKeysDown[i] == NULL)
                {
                    mKeysDown[i] = key;
                    break;
                }
            }
        }
    }
}

// GLFW_PRESS/GLFW_RELEASE
void GLFWCALL Input::charPressCallback(int key, int action)
{

}

bool Input::isKeyPressed(int key)
{
    if (glfwGetKey( key ) == GLFW_PRESS)
        return true;

    return false;
    /*
    for (int i = 0; i < 255; i++)
    {
        if (mKeysDown[i] == key)
        {
            return true;
        }
    }

    return false;
    */
}

bool Input::isKeyReleased(int key)
{
   if (glfwGetKey( key ) == GLFW_RELEASE)
      return true;

   return false;
}

bool Input::mouseButtonDown(int button)
{
    if(glfwGetMouseButton( button ) == GLFW_PRESS )
    {
        return true;
    }
    return false;
}

Vector2 Input::mouseDiff()
{
    return Vector2(currentMouse.X - lastMouse.X, currentMouse.Y - lastMouse.Y);
}
