#include "Vector2.h"
#include <Math.h>

Vector2::Vector2(real x, real y)
   :X(x), Y(y) {}

Vector2 Vector2::operator+( const Vector2& rhs ) const
{
   Vector2 newVec = Vector2(X + rhs.X, Y + rhs.Y);

   return newVec;
}

Vector2 Vector2::operator-( const Vector2& rhs ) const
{
   Vector2 newVec = Vector2(X - rhs.X, Y - rhs.Y);

   return newVec;
}

real Vector2::operator*( const Vector2& rhs ) const
{
   real dot = (X * rhs.X) + (Y * rhs.Y);

   return dot;
}

Vector2 Vector2::operator*( const real& rhs ) const
{
   return Vector2(X * rhs, Y * rhs);
}

void Vector2::normalize()
{
    real x2 = X*X;
    real y2 = Y*Y;

    real mag = sqrt(x2 + y2);

    if (mag == 0) return;

    X /= mag;
    Y /= mag;
}

Vector2 Vector2::normalized()
{
    Vector2 newVector = Vector2(X, Y);

    newVector.normalize();

    return newVector;
}
