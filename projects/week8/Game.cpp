#include <iostream>
#include <math.h>
#include "Game.h"
#include "Input.h"
#include "Camera.h"
#include "Sphere.h"
#include "ParticleGravity.h"
#include "ParticleSpring.h"
#include "ParticleAnchorSpring.h"
#include "ParticleCollision.h"

using namespace std;

Game::Game()
{
    mpInputManager = new Input();
    mpCamera = new Camera(this, Vector3(0, 5, 20), Vector2(0, 0));
    mpForceRegistry = new ParticleForceRegistry();
    mpResolver = new ParticleContactResolver(0);
    mMaxContacts = 100;
    mpContacts = (ParticleContact*) malloc (sizeof(ParticleContact) * mMaxContacts);
}

Game::~Game()
{
    delete mpInputManager;
    mpInputManager = NULL;

    delete mpCamera;
    mpCamera = NULL;

    delete mpForceRegistry;
    mpForceRegistry = NULL;

    delete mpResolver;
    mpResolver = NULL;

    delete mpContacts;
    mpContacts = NULL;

    while(!mContactGenerators.empty()) mContactGenerators.pop_back();
}

bool Game::init()
{
    mpInputManager->Init();

    GLfloat sun_intensity[] = { 0.8, 0.8, 0.8, 1.0 };
    GLfloat sun_direction[] = { 0.0, 2.0, -1.0, 1.0 };
    GLfloat ambient_intensity[] = { 0.35, 0.35, 0.35, 1.0 };

    glEnable(GL_LIGHTING);
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient_intensity);

    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, sun_direction);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, sun_intensity);

    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);

    mDeltaTime = 0.0;
    mLastTime = 0;

    // init objects
    mFloor = new Plane(this, Vector3(1, 1, 1), Vector3(0, 0, 0), Vector3(0, 0, 0), Vector3(20, 20, 20));

    //------------------- Instantiate Objects ----------------------
    mAnchor = new PhysicsObject(new Sphere(this), Vector3(0, 6, 0));
    mAnchor->getObject()->setScale(Vector3(0.25, 0.25, 0.25));
    mAnchor->setInverseMass(0);
    mObjects.insert(ObjPair(mAnchor, 1));

    mBall = new PhysicsObject(new Sphere(this), Vector3(2, 5, 0));
    mBall->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mBall->setMass(1);
    mObjects.insert(ObjPair(mBall, 1));

    mBallPair1 = new PhysicsObject(new Sphere(this), Vector3(3, 4, -2));
    mBallPair1->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mBallPair1->setMass(1);
    mObjects.insert(ObjPair(mBallPair1, 1));

    mBallPair2 = new PhysicsObject(new Sphere(this), Vector3(6, 2, 4));
    mBallPair2->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mBallPair2->setMass(1);
    mObjects.insert(ObjPair(mBallPair2, 1));

    mSideBall = new PhysicsObject(new Sphere(this), Vector3(-6, 2, 0));
    mSideBall->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mSideBall->setMass(1);
    mObjects.insert(ObjPair(mSideBall, 1));

    mSideBall2 = new PhysicsObject(new Sphere(this), Vector3(-6, 0, 0), Vector3(5, 0, 0));
    mSideBall2->getObject()->setScale(Vector3(0.75, 0.75, 0.75));
    mSideBall2->setMass(1);
    mObjects.insert(ObjPair(mSideBall2, 1));
    //--------------------------------------------------------------

    //-------------------- Add Physics Forces ----------------------
    mpForceRegistry->add(mBall, new ParticleGravity(Vector3::gravity()));
    mpForceRegistry->add(mBallPair1, new ParticleGravity(Vector3::gravity()));
    mpForceRegistry->add(mBallPair2, new ParticleGravity(Vector3::gravity()));

    Vector3 anchorPos = mAnchor->getPosition();
    mpForceRegistry->add(mBall, new ParticleSpring(mAnchor, 5.0f, 1.0f));

    mpForceRegistry->add(mBallPair1, new ParticleSpring(mBallPair2, 5, 1));
    mpForceRegistry->add(mBallPair2, new ParticleSpring(mBallPair1, 5, 1));

    anchorPos = Vector3(4.5, 5, 1);
    mpForceRegistry->add(mBallPair1, new ParticleAnchorSpring(anchorPos, 5, 1));
    mpForceRegistry->add(mBallPair2, new ParticleAnchorSpring(anchorPos, 5, 1));

    anchorPos = Vector3(-8, 2, 0);
    mpForceRegistry->add(mSideBall, new ParticleGravity(Vector3::gravity()));
    mpForceRegistry->add(mSideBall, new ParticleAnchorSpring(anchorPos, 5, 1));
    mpForceRegistry->add(mSideBall, new ParticleSpring(mSideBall2, 5, 1));

    mpForceRegistry->add(mSideBall2, new ParticleGravity(Vector3::gravity()));
    mpForceRegistry->add(mSideBall2, new ParticleSpring(mSideBall, 5, 1));
    //--------------------------------------------------------------

    //--------------------- Add Contacts ---------------------------
    /*ParticlePlaneContact* collision = new ParticlePlaneContact();
    collision->Objects[0] = mBallPair1;
    collision->Objects[1] = mBallPair2;
    collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
    collision->Restitution = 0.99;
    mContactGenerators.push_back(collision);

    collision = new ParticleCollision();
    collision->Objects[0] = mSideBall;
    collision->Objects[1] = mSideBall2;
    collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
    collision->Restitution = 0.9;
    mContactGenerators.push_back(collision);*/

    /*collision = new ParticleCollision();
    collision->Objects[0] = mSideBall2;
    collision->MinLength = collision->Objects[0]->getScale().X;
    collision->Restitution = 1.0;
    mContactGenerators.push_back(collision);*/ // dont add this back

    ObjMap::iterator it;
    for (it = mObjects.begin(); it != mObjects.end(); ++it)
    {
       //doop
       ObjMap::iterator otherIt = it;
       ++otherIt;
       for (; otherIt != mObjects.end(); ++otherIt)
       {
          if ((*it) != (*otherIt))
          {
             ParticleCollision* collision = new ParticleCollision();
             collision->Objects[0] = (*it).first;
             collision->Objects[1] = (*otherIt).first;
             collision->MinLength = collision->Objects[0]->getScale().X + collision->Objects[1]->getScale().X;
             collision->Restitution = ((*it).second * (*otherIt).second) / 2;
             mContactGenerators.push_back(collision);
          }
       }
    }
    //--------------------------------------------------------------

    return true;
}

void Game::update()
{
    if (mLastTime == 0)
    {
        mLastTime = glfwGetTime();
        return;
    }

    mpInputManager->Update();

    real deltaTime = glfwGetTime() - mLastTime;

    runPhysics(deltaTime);

    mpCamera->update(deltaTime);

    mLastTime = glfwGetTime();

    draw();
}

unsigned Game::generateContacts()
{
   unsigned limit = mMaxContacts;
   ParticleContact* nextContact = mpContacts;

   for (ContactGenerators::iterator g = mContactGenerators.begin();
      g != mContactGenerators.end();
      g++)
   {
      unsigned used = (*g)->addContact(nextContact, limit);
      limit -= used;
      nextContact += used;

      if (limit <= 0) break;
   }

   return mMaxContacts - limit;
}

void Game::integrate(real duration)
{
    // update objects
    mBall->update(duration);
    mBallPair1->update(duration);
    mBallPair2->update(duration);
    mSideBall->update(duration);
    mSideBall2->update(duration);
}

void Game::runPhysics(real duration)
{
   // update force registry
   mpForceRegistry->updateForces(duration);

   integrate(duration);

   unsigned usedContacts = generateContacts();

   if (usedContacts)
   {
      mpResolver->setIterations(usedContacts * 2);
      mpResolver->resolveContacts(mpContacts, usedContacts, duration);
   }
}

void Game::draw()
{
    glClearColor( 0.2f, 0.2f, 0.2f, 1.0f ); // clear background to gray
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_MODELVIEW);

    glLoadIdentity();

    //do drawin) stuff here of objects
    //mFloor->draw();

    Vector3 position;

    glPushMatrix();
    mpCamera->applyCamera();

    glBegin(GL_LINES);
    glColor3f(1, 0, 0);
    position = mAnchor->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mBall->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    position = Vector3(4.5, 5, 1);
    glVertex3f(position.X, position.Y, position.Z);
    position = mBallPair1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    position = Vector3(4.5, 5, 1);
    glVertex3f(position.X, position.Y, position.Z);
    position = mBallPair2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    position = mBallPair1->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mBallPair2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    position = Vector3(-7, 2, 0);
    glVertex3f(position.X, position.Y, position.Z);
    position = mSideBall->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glBegin(GL_LINES);
    position = mSideBall2->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    position = mSideBall->getPosition();
    glVertex3f(position.X, position.Y, position.Z);
    glEnd();

    glColor3f(1, 1, 1);
    glPopMatrix();

    mFloor->draw();
    mAnchor->draw();
    mBall->draw();
    mBallPair1->draw();
    mBallPair2->draw();
    mSideBall->draw();
    mSideBall2->draw();

    glPushMatrix();

    mpCamera->applyCamera();

    glPopMatrix();

    mpCamera->draw();

    glfwSwapBuffers();
}
