#pragma once
#include <GL/glfw.h>
#include <vector>
#include <map>
#include "ParticleForceRegistry.h"
#include "ParticleContactResolver.h"
#include "ParticleContactGenerator.h"
#include "ParticleContact.h"
#include "PhysicsObject.h"
#include "Plane.h"

using namespace std;

class Input;
class Camera;

class Game
{
    public:
    Game();
    ~Game();

    Input* getInputManager() { return mpInputManager; };
    Camera* getCamera() { return mpCamera; };

    bool init();
    void update();

    void draw();

    private:
    Input* mpInputManager;
    Camera* mpCamera;

    typedef vector<ParticleContactGenerator*> ContactGenerators;
    ParticleForceRegistry* mpForceRegistry;
    ParticleContactResolver* mpResolver;
    ContactGenerators mContactGenerators;
    ParticleContact* mpContacts;
    unsigned mMaxContacts;
    unsigned generateContacts();
    void integrate(real duration);
    void runPhysics(real duration);

    typedef map<PhysicsObject*, real> ObjMap;
    typedef pair<PhysicsObject*, real> ObjPair;
    ObjMap mObjects;
    Plane* mFloor;
    PhysicsObject* mAnchor;
    PhysicsObject* mBall;
    PhysicsObject* mBallPair1;
    PhysicsObject* mBallPair2;
    PhysicsObject* mSideBall;
    PhysicsObject* mSideBall2;

    real mDeltaTime;
    real mLastTime;
};
