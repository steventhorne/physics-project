#include "ParticleLink.h"

class CableLink : ParticleLink
{
   public:
   real MaxLength;
   real Restitution;

   virtual unsigned addContact(ParticleContact* contact, unsigned limit) const;
};
