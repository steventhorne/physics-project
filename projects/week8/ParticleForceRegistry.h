#pragma once
#include <vector>
#include "ParticleForceGenerator.h"

class ParticleForceRegistry
{
   protected:
   struct ParticleForceRegistration
   {
      PhysicsObject *object;
      ParticleForceGenerator *fg;
   };

   typedef std::vector<ParticleForceRegistration> Registry;
   Registry registrations;

   public:
   ParticleForceRegistry();
   ~ParticleForceRegistry();

   void add(PhysicsObject *object, ParticleForceGenerator *fg);

   void remove(PhysicsObject *object, ParticleForceGenerator *fg);

   void clear();

   void updateForces(real duration);
};
