#include "ParticleSpring.h"
#include <Math.h>

ParticleSpring::ParticleSpring(PhysicsObject* other, real springConstant, real restLength)
{
   mOther = other;
   mSpringConstant = springConstant;
   mRestLength = restLength;
}

void ParticleSpring::updateForce(PhysicsObject* object, real duration)
{
   Vector3 force;
   force = object->getPosition();
   force -= mOther->getPosition();

   real magnitude = force.magnitude();
   magnitude = real_abs(magnitude - mRestLength);
   magnitude *= mSpringConstant;

   force.normalize();
   force *= -magnitude;
   object->addForce(force);
}
