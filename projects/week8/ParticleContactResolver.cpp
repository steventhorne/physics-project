#include "ParticleContactResolver.h"

ParticleContactResolver::ParticleContactResolver(unsigned iterations)
{
   mIterations = iterations;
}

void ParticleContactResolver::setIterations(unsigned iterations)
{
   mIterations = iterations;
}

void ParticleContactResolver::resolveContacts(ParticleContact* contactArray, unsigned numContacts, real duration)
{
   unsigned i;

   mIterationsUsed = 0;
   while(mIterationsUsed < mIterations)
   {
      real max = REAL_MAX;
      unsigned maxIndex = numContacts;
      for (i = 0; i < numContacts; i++)
      {
         real sepVel = contactArray[i].CalculateSeparatingVelocity();
         if (sepVel < max &&
            (sepVel < 0 || contactArray[i].Penetration > 0))
         {
            max = sepVel;
            maxIndex = i;
         }
      }

      if (maxIndex == numContacts) break;

      contactArray[maxIndex].Resolve(duration);

      mIterationsUsed++;
   }
}
