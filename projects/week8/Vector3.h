#pragma once
#include "Precision.h"

class Vector3
{
    public:
    Vector3();
    Vector3( real x, real y, real z );
    ~Vector3() {};

    Vector3 operator+(const Vector3& rhs) const; // Vec3 + Vec3
    void operator+=(const Vector3& rhs); // Vec3 += Vec3
    Vector3 operator-(const Vector3& rhs) const; // Vec3 - Vec3
    void operator-=(const Vector3& rhs);
    Vector3 operator%(const Vector3& rhs) const; // cross product
    real operator*(const Vector3& rhs) const; // dot product
    Vector3 operator*(const real& rhs) const; // Vec3 * float
    void operator*=(const real& rhs); // Vec3 *= float

    void clear();

    static Vector3 gravity() { return Vector3(0, -15, 0); };

    void normalize();
    Vector3 normalized();

    real magnitude();
    real magnitude2();

    Vector3 cross(Vector3 other);

    real X;
    real Y;
    real Z;
};
