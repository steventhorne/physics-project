#pragma once
#include "ParticleForceGenerator.h"

class ParticleSpring : public ParticleForceGenerator
{
   public:
   ParticleSpring(PhysicsObject* other, real springConstant, real restLength);

   virtual void updateForce(PhysicsObject* object, real duration);

   private:
   PhysicsObject* mOther;
   real mSpringConstant;
   real mRestLength;
};
